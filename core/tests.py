from django.test import TestCase

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

LOCAL_HOST = 'http://localhost:8000'

# Create your tests here.
class Prueba(TestCase):
    """Un test de Prueba"""
    def setUp(self):
        self.driver = webdriver.Firefox()

    def test_search_in_python_org(self):
        driver = self.driver
        driver.get(LOCAL_HOST)
        self.assertIn("ERP", driver.title)
        # elem = driver.find_element_by_name("q")
        # elem.send_keys("pycon")
        # elem.send_keys(Keys.RETURN)
        # assert "No results found." not in driver.page_source


    def tearDown(self):
        self.driver.close()


# class EmpresaViewTest(TestCase):
#     """ Pruebas sobre la vista para empresas """
#
#     def setUp(self):
#         self.driver = webdriver.Firefox()
#
#     def tearDown(self):
#         self.driver.close()
#
#     def create_new_element(self):
#         self.driver.get(LOCAL_HOST)


    # def test_list_elements(self):
    #     self.driver.get(LOCAL_HOST)
    #     print(self.driver.perform())
