from django.conf import settings
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand
from django.core.management import call_command


class Command(BaseCommand):
    args = 'No es necesario pasar argumentos'
    help = 'Comando de ejecución de tereas para poner en marcha el sistema'
    password = "G7m6Vgkz"

    def handle(self, *args, **options):
        self.stdout.write("Inicializando el sistema...")
        self.stdout.write("cargando data fixutures...")
        call_command('loaddata', 'division_territorial/fixtures/initial_data.json')
        call_command('loaddata', 'core/fixtures/initial_data.json')
        call_command('loaddata', 'almacen/fixtures/initial_data.json')
        self.stdout.write("data cargada.")
        self.stdout.write("actualizando archivos estaticos...")
        call_command('collectstatic', clear=True, interactive=False)
        self.stdout.write("actualización de archivos estaticos finalizada.")
        self.stdout.write("creando usuario administrador.")
        if not User.objects.filter(username="admin").exists():
            User.objects.create_superuser("admin", "admin@soniqsa.com.mx", self.password)
        self.stdout.write("creado usuario administrador con los siguientes datos")
        self.stdout.write("user: admin")
        self.stdout.write("password: %s" % self.password)
        self.stdout.write("Sistema inicializado")
        #if settings.DEBUG == False:
