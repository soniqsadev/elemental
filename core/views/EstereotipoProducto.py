from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import ValidationError
from django.shortcuts import render, redirect, get_object_or_404

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from django.views.generic import View
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView as CV, UpdateView as UV, DeleteView as DV
from django.views.generic.detail import DetailView

from django.forms.formsets import formset_factory
from django.forms import modelformset_factory

from ..models import EstereotipoProducto, PropiedadEstereotipo
from ..forms.EstereotipoProducto import EstereotipoProductoForm
from ..forms.PropiedadEstereotipo import PropiedadEstereotipoForm


SUCCESS_URL = reverse_lazy('productos.list')

def permissions_show(user):
    return user.has_perm('core.can_add_estereotipoproducto') or user.has_perm('core.can_change_estereotipoproducto') or user.has_perm('core.can_delete_estereotipoproducto')


class ListView(ListView):
    context_object_name = 'productos'
    model = EstereotipoProducto
    template_name = 'core/producto/list.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(ListView, self).dispatch(*args, **kwargs)


class CreateView(View):

    template_name = 'core/producto/create.html'

    def __init__(self, *args, **kwargs):
        super(CreateView, self).__init__(*args, **kwargs)
        self.PropiedadEstereotipoFormSet = modelformset_factory(PropiedadEstereotipo,
                                                             can_delete=True,
                                                             max_num=20,
                                                             min_num=0,
                                                             validate_max=True,
                                                             fields=('id', 'propiedad', 'valor')
                                                        )

    def get(self, request):
        redirection = request.GET.get('redirection', None)
        print('redirection', redirection)
        form = EstereotipoProductoForm()
        form.propiedad_form = self.PropiedadEstereotipoFormSet(queryset=PropiedadEstereotipo.objects.none())
        ctx = {'form':form}
        if redirection != None:
            ctx.append({'redirection': redirection})
        return render(request, self.template_name, ctx)

    def post(self, *args, **kwargs):
        form = EstereotipoProductoForm(self.request.POST)
        propiedades = self.PropiedadEstereotipoFormSet(self.request.POST)
        redirection = self.request.POST.get('redirection', SUCCESS_URL)
        if form.is_valid() and propiedades.is_valid():
            form.save()
            for propiedad in propiedades:
                propiedad.instance.estereotipo = form.instance
            propiedades.save()
            return redirect(redirection)

        form.propiedad_form = propiedades
        return render(self.request, self.template_name, {'form': form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('core.can_add_estereotipoproducto'))
    def dispatch(self, *args, **kwargs):
        return super(CreateView, self).dispatch(*args, **kwargs)


class DetailView(DetailView):
    model = EstereotipoProducto
    context_object_name = 'producto'
    template_name = 'core/producto/show.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(DetailView, self).dispatch(*args, **kwargs)

class UpdateView(View):
    model = EstereotipoProducto
    form_class = EstereotipoProductoForm
    context_object_name = 'producto'
    success_url = SUCCESS_URL
    template_name = 'core/producto/create.html'

    def __init__(self, *args, **kwargs):
        super(UpdateView, self).__init__(*args, **kwargs)
        self.PropiedadEstereotipoFormSet = modelformset_factory(PropiedadEstereotipo,
                                                             can_delete=True,
                                                             max_num=20,
                                                             validate_max=True,
                                                             fields= ('id', 'propiedad', 'valor')
                                                        )

    def get(self, request, pk):
        estereotipo = get_object_or_404(EstereotipoProducto, pk=pk)
        form = EstereotipoProductoForm(instance=estereotipo)
        form.propiedad_form = self.PropiedadEstereotipoFormSet(queryset=estereotipo.propiedades_estereotipos.all())
        return render(request, self.template_name, {'form':form})

    def post(self, *args, **kwargs):
        estereotipo = get_object_or_404(EstereotipoProducto, pk=kwargs['pk'])
        form = EstereotipoProductoForm(self.request.POST, instance=estereotipo)
        propiedades = self.PropiedadEstereotipoFormSet(self.request.POST)
        for propiedad in propiedades:
            propiedad.instance.estereotipo = estereotipo
        if form.is_valid() and propiedades.is_valid():
            form.save()
            propiedades.save()
            return redirect(SUCCESS_URL)

        form.propiedad_form = propiedades
        return render(self.request, self.template_name, {'form': form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('core.can_change_estereotipoproducto'))
    def dispatch(self, *args, **kwargs):
        return super(UpdateView, self).dispatch(*args, **kwargs)


class DeleteView(DV):
    model = EstereotipoProducto
    context_object_name = 'producto'
    success_url = SUCCESS_URL
    template_name = 'core/producto/delete.html'

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('core.can_delete_estereotipoproducto'))
    def dispatch(self, *args, **kwargs):
        return super(DeleteView, self).dispatch(*args, **kwargs)
