from django.core.urlresolvers import reverse_lazy
from django.contrib import messages

from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView


from ..models import Propiedad
from ..forms.EstereotipoProducto import PropiedadForm


SUCCESS_URL = reverse_lazy( 'producto.propiedad.list' )


class ListView(ListView):
    context_object_name = 'propiedades'
    model = Propiedad
    template_name = 'core/propiedad/list.html'


class CreateView(CreateView):
    model = Propiedad
    form_class = PropiedadForm
    success_url = SUCCESS_URL
    template_name = 'core/propiedad/form.html'


class DetailView(DetailView):
    model = Propiedad
    context_object_name = 'propiedad'
    template_name = 'core/propiedad/detail.html'


class UpdateView(UpdateView):
    model = Propiedad
    form_class = PropiedadForm
    context_object_name = 'propiedad'
    success_url = SUCCESS_URL
    template_name = 'core/propiedad/form.html'


class DeleteView(DeleteView):
    model = Propiedad
    context_object_name = 'propiedad'
    success_url = SUCCESS_URL
    template_name = 'core/propiedad/delete.html'
