from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from django.forms import modelformset_factory
from django.views.generic import View
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView

from ..models import CategoriaProducto
from ..forms.CategoriaProducto import CategoriaProductoForm


SUCCESS_URL = reverse_lazy('producto.categoria.list')

def permissions_show(user):
    return user.has_perm('core.can_add_categoriaproducto') or user.has_perm('core.can_change_categoriaproducto') or user.has_perm('core.can_delete_categoriaproducto')

class ListView(ListView):
    context_object_name = 'categorias'
    model = CategoriaProducto
    page_kwarg = 'page'
    paginate_by = 10
    template_name = 'core/producto/categoria/list.html'

    def get_queryset(self):
        return CategoriaProducto.objects.filter(parent__isnull=True)

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(ListView, self).dispatch(*args, **kwargs)


class CreateView(View):
    success_url = SUCCESS_URL
    template_name = 'core/producto/categoria/create.html'

    def get(self, *args, **kwargs):
        parent_id = self.request.GET.get('parent', None)
        initial = {}
        if parent_id != None:
            parent = get_object_or_404(CategoriaProducto, pk=parent_id)
            initial = {'parent': parent}
        form = CategoriaProductoForm(initial=initial)
        return render(self.request, self.template_name, {'form':form})

    def post(self, *args, **kwargs):
        form = CategoriaProductoForm(self.request.POST, self.request.FILES)
        if form.is_valid():
            form.save()
            return redirect( self.success_url )
        return render(self.request, self.template_name, {'form': form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('core.can_add_categoriaproducto'))
    def dispatch(self, *args, **kwargs):
        return super(CreateView, self).dispatch(*args, **kwargs)


class DetailView(DetailView):
    model = CategoriaProducto
    context_object_name = 'categoria'
    template_name = 'core/producto/categoria/show.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(DetailView, self).dispatch(*args, **kwargs)


class CategoriaProductoUpdateView(UpdateView):
    model = CategoriaProducto
    form_class = CategoriaProductoForm
    context_object_name = 'categoria'
    success_url = SUCCESS_URL
    template_name = 'core/producto/categoria/create.html'

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('core.can_change_categoriaproducto'))
    def dispatch(self, *args, **kwargs):
        return super(CategoriaProductoUpdateView, self).dispatch(*args, **kwargs)


class UpdateView(View):
    model = CategoriaProducto
    form_class = CategoriaProductoForm
    context_object_name = 'categoria'
    success_url = SUCCESS_URL
    template_name = 'core/producto/categoria/create.html'

    def get(self, *args, **kwargs):
        categoria = get_object_or_404(CategoriaProducto, pk=kwargs['pk'])
        form = CategoriaProductoForm(instance=categoria)
        return render(self.request, self.template_name, {'form':form})


    def post(self, *args, **kwargs):
        categoria = get_object_or_404(CategoriaProducto, pk=kwargs['pk'])
        form = CategoriaProductoForm(self.request.POST, self.request.FILES, instance=categoria)
        if form.is_valid():
            form.save()
            return redirect( self.success_url )

        return render(self.request, self.template_name, {'form': form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('core.can_change_categoriaproducto'))
    def dispatch(self, *args, **kwargs):
        return super(UpdateView, self).dispatch(*args, **kwargs)


class DeleteView(DeleteView):
    model = CategoriaProducto
    context_object_name = 'categoria'
    success_url = SUCCESS_URL
    template_name = 'core/producto/categoria/delete.html'

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('core.can_delete_categoriaproducto'))
    def dispatch(self, *args, **kwargs):
        return super(DeleteView, self).dispatch(*args, **kwargs)
