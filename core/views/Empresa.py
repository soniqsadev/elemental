from django.views.generic.list import ListView as GenericListView
from django.views.generic.detail import DetailView as GenericDetailView
from django.views.generic.edit import DeleteView as GenericDeleteView
from django.views.generic import View
from django.forms import modelformset_factory
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse_lazy

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from core.models import Telefono
from core.views import Moral as MoralViews
from core.models import Moral as MoralModel
from ..models import Empresa
from ..forms.Empresa import EmpresaForm

SUCCESS_URL = reverse_lazy('empresa.list')

def permissions_show(user):
    return user.has_perm('core.can_add_empresa') or user.has_perm('core.can_change_empresa') or user.has_perm('core.can_delete_empresa')

class ListView(GenericListView):
    context_object_name = 'empresas'
    model = Empresa
    page_kwarg = 'page'
    paginate_by = 10
    template_name = 'core/empresa/list.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(ListView, self).dispatch(*args, **kwargs)


class CreateView(View):
    template_name = 'core/empresa/create.html'
    success_url = SUCCESS_URL

    def __init__(self, *args, **kwargs):
        super(CreateView, self).__init__(*args, **kwargs)
        self.TelefonoFormSet = modelformset_factory(Telefono,
                                                         can_delete=True,
                                                         max_num=5,
                                                         min_num=0,
                                                         validate_max=True,
                                                         fields= ('id', 'tipo', 'numero')
                                                    )

    def get(self, *args, **kwargs):
        form = EmpresaForm()
        form.telefono_form = self.TelefonoFormSet(queryset=Telefono.objects.none())
        return render(self.request, self.template_name, {'form':form})

    def post(self, *args, **kwargs):
        form = EmpresaForm(self.request.POST, self.request.FILES)
        telefonos = self.TelefonoFormSet(self.request.POST)
        if form.is_valid() and telefonos.is_valid():
            form.instance.is_empresa = True
            form.save()
            for telefono in telefonos:
                telefono.instance.persona = form.instance
            telefonos.save()
            return redirect( self.success_url )
        form.telefono_form = telefonos
        return render(self.request, self.template_name, {'form': form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('core.can_add_empresa'))
    def dispatch(self, *args, **kwargs):
        return super(CreateView, self).dispatch(*args, **kwargs)


class DetailView(GenericDetailView):
    model = Empresa
    context_object_name = 'empresa'
    template_name = 'core/empresa/show.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(DetailView, self).dispatch(*args, **kwargs)


class UpdateView(View):
    template_name = 'core/empresa/create.html'
    success_url = SUCCESS_URL

    def __init__(self, *args, **kwargs):
        super(UpdateView, self).__init__(*args, **kwargs)
        self.TelefonoFormSet = modelformset_factory(Telefono,
                                                         can_delete=True,
                                                         max_num=5,
                                                         min_num=0,
                                                         validate_max=True,
                                                         fields= ('id', 'tipo', 'numero')
                                                    )

    def get(self, *args, **kwargs):
        empresa = get_object_or_404(Empresa, pk=kwargs['pk'])
        form = EmpresaForm(instance=empresa)
        form.telefono_form = self.TelefonoFormSet(queryset=empresa.telefonos.all())
        return render(self.request, self.template_name, {'form':form})


    def post(self, *args, **kwargs):
        empresa = get_object_or_404(Empresa, pk=kwargs['pk'])
        form = EmpresaForm(self.request.POST, self.request.FILES, instance=empresa)
        telefonos = self.TelefonoFormSet(self.request.POST)
        for telefono in telefonos:
            telefono.instance.persona = empresa
        if form.is_valid() and telefonos.is_valid():
            form.save()
            telefonos.save()
            return redirect( self.success_url )

        form.telefono_form = telefonos
        return render(self.request, self.template_name, {'form': form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('core.can_change_empresa'))
    def dispatch(self, *args, **kwargs):
        return super(UpdateView, self).dispatch(*args, **kwargs)


class DeleteView(GenericDeleteView):
    model = Empresa
    context_object_name = 'empresa'
    success_url = 'empresa.list'
    template_name = 'core/empresa/delete.html'

    def get_context_data(self, **kwargs):
        ctx = super(DeleteView, self).get_context_data(**kwargs)
        ctx['route_success'] = self.success_url
        return ctx

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('core.can_delete_empresa'))
    def dispatch(self, *args, **kwargs):
        return super(DeleteView, self).dispatch(*args, **kwargs)
