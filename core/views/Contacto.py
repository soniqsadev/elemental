from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import modelformset_factory

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from django.views.generic import View

from ..models import Contacto, Moral, Telefono
from ..forms.Contacto import ContactoForm

class CreateContactoView(View):

    def __init__(self, *args, **kwargs):
        super(CreateContactoView, self).__init__(*args, **kwargs)
        self.TelefonoFormSet = modelformset_factory(Telefono,
                                                         can_delete=True,
                                                         max_num=5,
                                                         min_num=0,
                                                         validate_max=True,
                                                         fields= ('id', 'tipo', 'numero')
                                                    )


    def get(self, *args, **kwargs):
        moral = get_object_or_404(Moral, pk=kwargs['moral_id'])
        redirection = self.request.GET.get('redirection')
        form = ContactoForm()
        form.telefono_form = self.TelefonoFormSet(queryset=Telefono.objects.none())
        return render(self.request, 'core/contacto/form.html', {'form':form, 'redirection':redirection})

    def post(self, *args, **kwargs):
        moral = get_object_or_404(Moral, pk=kwargs['moral_id'])
        form = ContactoForm(self.request.POST)
        telefonos = self.TelefonoFormSet(self.request.POST)
        url = self.request.POST.get('redirection', reverse_lazy('home'))
        if form.is_valid() and telefonos.is_valid():
            form.instance.persona_moral = moral
            form.save()
            for telefono in telefonos:
                telefono.instance.persona = form.instance
            telefonos.save()
            return redirect( url )

        form.telefono_form = telefonos
        return render(self.request, 'core/contacto/form.html', {'form':form, 'redirection':url})

    # Permisos
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CreateContactoView, self).dispatch(*args, **kwargs)


class UpdateContactoView(View):

    def __init__(self, *args, **kwargs):
        super(UpdateContactoView, self).__init__(*args, **kwargs)
        self.TelefonoFormSet = modelformset_factory(Telefono,
                                                         can_delete=True,
                                                         max_num=5,
                                                         min_num=0,
                                                         validate_max=True,
                                                         fields= ('id', 'tipo', 'numero')
                                                    )

    def get(self, *args, **kwargs):
        moral = get_object_or_404(Moral, pk=kwargs['moral_id'])
        contacto = get_object_or_404(Contacto, pk=kwargs['contacto_id'])
        redirection = self.request.GET.get('redirection')
        form = ContactoForm(instance=contacto)
        form.telefono_form = self.TelefonoFormSet(queryset=contacto.telefonos.all())
        return render(self.request, 'core/contacto/form.html', {'form':form, 'redirection':redirection})

    def post(self, *args, **kwargs):
        moral = get_object_or_404(Moral, pk=kwargs['moral_id'])
        contacto = get_object_or_404(Contacto, pk=kwargs['contacto_id'])
        form = ContactoForm(self.request.POST, instance=contacto)
        telefonos = self.TelefonoFormSet(self.request.POST)
        url = self.request.POST.get('redirection', reverse_lazy('home'))
        for telefono in telefonos:
            telefono.instance.persona = form.instance
        if form.is_valid() and telefonos.is_valid():
            form.save()
            telefonos.save()
            return redirect( url )

        form.telefono_form = telefonos
        return render(self.request, 'core/contacto/form.html', {'form':form, 'redirection':url})

    # Permisos
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UpdateContactoView, self).dispatch(*args, **kwargs)
