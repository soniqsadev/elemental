from django.core.urlresolvers import reverse_lazy

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView

from ..models import MarcaProducto
from ..forms.MarcaProducto import MarcaProductoForm


SUCCESS_URL = reverse_lazy('producto.marca.list')

def permissions_show(user):
    return user.has_perm('core.can_add_marcaproducto') or user.has_perm('core.can_change_marcaproducto') or user.has_perm('core.can_delete_marcaproducto')

class ListView(ListView):
    context_object_name = 'marcas'
    model = MarcaProducto
    page_kwarg = 'page'
    paginate_by = 10
    template_name = 'core/producto/marca/list.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(ListView, self).dispatch(*args, **kwargs)

class CreateView(CreateView):
    model = MarcaProducto
    form_class = MarcaProductoForm
    success_url = SUCCESS_URL
    template_name = 'core/producto/marca/create.html'

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('core.can_add_marcaproducto'))
    def dispatch(self, *args, **kwargs):
        return super(CreateView, self).dispatch(*args, **kwargs)


class DetailView(DetailView):
    model = MarcaProducto
    context_object_name = 'marca'
    template_name = 'core/producto/marca/show.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(DetailView, self).dispatch(*args, **kwargs)


class UpdateView(UpdateView):
    model = MarcaProducto
    form_class = MarcaProductoForm
    context_object_name = 'marca'
    success_url = SUCCESS_URL
    template_name = 'core/producto/marca/create.html'

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('core.can_change_marcaproducto'))
    def dispatch(self, *args, **kwargs):
        return super(UpdateView, self).dispatch(*args, **kwargs)


class DeleteView(DeleteView):
    model = MarcaProducto
    context_object_name = 'marca'
    success_url = SUCCESS_URL
    template_name = 'core/producto/marca/delete.html'

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('core.can_delete_marcaproducto'))
    def dispatch(self, *args, **kwargs):
        return super(DeleteView, self).dispatch(*args, **kwargs)
