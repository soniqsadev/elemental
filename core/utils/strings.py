from string import ascii_lowercase, ascii_uppercase, digits
import random

def key_generator(elements=digits, size=8):
    return ''.join(random.choice(elements) for _ in range(size))

def key_generator_lower(size=16, elements=ascii_lowercase + digits):
    return key_generator(size, elements)

def key_generator_upper(size=16, elements=ascii_uppercase + digits):
	return key_generator(size, elements)

def key_from_dic(dic):
    print ( '[%s]' % ', '.join(map(str, dic)) )
