import subprocess

from django.conf import settings
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse

from ..models import EstereotipoProducto as Producto

class PDFResponse(object):
    """
    An HttpResponse that renders its content into PDF.
    """
    def __init__(self, data=None, **kwargs):
        self.response = HttpResponse(data, content_type='application/pdf')
        filename = kwargs.get('filename', 'default')
        self.response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % filename

    def get(self):
        return self.response

class RouteToPDF(object):
    """
    Clase para convertir rutas en PDF
    """

    def __init__(self, route, **kwargs):
        self.route = route
        self.orientation = kwargs.get('orientation','Landscape')
        self.args = kwargs.get('args',[])
        self.papersize = kwargs.get('papersize', 'Letter')
        self.host = kwargs.get('host', settings.LOCAL_HOST)

    def generate(self, filename):
        command = "%s -O %s -s %s -T 0 -R 0 -B 0 -L 0 %s%s -" % (settings.WKHTMLTOPDF_BIN, self.orientation, self.papersize, self.host, reverse_lazy(self.route, args=self.args))
        pdf_contents = subprocess.check_output(command.split())
        response = PDFResponse(pdf_contents, filename=filename).get()
        return response
