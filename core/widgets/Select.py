from django.conf import settings
from django.forms import widgets
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe


class SelectSearchable(widgets.Select):

    def __init__(self, **kwargs):
        attrs = kwargs.pop('attrs', None)

        super(SelectSearchable, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        attrs = attrs or {}
        attrs['class'] = 'form-control searchable input-sm'
        attrs['data-placeholder'] = 'Selecciona una opción'

        select = super(SelectSearchable, self).render(name, value, attrs)

        return render_to_string('core/widgets/select_searchable.html', {
            'field_name': name,
            'field_select': mark_safe(select),
        })

    class Media:
        js = (
            settings.STATIC_URL + 'js/core/widgets/select_searchable.js',
        )


class SelectMultipleSearchable(widgets.SelectMultiple):
    def __init__(self, **kwargs):
        attrs = kwargs.pop('attrs', None)

        super(SelectMultipleSearchable, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        attrs = attrs or {}
        attrs['class'] = 'form-control searchable input-sm'
        attrs['data-placeholder'] = 'Selecciona una opción'

        select = super(SelectMultipleSearchable, self).render(name,
                                                              value,
                                                              attrs)

        return render_to_string('core/widgets/select_searchable.html', {
            'field_name': name,
            'field_select': mark_safe(select),
        })

    class Media:
        js = (
            settings.STATIC_URL + 'js/core/widgets/select_searchable.js',
        )


class SelectAddMore(widgets.Select):
    def __init__(self, **kwargs):
        attrs = kwargs.pop('attrs', None)

        super(SelectAddMore, self).__init__(attrs)

        self.ruta = kwargs.pop('ruta')
        self.form = kwargs.pop('form')

        kv = kwargs.pop('kv')
        self.key = kv['key']
        self.value = kv['value']

    def render(self, name, value, attrs=None):

        attrs = attrs or {}
        attrs['class'] = 'form-control input-sm'

        select = super(SelectAddMore, self).render(name, value, attrs)

        return render_to_string('core/widgets/select_addmore.html', {
            'field_name': name,
            'field_select': mark_safe(select),
            'ruta': self.ruta,
            'form': self.form,
            'key': self.key,
            'value': self.value,
        })

    class Media:
        js = (
            settings.STATIC_URL + 'js/core/widgets/select_addmore.js',
        )


class SelectDependent(widgets.Select):
    def __init__(self, **kwargs):
        attrs = kwargs.pop('attrs', None)

        self.url = kwargs.pop('url')
        self.target_id = kwargs.pop('target_id')
        self.kv = kwargs.pop('kv')

        super(SelectDependent, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        if 'modal' in name and '_modal-' not in self.target_id:
            self.target_id = '_modal-'.join(self.target_id.split('_'))
        attrs = attrs or {}
        attrs['class'] = 'form-control input-sm'
        attrs['dependent'] = ''
        select = super(SelectDependent, self).render(name, value, attrs)

        return render_to_string('core/widgets/select_dependent.html', {
            'field_name': name,
            'field_select': mark_safe(select),
            'target_id': self.target_id,
            'url': self.url,
            'value': value,
            'dependent_field_key': self.kv['key'],
            'dependent_field_value': self.kv['value'],
        })

    class Media:
        js = (
            settings.STATIC_URL + 'js/core/widgets/select_dependent.js',
        )


class SelectSearchableDependent(widgets.Select):
    def __init__(self, **kwargs):
        attrs = kwargs.pop('attrs', None)

        self.url = kwargs.pop('url')
        self.target_id = kwargs.pop('target_id')
        self.kv = kwargs.pop('kv')

        super(SelectSearchableDependent, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        if 'modal' in name and '_modal-' not in self.target_id:
            self.target_id = '_modal-'.join(self.target_id.split('_'))
        attrs = attrs or {}
        attrs['class'] = 'form-control searchable input-sm'
        attrs['dependent'] = ''
        select = super(SelectSearchableDependent, self).render(name,
                                                               value,
                                                               attrs)

        return render_to_string('core/widgets/select_dependent.html', {
            'field_name': name,
            'field_select': mark_safe(select),
            'target_id': self.target_id,
            'url': self.url,
            'value': value,
            'dependent_field_key': self.kv['key'],
            'dependent_field_value': self.kv['value'],
        })

    class Media:
        js = (
            settings.STATIC_URL + 'js/core/widgets/select_dependent.js',
        )
