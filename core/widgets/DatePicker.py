from django.conf import settings
from django.forms import widgets
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe

class DatePicker(widgets.DateInput):
    def __init__(self, **kwargs):
        attrs = kwargs.pop('attrs', None)

        super(DatePicker, self).__init__(attrs)


    def render(self, name, value, attrs=None):

        attrs = attrs or {}
        attrs['class'] = 'form-control input-sm'

        select = super(DatePicker, self).render(name, value, attrs)

        return render_to_string('core/widgets/datepicker.html', {
            'field_name': name,
            'field_input': mark_safe(select),
        })

    class Media:
        js = (
            settings.STATIC_URL + 'js/core/widgets/datepicker.js',
        )


class DateTimePicker(widgets.DateTimeInput):
    def __init__(self, **kwargs):
        attrs = kwargs.pop('attrs', None)

        super(DateTimePicker, self).__init__(attrs)


    def render(self, name, value, attrs=None):

        attrs = attrs or {}
        attrs['class'] = 'form-control input-sm'

        select = super(DateTimePicker, self).render(name, value, attrs)

        return render_to_string('core/widgets/datetimepicker.html', {
            'field_name': name,
            'field_input': mark_safe(select),
        })

    class Media:
        js = (
            settings.STATIC_URL + 'js/core/widgets/datetimepicker.js',
        )
