import copy

from . import Moral as MoralViews
from ..models import Moral as MoralModel

copy_conf = copy.deepcopy(MoralViews.conf)

copy_conf['URL_SUCCESS'] = 'empresa.list'
copy_conf['URL_CREATE'] = 'empresa.create'
copy_conf['URL_DETAIL'] = 'empresa.detail'
copy_conf['URL_EDIT'] = 'empresa.update'
copy_conf['URL_DELETE'] = 'empresa.delete'

copy_conf['VERBOSE_NAME'] = 'Empresa'
copy_conf['VERBOSE_NAME_PLURAL'] = 'Empresas'

class ListView(MoralViews.ListView):
    conf = copy_conf
    queryset = MoralModel.objects.filter(is_me=True)

class CreateView(MoralViews.CreateView):
    conf = copy_conf

    def form_valid(self, form):
        form.instance.is_me = True
        return super(CreateView, self).form_valid(form)


class DetailView(MoralViews.DetailView):
    conf = copy_conf

class UpdateView(MoralViews.UpdateView):
    conf = copy_conf

class DeleteView(MoralViews.DeleteView):
    conf = copy_conf
