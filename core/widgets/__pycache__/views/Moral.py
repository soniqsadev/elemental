from django.core.urlresolvers import reverse_lazy

from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView

from ..models import Moral
from ..forms.Moral import MoralForm

conf= {
    'URL_SUCCESS' : 'moral.list',
    'URL_CREATE' : 'moral.create',
    'URL_DETAIL' : 'moral.detail',
    'URL_EDIT' : 'moral.update',
    'URL_DELETE' : 'moral.delete',

    'VERBOSE_NAME' : 'Persona modal',
    'VERBOSE_NAME_PLURAL' : 'Personas morales',
}


class ListView(ListView):
    conf = conf
    context_object_name = 'objects'
    model = Moral
    page_kwarg = 'page'
    paginate_by = 10
    template_name = 'core/moral/list.html'

    def get_context_data(self, **kwargs):
        ctx = super(ListView, self).get_context_data(**kwargs)
        ctx['titulo'] = self.conf['VERBOSE_NAME_PLURAL']
        ctx['route_create'] = self.conf['URL_CREATE']
        ctx['route_detail'] = self.conf['URL_DETAIL']
        ctx['route_edit'] = self.conf['URL_EDIT']
        ctx['route_delete'] = self.conf['URL_DELETE']
        return ctx

class CreateView(CreateView):
    conf = conf
    model = Moral
    form_class = MoralForm
    template_name = 'core/moral/create.html'

    def get_context_data(self, **kwargs):
        ctx = super(CreateView, self).get_context_data(**kwargs)
        ctx['titulo'] = self.conf['VERBOSE_NAME']
        return ctx

    def get_success_url(self):
        return reverse_lazy(self.conf['URL_SUCCESS'])


class DetailView(DetailView):
    conf = conf
    model = Moral
    context_object_name = 'object'
    template_name = 'core/moral/show.html'

    def get_context_data(self, **kwargs):
        ctx = super(DetailView, self).get_context_data(**kwargs)
        ctx['titulo'] = self.conf['VERBOSE_NAME']
        ctx['route_success'] = self.conf['URL_SUCCESS']
        ctx['route_edit'] = self.conf['URL_EDIT']
        return ctx


class UpdateView(UpdateView):
    conf = conf
    model = Moral
    form_class = MoralForm
    context_object_name = 'object'
    template_name = 'core/moral/create.html'

    def get_context_data(self, **kwargs):
        ctx = super(UpdateView, self).get_context_data(**kwargs)
        ctx['titulo'] = self.conf['VERBOSE_NAME']
        return ctx

    def get_success_url(self):
        return reverse_lazy(self.conf['URL_SUCCESS'])


class DeleteView(DeleteView):
    conf = conf
    model = Moral
    context_object_name = 'object'
    template_name = 'core/moral/delete.html'

    def get_context_data(self, **kwargs):
        ctx = super(DeleteView, self).get_context_data(**kwargs)
        ctx['route_success'] = self.conf['URL_SUCCESS']
        return ctx

    def get_success_url(self):
        return reverse_lazy(self.conf['URL_SUCCESS'])
