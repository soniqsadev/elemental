from django.shortcuts import render

from django.views.generic.base import TemplateView
from django.views.generic import View

from ..utils.pdf import RouteToPDF

from ..models import (
    MetaInformacion,
    CategoriaProducto,
    EstereotipoProducto,
    MarcaProducto,
    ModeloProducto,
    Producto,
    UnidadMedicion,
)

class IndexTemplateView(TemplateView):
    template_name = 'body.html'

class ConfigTemplateView(View):
    """
    Clase para la configuración basica del sistema
    """
    template_name = 'core/configuracion.html'

    def get(self, request):
        metas = MetaInformacion.objects.all()
        return render(request, self.template_name, {'metas':metas})

    def post(self, request):
        for value in request.POST:
            if value != 'csrfmiddlewaretoken':
                obj = MetaInformacion.objects.get(pk=value)
                obj.valor = request.POST.get(value)
                obj.save()
        metas = MetaInformacion.objects.all()
        return render(request, self.template_name, {'metas':metas})

def test_pdf(request):
    return RouteToPDF('invoce_pdf').generate('test')


class TestTemplateView(TemplateView):
    template_name = 'pdf/invoice.html'
