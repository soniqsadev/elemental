from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import ValidationError
from django.shortcuts import render, redirect, get_object_or_404

from django.views.generic import View
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView as CV, UpdateView as UV, DeleteView as DV
from django.views.generic.detail import DetailView

from django.forms.formsets import formset_factory
from django.forms import modelformset_factory

from ..models import EstereotipoProducto, PropiedadProducto
from ..forms.EstereotipoProducto import EstereotipoProductoForm
from ..forms.PropiedadProducto import PropiedadProductoForm


SUCCESS_URL = reverse_lazy('productos.list')

class ListView(ListView):
    context_object_name = 'productos'
    model = EstereotipoProducto
    page_kwarg = 'page'
    paginate_by = 10
    template_name = 'core/producto/list.html'

class CreateView(View):

    template_name = 'core/producto/create.html'

    def __init__(self, *args, **kwargs):
        super(CreateView, self).__init__(*args, **kwargs)
        self.PropiedadProductoFormSet = modelformset_factory(PropiedadProducto,
                                                             can_delete=True,
                                                             max_num=20,
                                                             min_num=0,
                                                             validate_max=True,
                                                             fields= ('id', 'propiedad', 'valor')
                                                        )

    def get(self, request):
        form = EstereotipoProductoForm()
        form.propiedad_form = self.PropiedadProductoFormSet(queryset=PropiedadProducto.objects.none())
        return render(request, self.template_name, {'form':form})

    def post(self, request):
        form = EstereotipoProductoForm(request.POST)
        propiedades = self.PropiedadProductoFormSet(request.POST)
        if form.is_valid() and propiedades.is_valid():
            form.save()
            for propiedad in propiedades:
                propiedad.instance.estereotipo = form.instance
            propiedades.save()
            return redirect(SUCCESS_URL)

        form.propiedad_form = propiedades
        return render(request, self.template_name, {'form': form})


class DetailView(DetailView):
    model = EstereotipoProducto
    context_object_name = 'producto'
    template_name = 'core/producto/show.html'

class UpdateView(View):
    model = EstereotipoProducto
    form_class = EstereotipoProductoForm
    context_object_name = 'producto'
    success_url = SUCCESS_URL
    template_name = 'core/producto/create.html'

    def __init__(self, *args, **kwargs):
        super(UpdateView, self).__init__(*args, **kwargs)
        self.PropiedadProductoFormSet = modelformset_factory(PropiedadProducto,
                                                             can_delete=True,
                                                             max_num=20,
                                                             validate_max=True,
                                                             fields= ('id', 'propiedad', 'valor')
                                                        )

    def get(self, request, pk):
        estereotipo = get_object_or_404(EstereotipoProducto, pk=pk)
        form = EstereotipoProductoForm(instance=estereotipo)
        form.propiedad_form = self.PropiedadProductoFormSet(queryset=estereotipo.propiedades.all())
        return render(request, self.template_name, {'form':form})

    def post(self, request, pk):
        estereotipo = get_object_or_404(EstereotipoProducto, pk=pk)
        form = EstereotipoProductoForm(request.POST, instance=estereotipo)
        propiedades = self.PropiedadProductoFormSet(request.POST)
        for propiedad in propiedades:
            propiedad.instance.estereotipo = estereotipo
        if form.is_valid() and propiedades.is_valid():
            form.save()
            propiedades.save()
            return redirect(SUCCESS_URL)

        form.propiedad_form = propiedades
        return render(request, self.template_name, {'form': form})

class DeleteView(DV):
    model = EstereotipoProducto
    context_object_name = 'producto'
    success_url = SUCCESS_URL
    template_name = 'core/producto/delete.html'
