angular.module('main')
.controller('ConfiguracionController', function ($scope, $route, $location) {
    $scope.saludo = 'Hola! :-)';
    $scope.route = $route.current;
    $scope.url = $location.path();
})

//   PRODUCTOS
.controller('indexProductoController', function ($scope, ProductoResource) {
    $scope.productos = ProductoResource.query();
})
.controller('CreateProductoController', function ($scope, $location, $http, $compile,
                                                    ProductoResource, HttpLocal, Categoria, Marca, UnidadMedicion,
                                                    ProcessorTemplatesEJS
                                                ) {
    $scope.categorias = Categoria.query();
    $scope.unidades = UnidadMedicion.query();
    $scope.marcas = Marca.query();
    $scope.producto = {};
    $scope.producto.modelos = [{}];
    $scope.producto.unidades_medicion = [];

    HttpLocal.get('/unidades-medicion/unidades/')
    .then(function (res) {
        $scope.dimensiones = JSON.parse(res.data);
    }, function () {
        Materialize.toast('Se ha producido un error, recargue la página por favor', 4000);
    });

    $scope.saveCategoria = function () {
        $scope.categorias = Categoria.save($scope.nueva_categoria, $scope);
    };

    $scope.saveUnidad = function () {
        $scope.unidades = UnidadMedicion.save($scope.nueva_unidad, $scope);
    };

    $scope.saveMarca = function () {
        $scope.marcas = Marca.save($scope.nueva_marca, $scope);
    }

    $scope.addRow = function (model_to_add) {
        console.log($scope.producto.modelos.length);

        model_to_add = model_to_add || {};

        HttpLocal.get('/static/js/templates/producto/partials/new_modelo.ejs')
        .then(function (res) {
            // crear json con data
            jonson = {'modelo' : 'producto.modelos['+ $scope.producto.modelos.length +']'}
            // agregar el nuevo elemento al arreglo modelos
            $scope.producto.modelos.push(model_to_add);
            // agregar el resultado como una fila de la tabla
            datos = ProcessorTemplatesEJS.build(res.data, jonson, $scope);
            // insertar en el DOM
            $(datos).insertBefore('#row-addmore');
        }, function () {
            H34.genericError();
        })
    }

    $scope.removeRow = function ($event) {
        row = $event.currentTarget.parentNode.parentNode;
        indice = row.getAttribute('data-model').match(/\[[0-9]+\]/)[0].match(/[0-9]/)[0];
        rows = $(row).parent().children();

        if (rows.length > 2) {
            rows[rows.length-2].remove();
            $scope.producto.modelos.splice(indice, 1);
        }else{
            $scope.tiene_modelos = false;
        }
    }

    // Create
    $scope.saveProducto = function () {
        console.log('save');
        if ($scope.tiene_modelos != true && (typeof $scope.producto.modelos[0].nombre === 'undefined' || $scope.producto.modelos[0].nombre.length == 0) ) {
            $scope.producto.modelos[0].nombre = 'default';
        }
        // llena el arreglo de unidades de mediciones permitidas para el estereotipo de producto
        for (var i = 0; i < $scope.producto.modelos.length; i++) {
            $scope.producto.unidades_medicion.push($scope.producto.modelos[i].unidad_medicion);
        }
        console.log('producto', $scope.producto);
        ProductoResource.save($scope.producto, function (data) {
            console.log(data);
            $location.path('/configuracion/producto');
        }, function (data) {
            console.log('fail', data);
        });
    }
})
.controller('OneProductoController', function ($scope, $location, HttpLocal, ProductoResource, $routeParams, CategoriaProductoResource,
                                                UnidadesMedicionResource, MarcaProductoResource, Categoria, Marca, UnidadMedicion) {
    $scope.categorias = CategoriaProductoResource.query();
    $scope.unidades = UnidadesMedicionResource.query();
    $scope.marcas = MarcaProductoResource.query();

    ProductoResource.get({id:$routeParams.id}, function (data) {
        // Modificar los indices de enteros a cadenas de texto
        data.categoria = data.categoria.toString();
        for (var i = 0; i < data.modelos.length; i++) {
            data.modelos[i].marca = data.modelos[i].marca.toString();
            data.modelos[i].unidad_medicion = data.modelos[i].unidad_medicion.toString();
        }
        $scope.producto = data;
        console.log(data);
        $scope.tiene_modelos = $scope.producto.modelos.length > 1;
    });

    HttpLocal.get('/unidades-medicion/unidades/')
    .then(function (res) {
        $scope.dimensiones = JSON.parse(res.data);
    }, function () {
        Materialize.toast('Se ha producido un error, recargue la página por favor', 4000);
    });

    $scope.saveCategoria = function () {
        $scope.categorias = Categoria.save($scope.nueva_categoria);
        Categoria.updateUI($scope);
    };

    $scope.saveUnidad = function () {
        $scope.unidades = UnidadMedicion.save($scope.nueva_unidad);
        UnidadMedicion.updateUI($scope);
    };

    $scope.saveMarca = function () {
        $scope.marcas = Marca.save($scope.nueva_marca);
        Marca.updateUI($scope);
    }

    $scope.saveProducto = function () {
        console.log('save');
        if ($scope.tiene_modelos != true && (typeof $scope.producto.modelos[0].nombre === 'undefined' || $scope.producto.modelos[0].nombre.length == 0) ) {
            $scope.producto.modelos[0].nombre = 'default';
        }
        // llena el arreglo de unidades de mediciones permitidas para el estereotipo de producto
        $scope.producto.unidades_medicion = [];
        for (var i = 0; i < $scope.producto.modelos.length; i++) {
            $scope.producto.unidades_medicion.push($scope.producto.modelos[i].unidad_medicion);
        }
        console.log('producto', $scope.producto);
        ProductoResource.update({id:$scope.producto.id}, $scope.producto, function (data) {
            console.log(data);
            $location.path('/configuracion/producto');
        }, function (data) {
            console.log('fail', data);
        });
    }
})
;
