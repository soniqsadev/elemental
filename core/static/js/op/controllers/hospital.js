angular.module('main')
.controller('IndexHospitalController', function ($scope, HospitalResource) {
    // List
    $scope.hospitales = HospitalResource.query();
    // Delete
    $scope.removeHospital = function (hospital) {
        HospitalResource.delete({id: hospital.id}, function(data){
            console.log(data);
            $scope.hospitales = HospitalResource.query();
        });
    }
})
.controller('ShowHospitalController', function ($scope, $location, $routeParams, HospitalResource) {
    // Show
    $scope.hospital = HospitalResource.get({id:$routeParams.id});
    // Update
    $scope.saveHospital = function () {
        HospitalResource.update({ id:$scope.hospital.id },$scope.hospital, function (data) {
            console.log(data);
            $location.path('/hospital/' + $scope.hospital.id );
        });
    }
})
.controller('CreateHospitalController', function ($scope, $location, HospitalResource) {
    // Create
    $scope.saveHospital = function () {
        HospitalResource.save($scope.hospital, function (data) {
            console.log(data);
            $location.path('/hospital');
        });
    }
})
;
