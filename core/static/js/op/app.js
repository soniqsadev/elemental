angular.module('main', ['ngRoute', 'ngResource', 'ui.materialize'])
.config(function($routeProvider, $httpProvider, $resourceProvider){


    // Configuración para token de ajax
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    //  No quitar el slash al final de las url cuando se haga
    // peticiones con resourceProvider
    $resourceProvider.defaults.stripTrailingSlashes = false;

    // Configurar rutas con routeProvider
    $routeProvider
        .when('/', {
            controller: 'IndexHospitalController',
            templateUrl: '/static/js/templates/index.html'
        })
        // Productos
        .when('/configuracion/producto',{
            controller: 'indexProductoController',
            templateUrl: '/static/js/templates/producto/list.html'
        })
        .when('/configuracion/producto/create',{
            controller: 'CreateProductoController',
            templateUrl: '/static/js/templates/producto/edit.html'
        })
        .when('/configuracion/producto/:id/edit',{
            controller: 'OneProductoController',
            templateUrl: '/static/js/templates/producto/edit.html'
        })
        // Configuración
        .when('/configuracion',{
            controller: 'ConfiguracionController',
            templateUrl: '/static/js/templates/configuracion.html'
        })
        // Hospitales
        .when('/hospital',{
            controller: 'IndexHospitalController',
            templateUrl: '/static/js/templates/hospital/list.html'
        })
        .when('/hospital/create',{
            controller: 'CreateHospitalController',
            templateUrl: '/static/js/templates/hospital/edit.html'
        })
        .when('/hospital/:id/edit',{
            controller: 'ShowHospitalController',
            templateUrl: '/static/js/templates/hospital/edit.html'
        })
        .when('/hospital/:id', {
            controller: 'ShowHospitalController',
            templateUrl: '/static/js/templates/hospital/show.html'
        })
        // Default
        .otherwise('/')
        ;
})
.run(function ($rootScope, $location) {
    $rootScope.$on('$locationChangeSuccess', function(event, nuevo, anterior){
        H34.addClassActive($location.path());
    })
})
;
