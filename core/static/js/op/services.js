angular.module('main')
.factory("HospitalResource", function ($resource) {
    return $resource(window.location.protocol + '//' + window.location.host + '/hospital/:id/', {id: '@id'}, { update:{method:'patch'} }, { stripTrailingSlashes: false });
})
.factory("ProductoResource", function ($resource) {
    return $resource(window.location.protocol + '//' + window.location.host + '/producto/:id/', {id: '@id'}, { update:{method:'patch'} }, { stripTrailingSlashes: false });
})
.factory("UnidadesMedicionResource", function ($resource) {
    return $resource(window.location.protocol + '//' + window.location.host + '/unidades-medicion/:id/', {id: '@id'}, { update:{method:'patch'} }, { stripTrailingSlashes: false });
})
.factory("DimensionesMedicionResource", function ($resource) {
    return $resource(window.location.protocol + '//' + window.location.host + '/unidades-medicion/unidades/:id/', {id: '@id'}, { update:{method:'patch'} }, { stripTrailingSlashes: false });
})
.factory("MarcaProductoResource", function ($resource) {
    return $resource(window.location.protocol + '//' + window.location.host + '/producto/marca/:id/', {id: '@id'}, { update:{method:'patch'} }, { stripTrailingSlashes: false });
})
.factory("CategoriaProductoResource", function ($resource) {
    return $resource(window.location.protocol + '//' + window.location.host + '/producto/categoria/:id/', {id: '@id'}, { update:{method:'patch'} }, { stripTrailingSlashes: false });
})
.service('ProcessorTemplatesEJS', function ($compile) {
    this.build = function (template, datos, scope) {
        return $compile( ejs.render(template, datos, {}) )(scope);
    }
})
.service('HttpLocal', function ($http) {
    this.host = window.location.protocol + '//' + window.location.host;
    this.get = function (path, data) {
        return $http.get(this.host + path, data);
    }
})
.service('objectResource', function (HttpLocal, ProcessorTemplatesEJS) {
    this.resource = null;
    this.template = null;

    this.save = function (modelo, scope, selector) {
        if(this.isResource()){
            this.resource.save(modelo);
        }else{
            H34.genericMessage('No se ha definido resource');
        }
        return this.query();
    }

    // Retornar todos los elementos
    this.query = function () {
        return this.isResource() ? this.resource.query() : [];
    }

    // se ha definido el recurso
    this.isResource = function () {
        return this.resource !== null;
    }

})
.factory('Categoria', function (objectResource, CategoriaProductoResource, HttpLocal, ProcessorTemplatesEJS) {
    objectResource.template = '/static/js/templates/categoria/partials/select.ejs';
    objectResource.resource = CategoriaProductoResource;

    objectResource.updateUI = function (scope, selector) {
        selector = selector || '#wrapper-categoria';

        HttpLocal.get(objectResource.template)
        .then(function (res) {
            datos = ProcessorTemplatesEJS.build(res.data, {}, scope);
            $(selector).html($(datos));
        }, function () {
            H34.genericError();
        })
    }

    return objectResource;
})
.factory('Marca', function (objectResource, MarcaProductoResource, HttpLocal, ProcessorTemplatesEJS) {
    objectResource.template = '/static/js/templates/marca/partials/select.ejs';
    objectResource.resource = MarcaProductoResource;

    objectResource.updateUI = function (scope, selector) {
        selector = selector || '.wrapper-marca';

        HttpLocal.get(objectResource.template)
        .then(function (res) {
            parent = $(selector).parent().parent().children();
            $.each(parent, function (k, v) {
                modelo = $(v).data('model');
                if (typeof modelo !== 'undefined'){
                    datos = ProcessorTemplatesEJS.build(res.data, {'modelo':modelo}, scope);
                    $(v).children(selector).html($(datos));
                }
            });
        }, function () {
            H34.genericError();
        });
    }

    return objectResource;
})
.service('UnidadMedicion', function (objectResource, UnidadesMedicionResource, HttpLocal, ProcessorTemplatesEJS) {
    objectResource.template = '/static/js/templates/unidad_medicion/partials/select.ejs';
    objectResource.resource = UnidadesMedicionResource;

    objectResource.updateUI = function (scope, selector) {
        selector = selector || '.wrapper-unidad';

        HttpLocal.get(objectResource.template)
        .then(function (res) {
            parent = $(selector).parent().parent().children();
            $.each(parent, function (k, v) {
                modelo = $(v).data('model');
                if (typeof modelo !== 'undefined'){
                    datos = ProcessorTemplatesEJS.build(res.data, {'modelo':modelo}, scope);
                    $(v).children(selector).html($(datos));
                }
            });
        }, function () {
            H34.genericError();
        });
    }

    return objectResource;
})
;
