H34.eventos.searchable = function () {
    $('select.searchable').chosen({
        no_results_text: "No se han encontrado resultados"
    });
}

$(H34.eventos.searchable());

H34.eventos.addEvento(H34.eventos.searchable);
