H34.select_dependent = function (el) {
    this.el = el;
    this.url = $('#url', el).val();
    this.target_id = $('#target_id', el).val();
    this.dependent_id = $('#dependent_id', el).val();
    this.valor_select = $('#valor_select', el).val();
    this.dependent_field_key = $('#dependent_field_key', el).val();
    this.dependent_field_value = $('#dependent_field_value', el).val();

    // Comprobar que la url tenga el patron __id__ que sera el que usaremos
    // para sustituir el campo clave del elemento disparador
    this.url = this.url.split('__id__');
    if(this.url.length<2) return;

    // Obtener el formulario en el cual esta contenido el select
    this.formulario = this.getParentForm(this.el)

    // Registrar la acción para el evento si cambia el padre
    this.chageListener()

    // Si el select disparador tiene un valor al cargarse el DOM
    // entonces se realiza el filtrado de este select
    if( $('#'+this.target_id).val() ){
        this.getTemplate( $('#'+this.target_id).val() );
    }
}

/*
 *  Obtiene los valores via AJAX y construye el html (options) que debe ir
 *  en el select
 */
H34.select_dependent.prototype.getTemplate = function (selected_id, focus) {
    focus = focus || false; // Determina si luego de actualizarse el select debe quedase con el foco
    var that = this;
    $.getJSON(this.url[0] +  selected_id + this.url[1], {}, function(json, textStatus) {
        // Obtener la lista de options y sustituirla en el select
        $.get('/static/js/core/templates/options.ejs', function (template) {
            options = ejs.render(template, {'options':json, 'key':that.dependent_field_key, 'valores':that.dependent_field_value.split(','), 'selected':that.valor_select})
            select = $('#'+that.dependent_id, that.formulario).html($(options));
            //console.log(select)
            if(focus){
                if ( select.hasClass('searchable') ) { // Si el select es un objeto chosen
                    // Eliminar la clase active del resto de los chosen que haya en el formulario
                    $('.chosen-container', that.formulario).removeClass('chosen-container-active');
                    // Colocar la clase active al select actualizado
                    $('#'+that.dependent_id+'_chosen', that.formulario).addClass('chosen-container-active').focus();
                    // Informar que la lista de options ha cambiado
                } else { // Si es un select convencional
                    select.focus();
                }
            }
            $('.searchable', that.el).trigger("chosen:updated");
        })
    });
}

/*
 *  Devuelve el formulario que contiene al elemento que se pasa por parametro
 */
H34.select_dependent.prototype.getParentForm = function (el) {
    for (var i = 0; i < 30 && el.tagName != 'FORM' && el.id != 'form' && typeof el != 'undefined'; i++)
        el = el.parentNode;
    return el;
}

/*
 *  Funcion a ejecutar cuando cambien el select disparador
 */
H34.select_dependent.prototype.chageListener = function () {
    var that = this;
    // Función que contiene las acciones que deben llevarse acabo en el evento
    var listener = function (e) {
        e.preventDefault()
        selected_id = this.options[this.selectedIndex].value;
        console.log(selected_id);
        that.getTemplate(selected_id, true);
    }

    if ($('#'+this.target_id, this.formulario).hasClass('searchable')) {
        // Si el trigger es searchable
        $('#'+this.target_id, this.formulario).chosen().change(listener);
    } else {
        // Si el trigger no es searchable
        $('#'+this.target_id, this.formulario).on('change', listener);
    }
}


// Inicialización de widget
$(function () {
    $.each($(".dependent"), function(index, el) {
        elemento = new H34.select_dependent(el);
    });
});

// Agregar a la cola de eventos la ejecución de la función
H34.eventos.addEvento(function () {
    $.each($(".dependent"), function(index, el) {
        elemento = new H34.select_dependent(el);
    });
});
