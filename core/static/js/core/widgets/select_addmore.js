H34.select_addmore = function () {
    that = this;
    this.getFieldsValues = function (context) {
        data = {'csrfmiddlewaretoken': H34.csrfmiddlewaretoken};
        // inputs text
        $.each($('input[type=text]', context), function (k,v) {
            data[v.name.replace('modal-', '')] = $(v).val();
        });
        // textarea
        $.each($('textarea', context), function (k,v) {
            data[v.name.replace('modal-', '')] = $(v).val();
        });
        // select
        $.each($('select', context), function (k,v) {
            data[v.name.replace('modal-', '')] = $(v).val();
        });

        return data;
    }

    this.modalOwnEvent = function (el) {
        $('div.modal', el).on('shown.bs.modal', function (event) {
            H34.eventos.DomModificado();
        })
    }

    this.modalEventShow = function (el) {
        $("a.addmore", el).on('click', function (event) {
            event.preventDefault();

            div_modal = $('div.modal', el);
            div_modal.modal('show');

        });
    }

    this.modalEventAction = function (el) {
        $('.aceptar', el).on('click', function (event) {
            event.preventDefault();

            a_addmore = $("a.addmore", el);
            this.route = a_addmore.data('route');
            this.key = $('input.key', a_addmore.parent()).val();
            this.value = $('input.value', a_addmore.parent()).val();

            div_modal = $('div.modal', el);
            data = that.getFieldsValues( $('#form', el) );

            eventThis = this;
            $.ajax({
                url: eventThis.route,
                type: 'POST',
                data: data
            })
            .done(function(res) {
                $.getJSON(eventThis.route, {}, function(json, textStatus) {
                    $.get('/static/js/core/templates/options.ejs', function (template) {
                        options = ejs.render(template, {'options':json, 'key':eventThis.key, 'valores':eventThis.value.split(','), 'selected': res[eventThis.key]})
                        $('select', el).html($(options));
                    });
                });
            })
            .fail(function(res) {
                console.log(res)
                BootstrapDialog.alert({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: '¡ERROR!',
                    message: 'Se ha producido un error al intentar crear el elemento',
                });
            })
            .always(function() {
                div_modal.modal('hide');
            });
        });
    }
    that = this;
    $.each($('.addmore-container'), function(index, el) {
        //that.modalOwnEvent(el);
        that.modalEventShow(el);
        that.modalEventAction(el);
    });
}

$(H34.select_addmore);
