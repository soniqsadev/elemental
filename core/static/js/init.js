var H34 = {};

jQuery = $ = $ || django.jQuery;

/**
 *	Utilidad para obtener un valor almacenado en las cookies
 */
H34.getCookie = function(name){
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = $.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};

H34.genericMessage = function (mensaje, milis) {
    mensaje = mensaje || 'Debe introduccir un mensaje';
    milis = milis || 4000;

    Materialize.toast(mensaje, milis);
}

H34.genericError = function (mensaje, milis) {
    mensaje = mensaje || 'Se ha producido un error';

    H34.genericMessage(mensaje, milis);
}


H34.stripTrailingSlash = function (str) {
    return str.replace(/\/$/, "");
}

H34.removeOrigin = function(str){
    return str.replace(window.location.origin,"");
}

/**
 * Hay tres tipos de menus
 *  - #horizontal-menu
 *  - #side-menu
 *  - #side-over-menu
 */
H34.addClassActive = function (path) {
    path = this.stripTrailingSlash(path);
    menu_horizontal_id = 'horizontal-menu';
    menu_side_id = 'side-menu';
    side_overlay_menu_id = 'side-over-menu';

    // Removiendo clases active
    $('.active', document.getElementById(menu_horizontal_id) ).removeClass('active');
    $('.active', document.getElementById(menu_side_id) ).removeClass('active');

    definitiva = null;
    $.each($('a', document.getElementById(menu_side_id)) ,function(index, el) {
        href = H34.removeOrigin(el.href);
        if(path.lastIndexOf(href, 0) === 0) // si el href esta contenido en la url actual
            if(definitiva === null || href.lastIndexOf(H34.removeOrigin(definitiva.href), 0) === 0)
                definitiva = el;
    });
    parent = $(definitiva).parent();
    parent.addClass('active');
    $('#' + parent.attr('parent_id')).addClass('active');

    $.each($('.collection', document.getElementById(menu_side_id)), function (index, el) {
        if (el.getAttribute('parent_id') == parent.attr('parent_id')){
            el.style.display = "initial";
        }
    });
    //console.log($('.collection'))

}


H34.buildMenu = function (data) {
    for (var i = 0; i < data.length; i++) {
        console.log(data[i]);
    }
}


H34.getMenu = function () {
    $.get('/static/js/menu.json', function (data) {
        //console.log(data);
        //H34.buildMenu(data);
    })
    .done(function() {
        console.log("success");
    })
    .fail(function(data) {
        console.log("error", data);
    })
    .always(function() {
        console.log("complete");
    });
};

/**
 * Manejador simple de eventos
 */
H34.eventos = {
     coleccion: [],
     addEvento: function (funcion) {
         H34.eventos.coleccion.push(funcion);
     },
     DomModificado: function () {
         for (var i = 0; i < H34.eventos.coleccion.length; i++) {
             H34.eventos.coleccion[i]();
         }
     }
 }

/**
 *  Clase para gestionar el funcionamiento de los formset
 */
H34.formset = function (config) {
    this.add_more_class = config.add_more_class || '.add_more';
    this.delete_class = config.delete_class || '.delete';
    this.container_id = config.container_id || '#container';
    this.empty_form_id = config.empty_form_id || '#empty_form';
    this.total_form_id = config.total_form_id || '#id_form-TOTAL_FORMS';
    this.max_num_form_id = config.max_num_form_id || '#id_form-MAX_NUM_FORMS';
    this.local_total_form = null;

    this.setTotalForms();

    var that = this;

    $(this.add_more_class).on('click', function(e){
        console.log(that.local_total_form)
        e.preventDefault();
        that.addForm();
    })

    $(this.delete_class).on('click', function(e){
        e.preventDefault();
        that.deleteForm(this.parentNode.parentNode);
    })
}

H34.formset.prototype.getNewForm = function(empty_form_id){
    local_empty_form_id = empty_form_id || this.empty_form_id;
    return $( local_empty_form_id ).val()
}

H34.formset.prototype.replaceTotalForm = function(nodo){
    return nodo.replace( /__prefix__/g, parseInt( this.getTotalForms() ) )
}

H34.formset.prototype.getTotalForms = function(total_form_id){
    local_total_form_id = total_form_id || this.total_form_id;
    console.log('local_total_form_id', local_total_form_id);
    return parseInt( $(local_total_form_id).val() );
    // return (this.local_total_form);
}

H34.formset.prototype.setTotalForms = function(total, total_form_id){
    console.log('se llamo a setTotalForms');
    local_total_form_id = total_form_id || this.total_form_id;
    jq_local_total_form = $(local_total_form_id);
    this.local_total_form = total || this.local_total_form || jq_local_total_form.val()

    jq_local_total_form.val( this.local_total_form );
    console.log('total_form', jq_local_total_form.val());
}

H34.formset.prototype.incrementTotalForms = function(){
    this.setTotalForms( parseInt(this.getTotalForms()) + 1 );
    this.local_total_form++;
}

H34.formset.prototype.decrementTotalForms = function(){
    // this.setTotalForms( parseInt(this.getTotalForms()) - 1 );
    this.local_total_form--;
}

H34.formset.prototype.getMaxNumForms = function (max_num_form_id) {
    local_max_num_form_id = max_num_form_id || this.max_num_form_id;
    console.log('local_max_num_form_id', local_max_num_form_id)
    return parseInt($(local_max_num_form_id).val());
}

H34.formset.prototype.setMaxNumForms = function (max_num_form, max_num_form_id) {
    local_max_num_form_id = max_num_form_id || this.max_num_form_id;
    this.max_num_form = $(local_max_num_form_id).val()
}

H34.formset.prototype.can_add = function () {
    console.log('getTotalForms= ', this.getTotalForms());
    console.log('getMaxNumForms= ', this.getMaxNumForms());
    return this.getTotalForms() < this.getMaxNumForms()
}

H34.formset.prototype.getContainer =  function(container_id){
    local_container_id = container_id || this.container_id;
    return $(local_container_id);
}

H34.formset.prototype.addForm = function(){
    if ( !this.can_add() ) {
        modal = BootstrapDialog;
        modal.alert({
            type: BootstrapDialog.TYPE_DANGER,
            title: 'Advertencia',
            message: "Ha alcanzado el limite de elementos que puede agregar.",
        });
        return;
    }

    new_form = this.replaceTotalForm( this.getNewForm() )
    new_form = $(new_form)
    that = this;
    new_form.find(this.delete_class).on('click', function(e){
        e.preventDefault();
        that.deleteForm(this.parentNode.parentNode)
    });
    this.incrementTotalForms()
    this.getContainer().append(new_form)
    H34.eventos.DomModificado();
}

H34.formset.prototype.deleteForm = function(form){
    modal = BootstrapDialog;
    that = this;
    modal.confirm({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: 'Advertencia',
                    message: "¿Confirma que deseas eliminar este elemento?",
                    callback: function(res){
                        if(res){
                            form = $(form)
                            form.find('input[type=checkbox]').prop('checked', 'checked')
                            form.addClass('hidden');
                            that.decrementTotalForms();
                            H34.eventos.DomModificado();
                        }
                    }
                });
}

$(function () {
    H34.csrfmiddlewaretoken = H34.getCookie('csrftoken');
    $.ajaxSetup({
        headers: { "X-CSRFToken": H34.getCookie("csrftoken") }
    });
    H34.getMenu();
    //$(".button-collapse").sideNav();
});
