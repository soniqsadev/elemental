from django import template

register = template.Library()


@register.filter(name='bootstrap_class')
def boostrap_class(field, args=""):
    inputs = ('TextInput', 'NumberInput', 'EmailInput', 'URLInput', 'PasswordInput', 'DateInput', 'DateTimeInput', 'TimeInput', 'Textarea', 'Select', 'SelectMultiple')
    if get_type(field) in inputs:
        return field.as_widget(attrs={"class":"form-control input-sm " + args })
    return field.as_widget(attrs={'class': args})

@register.filter(name='positive_bs')
def positive_bs(field):
    if get_type(field) == 'NumberInput':
        return field.as_widget(attrs={"class":"form-control input-sm", "min":"0"})
    return field

@register.filter(name0='attrs')
def attrs(field, args=""):
    """
    agregar atributos al widget:
    field|attrs:"class:form-control,id:element_id,data-atributo:principal"
    """
    separados_coma = args.split(",")
    attrs = {}
    for elemento in separados_coma:
        kv = elemento.split(':')
        attrs[kv[0]] = kv[1]
    return field.as_widget(attrs=attrs)


@register.filter(name='get_atribute')
def get_atribute(field, atribute):
    return getattr(field, atribute)


def get_type(field):
    return field.field.widget.__class__.__name__


@register.filter(name='add_partida')
def add_partida(field, args=""):
    i = 1
    for producto in field:
        producto.partida = i
        i = i + 1

    return field


@register.filter(name='relleno_cotizacion')
def relleno_cotizacion(field, args=""):
    print('args= ', args)
    if isinstance(args, int):
        cantidad = args
    else:
        cantidad = 30
    # cantidad = 30
    relleno = []
    if cantidad > len(field):
        faltantes = cantidad - len(field)
        for i in range(1, faltantes):
            relleno.append({})
    return relleno
