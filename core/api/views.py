import json

from django.views.generic.base import TemplateView

from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models import (
                        Moral,
                        CategoriaProducto,
                        MarcaProducto,
                        Producto,
                        EstereotipoProducto,
                        Propiedad
                    )
from .serializers import (
                            MoralSerializer,
                            ClienteSerializer,
                            EmpresaSerializer,
                            CategoriaProductoSerializer,
                            MarcaProductoSerializer,
                            ProductoSerializer,
                            EstereotipoProductoSerializer,
                            PropiedadSerializer,
                        )

class IndexTemplateView(TemplateView):
    template_name = 'op/index.html'


class MoralList(generics.ListCreateAPIView):
    """
    Lista de todos los Morals
    """
    queryset = Moral.objects.all()
    serializer_class = MoralSerializer

class MoralDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Ver, Editar y Eliminar un Moral
    """
    queryset = Moral.objects.all()
    serializer_class = MoralSerializer


class ClienteList(generics.ListCreateAPIView):
    """
    Lista de todos los Cliente
    """
    queryset = Moral.objects.filter(is_cliente=True)
    serializer_class = ClienteSerializer


class ClienteDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Ver, Editar y Eliminar un Cliente
    """
    queryset = Moral.objects.filter(is_cliente=True)
    serializer_class = ClienteSerializer


class EmpresaList(generics.ListCreateAPIView):
    """
    Lista de todos los Empresa
    """
    queryset = Moral.objects.filter(is_me=True)
    serializer_class = EmpresaSerializer


class EmpresaDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Ver, Editar y Eliminar un Empresa
    """
    queryset = Moral.objects.filter(is_me=True)
    serializer_class = EmpresaSerializer


@api_view(['GET'])
def list_dimensiones_view(request):
    return Response(json.dumps(UnidadMedicion.DIMENSIONES))


class CategoriaProductoList(APIView):
    """
    Lista de las categorias o categorias que no tienen padre
    """
    def get(self, request, format=None):
        categorias = CategoriaProducto.objects.filter(parent__isnull=True)
        serializer = CategoriaProductoSerializer(categorias, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def list_subcategorias_productos_view(request, parent):
    """
    Lista de subcategorias o categorias que son hijas de otas categorias
    """
    subcategorias = CategoriaProducto.objects.filter(parent=parent)
    serializer = CategoriaProductoSerializer(subcategorias, many=True)
    return Response(serializer.data)


class CategoriaProductoDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Ver, Editar y Eliminar un CategoriaProducto
    """
    queryset = CategoriaProducto.objects.all()
    serializer_class = CategoriaProductoSerializer


class MarcaProductoList(generics.ListCreateAPIView):
    """
    Lista de todos los MarcaProductos
    """
    queryset = MarcaProducto.objects.all()
    serializer_class = MarcaProductoSerializer

class MarcaProductoDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Ver, Editar y Eliminar un MarcaProducto
    """
    queryset = MarcaProducto.objects.all()
    serializer_class = MarcaProductoSerializer


class EstereotipoProductoList(generics.ListCreateAPIView):
    """
    Lista de todos los EstereotipoProductos
    """
    queryset = EstereotipoProducto.objects.all()
    serializer_class = EstereotipoProductoSerializer

class EstereotipoProductoDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Ver, Editar y Eliminar un EstereotipoProducto
    """
    queryset = EstereotipoProducto.objects.all()
    serializer_class = EstereotipoProductoSerializer


class PropiedadList(generics.ListCreateAPIView):
    """
    Lista de todos los Propiedads
    """
    queryset = Propiedad.objects.all()
    serializer_class = PropiedadSerializer

class PropiedadDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Ver, Editar y Eliminar un Propiedad
    """
    queryset = Propiedad.objects.all()
    serializer_class = PropiedadSerializer
