from rest_framework import serializers
from core.utils.strings import key_generator

from core.models import (
                        MarcaProducto,
                        Moral,
                        CategoriaProducto,
                        EstereotipoProducto,
                        MarcaProducto,
                        Producto,
                        Contacto,
                        Propiedad,
                    )


class MoralSerializer(serializers.ModelSerializer):
    """ Serialiador del modelo Persona Moral """

    class Meta:
        model = Moral
        fields = '__all__'


class ClienteSerializer(serializers.ModelSerializer):
    """ Serialiador del modelo Persona Moral (Cliente) """

    class Meta:
        model = Moral
        fields = '__all__'

    def create(self, validate_data):
        validate_data['is_cliente'] = True
        return Moral.objects.create(**validate_data)


class EmpresaSerializer(serializers.ModelSerializer):
    """ Serialiador del modelo Persona Moral (Empresa) """

    class Meta:
        model = Moral
        fields = '__all__'

    def create(self, validate_data):
        validate_data['is_me'] = True
        return Moral.objects.create(**validate_data)



class EstereotipoProductoSerializer(serializers.ModelSerializer):
    """ Serialiador de la clase Producto """
    # unidades_medicion = serializers.StringRelatedField(many=True, read_only=True)
    # modelos = ModeloProductoSerializer(many=True)

    class Meta:
        model = EstereotipoProducto
        fields = '__all__'

    # def create(self, validate_data):
    #     print('*************************')
    #     print(validate_data)
    #     print('*************************')
    #     modelos = validate_data.pop('modelos')
    #     unidades_medicion = validate_data.pop('unidades_medicion')
    #     validate_data['codigo'] = key_generator(size=16)
    #     estereotipo = EstereotipoProducto.objects.create(**validate_data)
    #     for modelo in modelos:
    #         ModeloProducto.objects.create(estereotipo=estereotipo, **modelo)
    #     for unidad in unidades_medicion:
    #         estereotipo.unidades_medicion.add(unidad)
    #     return estereotipo
    #
    # def update(self, instance, validate_data):
    #     print('*************************')
    #     print(validate_data)
    #     print('*************************')
    #     modelos = validate_data.pop('modelos')
    #     unidades_medicion = validate_data.pop('unidades_medicion')
    #     EstereotipoProducto.objects.filter(pk=instance.id).update(**validate_data)
    #
    #     print(modelos)
    #     #modelos_ids = [modelo['id'] for modelo in modelos]
    #     unidades_ids = [unidad.id for unidad in unidades_medicion]
    #
    #     # Crear o eliminar modelos si no se encuentran o no en el estereotipo
    #     for modelo in instance.modelos.all():
    #         if not modelo in modelos:
    #             print('"%s" no esta en los modelos del producto' % modelo)
    #             modelo.delete()
    #
    #     for modelo in modelos:
    #         print('se agrego => ', modelo)
    #         new_modelo = ModeloProducto(estereotipo=instance, **modelo)
    #         new_modelo.save()
    #
    #     # Asociar o desvincular unidades si se encuentran o no en el estereotipo
    #     for unidad in instance.unidades_medicion.all():
    #         if not unidad.id in unidades_ids:
    #             instance.unidades_medicion.remove(unidad)
    #
    #     for unidad in unidades_medicion:
    #         try:
    #             instance.unidades_medicion.get(pk=unidad.id)
    #         except Exception as e:
    #             instance.unidades_medicion.add(unidad)
    #
    #     return instance


class CategoriaProductoSerializer(serializers.ModelSerializer):
    """ Serialiador de la clase Producto """
    class Meta:
        model = CategoriaProducto
        fields = '__all__'


class MarcaProductoSerializer(serializers.ModelSerializer):
    """ Serialiador de la clase Producto """
    class Meta:
        model = MarcaProducto
        fields = '__all__'


class ProductoSerializer(serializers.ModelSerializer):
    """ Serialiador de la clase Producto """
    #modelo_id = serializers.StringRelatedField(many=False)
    #modelo_id = serializers.SlugRelatedField(many=False,read_only=True,slug_field='nombre')
    #modelo = ModeloProductoSerializer(many=False, read_only=True)

    class Meta:
        model = Producto
        fields = '__all__'


class ContactoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Contacto
        fields = '__all__'


class PropiedadSerializer(serializers.ModelSerializer):

    class Meta:
        model = Propiedad
        fields = '__all__'
