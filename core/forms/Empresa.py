from django.forms import ModelForm

from core.forms.custom import Searchable
from division_territorial.forms import CiudadField, EstadoField
from core.models import MarcaProducto, CategoriaProducto
from ..models import Empresa

class EmpresaForm(ModelForm):
    estado = EstadoField()
    ciudad = CiudadField()

    class Meta:
        model = Empresa
        exclude = ('is_me', 'is_cliente', 'is_proveedor')

    def __init__(self, *args, **kwargs):
        super(EmpresaForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(EmpresaForm, self).clean()
        return cleaned_data
