from django.forms import ModelForm
from ..models import MarcaProducto

class MarcaProductoForm(ModelForm):

    class Meta:
        model = MarcaProducto
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(MarcaProductoForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(MarcaProductoForm, self).clean()
        return cleaned_data
