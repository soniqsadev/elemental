from django.forms import ModelForm
from ..models import Contacto


class ContactoForm(ModelForm):
    # TODO: Define other fields here

    class Meta:
        model = Contacto
        fields = ['nombres', 'apellidos', 'cargo', 'division_administrativa']

    def __init__(self, *args, **kwargs):
        super(ContactoForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(ContactoForm, self).clean()
        return cleaned_data
