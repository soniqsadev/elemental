from django.db.models import Q
from django.forms import ModelForm

from .custom import AddMore, SelectDependent, Searchable
from ..models import CategoriaProducto

class CategoriaProductoForm(ModelForm):
    parent = Searchable.ChoiceSearchableField(queryset=CategoriaProducto.objects.all(), required=False)

    class Meta:
        model = CategoriaProducto
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(CategoriaProductoForm, self).__init__(*args, **kwargs)
        if self.instance.id:
            self.fields['parent'].queryset = CategoriaProducto.objects.exclude( Q(pk=self.instance.id) | Q(parent=self.instance.pk) )

    def clean(self):
        cleaned_data = super(CategoriaProductoForm, self).clean()
        return cleaned_data
