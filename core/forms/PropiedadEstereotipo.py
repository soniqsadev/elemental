from django.forms import ModelForm
from ..models import PropiedadEstereotipo


class PropiedadEstereotipoForm(ModelForm):
    # TODO: Define other fields here

    class Meta:
        model = PropiedadEstereotipo
        exclude = ['estereotipo',]

    def __init__(self, *args, **kwargs):
        super(PropiedadEstereotipo, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(PropiedadEstereotipo, self).clean()
        return cleaned_data
