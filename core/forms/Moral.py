from django.forms import ModelForm
from division_territorial.forms import CiudadField, EstadoField
from ..models import Moral


class MoralForm(ModelForm):
    """
    Formulario para entidad Moral
    """
    estado = EstadoField()
    ciudad = CiudadField()

    class Meta:
        model = Moral
        exclude = ('is_me', 'is_cliente', 'is_proveedor')

    def __init__(self, *args, **kwargs):
        super(MoralForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(MoralForm, self).clean()
        return cleaned_data



class MoralSimpleForm(ModelForm):
    """
    Una versión simplificada de MoralForm
    """
    estado = EstadoField()
    ciudad = CiudadField()

    class Meta:
        model = Moral
        exclude = ('is_me', 'is_cliente', 'is_proveedor', 'logo')

    def __init__(self, *args, **kwargs):
        super(MoralSimpleForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(MoralSimpleForm, self).clean()
        return cleaned_data
