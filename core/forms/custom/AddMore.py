from django.forms import fields
from django import forms
from core.widgets.Select import SelectAddMore


class ChoiceAddMoreField(forms.ModelChoiceField):

    def __init__(self, *args, **kwargs):
        self.widget = SelectAddMore(**kwargs)

        if 'ruta' in kwargs:
            kwargs.pop('ruta')

        if 'form' in kwargs:
            kwargs.pop('form')

        if 'kv' in kwargs:
            kwargs.pop('kv')

        super(ChoiceAddMoreField, self).__init__(*args, **kwargs)

    def validate(self, value):
        return super(ChoiceAddMoreField, self).validate(value)
