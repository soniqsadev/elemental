from django.forms import fields
from django import forms

from core.widgets.Select import SelectDependent, SelectSearchableDependent
from core.models.utils import Empty

class ChoiceDependentField(forms.ModelChoiceField):

    def __init__(self, *args, **kwargs):

        self.widget = kwargs.get('widget', SelectDependent(**kwargs))
        self.choices = [('','---------'),]

        if 'url' in kwargs:
        	kwargs.pop('url')

        if 'target_id' in kwargs:
        	kwargs.pop('target_id')

        if 'kv' in kwargs:
        	kwargs.pop('kv')

        super(ChoiceDependentField, self).__init__(*args, **kwargs)


    def validate(self, value):
        return super(ChoiceDependentField, self).validate(value)


class ChoiceSearchableDependentField(ChoiceDependentField):

    def __init__(self, *args, **kwargs):
        kwargs['widget'] = SelectSearchableDependent(**kwargs)
        super(ChoiceSearchableDependentField, self).__init__(*args, **kwargs)
