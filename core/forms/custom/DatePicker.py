from django.forms import fields
from django import forms
from core.widgets.DatePicker import DatePicker, DateTimePicker

class DatePickerField(forms.DateField):

    def __init__(self, *args, **kwargs):
        self.widget = DatePicker(**kwargs)

        super(DatePickerField, self).__init__(*args, **kwargs)

    def validate(self, value):
        return super(DatePickerField, self).validate(value)


class DateTimePickerField(forms.DateTimeField):

    def __init__(self, *args, **kwargs):
        self.widget = DateTimePicker(**kwargs)

        super(DateTimePickerField, self).__init__(*args, **kwargs)

    def validate(self, value):
        return super(DateTimePickerField, self).validate(value)
