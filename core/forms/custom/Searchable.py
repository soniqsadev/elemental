from django.forms import fields
from django import forms
from core.widgets.Select import SelectSearchable, SelectMultipleSearchable

class ChoiceSearchableField(forms.ModelChoiceField):

    def __init__(self, *args, **kwargs):
        self.widget = SelectSearchable(**kwargs)

        super(ChoiceSearchableField, self).__init__(*args, **kwargs)

    def validate(self, value):
        return super(ChoiceSearchableField, self).validate(value)



class ChoiceMultipleSearchableField(forms.ModelMultipleChoiceField):

    def __init__(self, *args, **kwargs):
        # kwargs['empty_label'] = None
        # print('kwargs', kwargs)
        self.widget = SelectMultipleSearchable(**kwargs)

        super(ChoiceMultipleSearchableField, self).__init__(*args, **kwargs)

    def validate(self, value):
        return super(ChoiceMultipleSearchableField, self).validate(value)
