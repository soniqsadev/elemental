from django.db.models import Q

from django.forms import ModelForm, ChoiceField
from ..models import EstereotipoProducto, CategoriaProducto, MarcaProducto, Propiedad
from .custom import AddMore, SelectDependent, Searchable
from ..forms.CategoriaProducto import CategoriaProductoForm
from ..forms.MarcaProducto import MarcaProductoForm


class EstereotipoProductoForm(ModelForm):
    categoria = Searchable.ChoiceSearchableField(queryset=CategoriaProducto.objects.filter( Q(parent__isnull=True) | Q(subcategorias__isnull=False) ).distinct() )
    # categoria = Searchable.ChoiceSearchableField(queryset=CategoriaProducto.objects.filter(subcategorias__isnull=False).distinct())
    subcategoria = SelectDependent.ChoiceSearchableDependentField(
                                    queryset=CategoriaProducto.objects.filter(parent__isnull=False),
                                    url='/api/producto/categoria/__id__/subcategorias',
                                    target_id='id_categoria',
                                    kv={'key':'id', 'value':'nombre'},
                                    required=False
        )
    # marca = ChoiceAddMoreField(
    #                                 ruta='api.producto.marca.list',
    #                                 form=MarcaProductoForm(prefix='modal'),
    #                                 kv={'key':'id', 'value':'nombre'},
    #                                 queryset=MarcaProducto.objects.all(),
    #                             )

    marca = Searchable.ChoiceSearchableField(queryset=MarcaProducto.objects.all(), required=False)

    class Meta:
        model = EstereotipoProducto
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(EstereotipoProductoForm, self).__init__(*args, **kwargs)
        # self.fields['categoria'].queryset = CategoriaProducto.objects.filter(parent__isnull=True)

    def clean(self):
        cleaned_data = super(EstereotipoProductoForm, self).clean()
        return cleaned_data


class PropiedadForm(ModelForm):
    # TODO: Define other fields here

    class Meta:
        model = Propiedad
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(PropiedadForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(PropiedadForm, self).clean()
        return cleaned_data
