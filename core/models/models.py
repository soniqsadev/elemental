from django.db import models


class MetaInformacion(models.Model):
    """
    Información de configuración de la aplicación
    """
    clave = models.CharField(max_length=100)
    valor = models.CharField(max_length=100, null=True)
    activo = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Variable de configuración'
        verbose_name_plural = 'Variables de configuración'

    def __str__(self):
        "%s : %s" % (self.clave, self.valor)
