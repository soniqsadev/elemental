from django.db import models


class CategoriaProducto(models.Model):
    nombre = models.CharField(max_length=64)
    descripcion = models.TextField(blank=True,
                                   null=True,
                                   verbose_name='Descripción')
    parent = models.ForeignKey('CategoriaProducto',
                               verbose_name='Categoría padre',
                               related_name='subcategorias',
                               null=True,
                               blank=True,
                               on_delete=models.SET_NULL)

    class Meta:
        verbose_name = 'Categoría (Producto)'
        verbose_name_plural = 'Categorías (Producto)'

    def __str__(self):
        return self.nombre


class MarcaProducto(models.Model):
    nombre = models.CharField(max_length=64)

    class Meta:
        verbose_name = 'Marca (Producto)'
        verbose_name_plural = 'Marca (Productos)'

    def __str__(self):
        return self.nombre


class Propiedad(models.Model):
    nombre = models.CharField(blank=True, max_length=100)

    class Meta:
        verbose_name = 'Propiedad'
        verbose_name_plural = 'Propiedades'

    def __str__(self):
        return self.nombre


class EstereotipoProducto(models.Model):
    """
    Concepto de producto
    """
    codigo = models.CharField(verbose_name='Código', max_length=64, null=True)
    categoria = models.ForeignKey(CategoriaProducto, related_name='estereotipos_productos_como_categoria', verbose_name='Categoría', null=True, on_delete=models.SET_NULL)
    subcategoria = models.ForeignKey(CategoriaProducto, related_name='estereotipos_productos_como_subcategoria', verbose_name="Sub-catería", null=True, blank=True, on_delete=models.SET_NULL)
    marca = models.ForeignKey(MarcaProducto, related_name='estereotipos_productos', null=True, on_delete=models.SET_NULL)
    parent = models.ForeignKey('EstereotipoProducto', null=True, blank=True, related_name="children")
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField(verbose_name='Descripción', blank=True)

    class Meta:
        verbose_name = 'Estereotipo (Producto)'
        verbose_name_plural = 'Estereotipos (Producto)'

    def __str__(self):
        return self.nombre

    def medidas(self):
        propiedad = Propiedad.objects.get(nombre='Medida')
        medida = PropiedadEstereotipo.objects.filter(propiedad=propiedad, estereotipo=self)
        # print(medida)
        return medida


class PropiedadEstereotipo(models.Model):
    estereotipo = models.ForeignKey(EstereotipoProducto, related_name='propiedades_estereotipos', on_delete=models.CASCADE)
    propiedad = models.ForeignKey(Propiedad, related_name='propiedades_estereotipos', on_delete=models.CASCADE)
    valor = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'Propiedad del Estereotipo Producto'
        verbose_name_plural = 'Propiedades del Estereotipo Producto'

    def __str__(self):
        return "%s : %s" % (self.propiedad, self.valor)


class Producto(models.Model):
    """
    Producto fisico
    """
    TIPOS = [
        ('virtual', 'Virtual'),
        ('real', 'Real'),
    ]
    tipo = models.CharField(blank=True, max_length=16, choices=TIPOS, default='real')
    estereotipo = models.ForeignKey(EstereotipoProducto, null=True, related_name='productos', on_delete=models.SET_NULL)
    codigo = models.CharField(max_length=100, null=True, blank=True, verbose_name='Código')
    fecha_fabricacion = models.DateField(null=True, blank=True, verbose_name='Fecha de fabricación')
    fecha_vencimiento = models.DateField(null=True, blank=True, verbose_name='Fecha de vencimiento')
    precio_compra = models.DecimalField(max_digits=9, decimal_places=2, null=True, blank=True, verbose_name='Precio de compra')
    precio_venta = models.DecimalField(max_digits=9, decimal_places=2, null=True, blank=True, verbose_name='Precio de venta')
    propiedades = models.ManyToManyField(PropiedadEstereotipo, related_name='productos')
    almacen = models.ForeignKey('almacen.Almacen', null=True, blank=True)
    ubicacion = models.ForeignKey('almacen.ubicacion', null=True, blank=True)


    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'

    def __str__(self):
        if self.codigo != None:
            return "(%s) %s" % (self.codigo, self.estereotipo)
        return str (self.estereotipo)
