from django.db.models import ForeignKey
from ..forms.SelectDependent import ChoiceDependentField


class FKDependent(ForeignKey):
    formfield_class = ChoiceDependentField
    form_class = ChoiceDependentField

    def __init__(self, *args, **kwargs):
        super(FKDependent, self).__init__(*args, **kwargs)
        ForeignKey.__init__(self, *args, **kwargs)

    def formfield(self, **kwargs):
        return super(FKDependent, self).formfield(form_class=ChoiceDependentField, **kwargs)
