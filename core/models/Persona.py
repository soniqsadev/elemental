import os

from django.core.files.storage import FileSystemStorage

from django.conf import settings
from django.db import models
from division_territorial.models import Estado, Ciudad


fs = FileSystemStorage(location=os.path.join(settings.BASE_TEMPLATES, "uploads"))

#########################################################################
#                              MetaModelos
#########################################################################

class Persona(models.Model):
    estado = models.ForeignKey(Estado, verbose_name='Estado', null=True, on_delete=models.DO_NOTHING)
    ciudad = models.ForeignKey(Ciudad, verbose_name='Delegación', null=True, on_delete=models.DO_NOTHING)


class Telefono(models.Model):
    TIPOS = [
        ('local', 'Local'),
        ('movil', 'Móvil'),
        ('fax', 'Fax'),
    ]
    tipo = models.CharField(max_length=16, choices=TIPOS)
    numero = models.CharField(max_length=32)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE, related_name='telefonos')

    def verbose_tipo(self):
        for tipo in self.TIPOS:
            if tipo[0] == self.tipo:
                return tipo[1]
        return None

    class Meta:
        verbose_name = 'Teléfono'
        verbose_name_plural = 'Telefonos'

    def __str__(self):
        "[%s] %s" % (self.tipo, self.numero)


class Natural(Persona):
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)

    def nombre_completo(self):
        return "%s %s" % (self.nombres, self.apellidos)

    def __str__(self):
        return self.nombre_completo()


class Moral(Persona):
    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    rfc = models.CharField(blank=True, null=True, max_length=32, verbose_name='RFC')
    logo = models.ImageField(upload_to="persona_moral/logos", null=True, blank=True)
    direccion = models.TextField(blank=True, verbose_name='Dirección')
    is_me = models.BooleanField(default=False)
    is_cliente = models.BooleanField(default=False)
    is_proveedor = models.BooleanField(default=False)

    def __str__(self):
        return self.nombre


class Contacto(Natural):
    cargo = models.CharField(null=True, max_length=100, verbose_name='Cargo')
    division_administrativa = models.CharField(blank=True, max_length=100, verbose_name='División administrativa a la que pertenece')
    persona_moral = models.ForeignKey(Moral, related_name='contactos', on_delete=models.CASCADE)


class Empresa(Moral):
    template_cotizacion = models.FileField(upload_to='cotizaciones', storage=fs, verbose_name="Plantilla para cotizaciones", null=True, blank=True)
    style_cotizacion = models.FileField(upload_to=os.path.join('empresas', 'cotizaciones'),
                                        verbose_name="CSS para cotizaciones",
                                        null=True,
                                        blank=True
                                )
