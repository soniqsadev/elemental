from django.contrib import admin


from .models import CategoriaProducto, EstereotipoProducto, MarcaProducto, Producto, Moral


@admin.register(CategoriaProducto)
class CategoriaProductoAdmin(admin.ModelAdmin):
    pass

@admin.register(EstereotipoProducto)
class EstereotipoProductoAdmin(admin.ModelAdmin):
    pass

@admin.register(MarcaProducto)
class MarcaProductoAdmin(admin.ModelAdmin):
    pass

@admin.register(Producto)
class ProductoAdmin(admin.ModelAdmin):
    pass

@admin.register(Moral)
class MoralAdmin(admin.ModelAdmin):
    pass
