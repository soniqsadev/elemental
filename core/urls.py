from django.conf.urls import url

from .views import default, EstereotipoProducto, CategoriaProducto, MarcaProducto, Moral, Empresa, Contacto, Propiedad

from .api.views import (
                        list_dimensiones_view,
                        IndexTemplateView,

                        CategoriaProductoList,
                        CategoriaProductoDetail,
                        list_subcategorias_productos_view,

                        MarcaProductoList,
                        MarcaProductoDetail,

                        EstereotipoProductoList,
                        EstereotipoProductoDetail,

                        MoralList,
                        MoralDetail,

                        ClienteList,
                        ClienteDetail,

                        EmpresaList,
                        EmpresaDetail,

                        PropiedadList,
                        PropiedadDetail,
                    )

urlpatterns = [

    url(regex=r'^$', view=default.IndexTemplateView.as_view(), name='index'),

    url(regex=r'^pdf/saluda/$', view=default.test_pdf, name='test_pdf'),
    url(regex=r'^pdf/invoice/$', view=default.TestTemplateView.as_view(), name='invoce_pdf'),

    url(regex=r'^configuracion/$', view=default.ConfigTemplateView.as_view(), name='configuracion'),

    url(regex=r'^configuracion/producto/$', view=EstereotipoProducto.ListView.as_view(), name='productos.list'),
    url(regex=r'^configuracion/producto/create/$', view=EstereotipoProducto.CreateView.as_view(), name='productos.create'),
    url(regex=r'^configuracion/producto/(?P<pk>[0-9]+)/$', view=EstereotipoProducto.DetailView.as_view(), name='productos.detail'),
    url(regex=r'^configuracion/producto/(?P<pk>[0-9]+)/edit/$', view=EstereotipoProducto.UpdateView.as_view(), name='productos.update'),
    url(regex=r'^configuracion/producto/(?P<pk>[0-9]+)/delete/$', view=EstereotipoProducto.DeleteView.as_view(), name='productos.delete'),

    url(regex=r'^configuracion/producto/categoria/$', view=CategoriaProducto.ListView.as_view(), name='producto.categoria.list'),
    url(regex=r'^configuracion/producto/categoria/create/$', view=CategoriaProducto.CreateView.as_view(), name='producto.categoria.create'),
    url(regex=r'^configuracion/producto/categoria/(?P<pk>[0-9]+)/$', view=CategoriaProducto.DetailView.as_view(), name='producto.categoria.detail'),
    url(regex=r'^configuracion/producto/categoria/(?P<pk>[0-9]+)/edit/$', view=CategoriaProducto.CategoriaProductoUpdateView.as_view(), name='producto.categoria.update'),
    url(regex=r'^configuracion/producto/categoria/(?P<pk>[0-9]+)/delete/$', view=CategoriaProducto.DeleteView.as_view(), name='producto.categoria.delete'),

    url(regex=r'^configuracion/producto/propiedad/$', view=Propiedad.ListView.as_view(), name='producto.propiedad.list'),
    url(regex=r'^configuracion/producto/propiedad/create/$', view=Propiedad.CreateView.as_view(), name='producto.propiedad.create'),
    url(regex=r'^configuracion/producto/propiedad/(?P<pk>[0-9]+)/$', view=Propiedad.DetailView.as_view(), name='producto.propiedad.detail'),
    url(regex=r'^configuracion/producto/propiedad/(?P<pk>[0-9]+)/edit/$', view=Propiedad.UpdateView.as_view(), name='producto.propiedad.update'),
    url(regex=r'^configuracion/producto/propiedad/(?P<pk>[0-9]+)/delete/$', view=Propiedad.DeleteView.as_view(), name='producto.propiedad.delete'),

    url(regex=r'^configuracion/producto/marca/$', view=MarcaProducto.ListView.as_view(), name='producto.marca.list'),
    url(regex=r'^configuracion/producto/marca/create/$', view=MarcaProducto.CreateView.as_view(), name='producto.marca.create'),
    url(regex=r'^configuracion/producto/marca/(?P<pk>[0-9]+)/$', view=MarcaProducto.DetailView.as_view(), name='producto.marca.detail'),
    url(regex=r'^configuracion/producto/marca/(?P<pk>[0-9]+)/edit/$', view=MarcaProducto.UpdateView.as_view(), name='producto.marca.update'),
    url(regex=r'^configuracion/producto/marca/(?P<pk>[0-9]+)/delete/$', view=MarcaProducto.DeleteView.as_view(), name='producto.marca.delete'),

    url(regex=r'^persona/moral/$', view=Moral.ListView.as_view(), name='moral.list'),
    url(regex=r'^persona/moral/create/$', view=Moral.CreateView.as_view(), name='moral.create'),
    url(regex=r'^persona/moral/(?P<pk>[0-9]+)/$', view=Moral.DetailView.as_view(), name='moral.detail'),
    url(regex=r'^persona/moral/(?P<pk>[0-9]+)/edit/$', view=Moral.UpdateView.as_view(), name='moral.update'),
    url(regex=r'^persona/moral/(?P<pk>[0-9]+)/delete/$', view=Moral.DeleteView.as_view(), name='moral.delete'),
    url(regex=r'^persona/moral/(?P<moral_id>[0-9]+)/contacto/create/$', view=Contacto.CreateContactoView.as_view(), name='contacto.create'),
    url(regex=r'^persona/moral/(?P<moral_id>[0-9]+)/contacto/(?P<contacto_id>[0-9]+)/edit/$', view=Contacto.UpdateContactoView.as_view(), name='contacto.edit'),

    url(regex=r'^configuracion/empresa/$', view=Empresa.ListView.as_view(), name='empresa.list'),
    url(regex=r'^configuracion/empresa/create/$', view=Empresa.CreateView.as_view(), name='empresa.create'),
    url(regex=r'^configuracion/empresa/(?P<pk>[0-9]+)/$', view=Empresa.DetailView.as_view(), name='empresa.detail'),
    url(regex=r'^configuracion/empresa/(?P<pk>[0-9]+)/edit/$', view=Empresa.UpdateView.as_view(), name='empresa.update'),
    url(regex=r'^configuracion/empresa/(?P<pk>[0-9]+)/delete/$', view=Empresa.DeleteView.as_view(), name='empresa.delete'),

]

urlpatterns = urlpatterns + [

    url(regex=r'^one/$', view=IndexTemplateView.as_view(), name='api.index'),

    url(regex=r'^api/producto/categoria/$', view=CategoriaProductoList.as_view(), name='api.producto.categoria.list'),
    url(regex=r'^api/producto/categoria/(?P<pk>[0-9]+)/$', view=CategoriaProductoDetail.as_view(), name='api.producto.categoria.detail'),
    url(regex=r'^api/producto/categoria/(?P<parent>[0-9]+)/subcategorias/$', view=list_subcategorias_productos_view, name='api.producto.subcategoria.list'),

    url(regex=r'^api/producto/marca/$', view=MarcaProductoList.as_view(), name='api.producto.marca.list'),
    url(regex=r'^api/producto/marca/(?P<pk>[0-9]+)/$', view=MarcaProductoDetail.as_view(), name='api.producto.marca.detail'),

    # url(regex=r'^api/unidades-medicion/$', view=UnidadMedicionList.as_view(), name='api.unidades_medicion.list'),
    # url(regex=r'^api/unidades-medicion/unidades/$', view=list_dimensiones_view, name='api.unidades_medicion.unidades'),
    # url(regex=r'^api/unidades-medicion/(?P<pk>[0-9]+)/$', view=UnidadMedicionDetail.as_view(), name='api.unidades_medicion.detail'),

    url(regex=r'^api/producto/$', view=EstereotipoProductoList.as_view(), name='api.productos.list'),
    url(regex=r'^api/producto/(?P<pk>[0-9]+)/$', view=EstereotipoProductoDetail.as_view(), name='api.productos.detail'),

    url(regex=r'^api/moral/$', view=MoralList.as_view(), name='api.moral.list'),
    url(regex=r'^api/moral/(?P<pk>[0-9]+)/$', view=MoralDetail.as_view(), name='api.moral.detail'),

    url(regex=r'^api/cliente/$', view=ClienteList.as_view(), name='api.cliente.list'),
    url(regex=r'^api/cliente/(?P<pk>[0-9]+)/$', view=ClienteDetail.as_view(), name='api.cliente.detail'),

    url(regex=r'^api/empresa/$', view=EmpresaList.as_view(), name='api.empresa.list'),
    url(regex=r'^api/empresa/(?P<pk>[0-9]+)/$', view=EmpresaDetail.as_view(), name='api.empresa.detail'),

    url(regex=r'^api/propiedad-producto/$', view=PropiedadList.as_view(), name='api.propiedad_producto.list'),
    url(regex=r'^api/propiedad-producto/(?P<pk>[0-9]+)/$', view=PropiedadDetail.as_view(), name='api.propiedad_producto.detail'),
]
