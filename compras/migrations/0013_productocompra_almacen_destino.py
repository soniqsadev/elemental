# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-23 21:56
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('almacen', '0002_actualizacioncompra_pedido'),
        ('compras', '0012_auto_20160822_1714'),
    ]

    operations = [
        migrations.AddField(
            model_name='productocompra',
            name='almacen_destino',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='productos_de_compra_como_destino', to='almacen.Almacen', verbose_name='Almacén donde debe ser recibido el producto'),
        ),
    ]
