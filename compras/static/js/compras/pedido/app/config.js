angular.module('tabla-pedido', ['ngResource', 'multiStepForm'])
.config(function ($httpProvider) {

    // Configuración para token de ajax
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

});
