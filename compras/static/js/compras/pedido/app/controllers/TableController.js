angular.module('tabla-pedido')
.controller('TableController', function ($scope, $compile, HttpLocal, AlmacenResource, ProveedorResource, PedidoCompraResource) {

    $scope.proveedores = ProveedorResource.query();
    $scope.almacenes = AlmacenResource.query();
    $scope.producto = {'id':null};
    $scope.pedido = {'id':null};
    $scope.orden_venta = $('#orden_venta').val();
    $scope.row = null;

    propiedad_producto_formset = new H34.formset({
        container_id : "#container_producto",
        empty_form_id: "#empty_form_producto"
    });

    propiedad_producto_formset.addForm = function () {
        new_form = $compile( this.replaceTotalForm( this.getNewForm() ) )($scope);
        console.log(new_form);
        that = this;
        new_form.find(this.delete_class).on('click', function(e){
            e.preventDefault();
            that.deleteForm(this.parentNode.parentNode)
        });
        this.incrementTotalForms()
        this.getContainer().append(new_form)
        H34.eventos.DomModificado();
    }

    propiedad_producto_formset.deleteForm = function (form) {
        modal = BootstrapDialog;
        modal.confirm({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'Advertencia',
                        message: "¿Confirma que deseas eliminar este elemento?",
                        callback: confirmacion
                    });

        confirmacion = function(res){
            console.log(form)
            if(res){
                form = $(form)
                form.find('input[type=checkbox]').prop('checked', 'checked')
                form.addClass('hidden');
                H34.eventos.DomModificado();
                // Eliminar pedidos de compra asociados a este producto
                HttpLocal.get('/api/compras/pedido/orden-venta/'+ $scope.orden_venta +'/producto/'+ $('.producto select', form).val() +'/')
                .then(function (res) {
                    PedidoCompraResource.delete({id:res.data.id}, function (data) {
                        console.log(data);
                    })
                }, function (fail) {
                    console.log(fail);
                })
            }
        };
    }

    $scope.getRow = function ($event) {
        get_data_from_row($event.currentTarget);
    }

    $scope.getTd = function ($event) {
        get_data_from_row($event.currentTarget.parentNode);
    }

    $scope.getInputTd = function ($event) {
        get_data_from_row($event.currentTarget.parentNode.parentNode);
    }

    is_errors = function () {
        errors = false;
        empresa = $('#id_empresa').val() || null;
        cliente = $('#id_cliente').val() || null;
        hospital = $('#id_hospital').val() || null;
        medico = $('#id_medico').val() || null;
        paciente = $('#id_paciente').val() || null;
        fecha_cirugia = $('#id_fecha_cirugia').val() || null;

        if (   empresa===null
            || cliente===null
            || hospital===null
            || medico===null
            || paciente===null
            || fecha_cirugia===null
        ){
            errors = true;
        }

        mensaje = 'Antes debe seleccionar:';
        mensaje += '<ul>';

        if(empresa===null){
            mensaje += '<li>Una empresa</li>';
            $('#id_empresa').addClass('errores');
        }else{
            $('#id_empresa').removeClass('errores');
        }
        if(cliente===null){
            mensaje += '<li>Un cliente</li>';
            $('#id_cliente').addClass('errores');
        }else{
            $('#id_cliente').removeClass('errores');
        }
        if(hospital===null){
            mensaje += '<li>Un hospital</li>';
            $('#id_hospital').addClass('errores');
        }else{
            $('#id_hospital').removeClass('errores');
        }
        if(medico===null){
            mensaje += '<li>Un médico</li>';
            $('#id_medico').addClass('errores');
        }else{
            $('#id_medico').removeClass('errores');
        }
        if(paciente===null){
            mensaje += '<li>Un paciente</li>';
            $('#id_paciente').addClass('errores');
        }else{
            $('#id_paciente').removeClass('errores');
        }
        if(fecha_cirugia===null){
            mensaje += '<li>Una fecha de cirugia</li>';
            $('#id_fecha_cirugia').addClass('errores');
        }else{
            $('#id_fecha_cirugia').removeClass('errores');
        }

        mensaje += '</ul>';

        if(errors){
            BootstrapDialog.alert({
                            type: BootstrapDialog.TYPE_DANGER,
                            title: '¡¡¡ERROR!!!',
                            message: mensaje,
                        });
        }

        return errors;
    }

    get_data_from_row = function (row) {
        $scope.row = row;
        // Si hay errores se cancela el proceso
        if( is_errors() )
            return;

        // Continuar en con las operaciones
        var cambio = $scope.producto.id !== ( $('.producto select', row).val() || null )
        $scope.producto.id = $('.producto select', row).val() || null;
        var cantidad = $('.cantidad input', row).val() || null;
        var precio_unidad = $('.precio input', row).val() || null;

        if ($scope.producto.id !== null) {

            HttpLocal.get('/api/almacen/producto/' + $scope.producto.id + '/existencia/')
            .then(function (res) {
                disponibilidad = JSON.parse(res.data).count;
                $scope.producto.disponibilidad = isNaN(disponibilidad) ? 0 : parseInt(disponibilidad);
                $scope.producto.cantidad = isNaN(cantidad) ? 0 : parseInt(cantidad);
                $scope.producto.precio_unidad = isNaN(precio_unidad) ? 0 : parseInt(precio_unidad);

                if(cambio){
                    if($scope.producto.cantidad > $scope.producto.disponibilidad){
                        HttpLocal.get('/api/compras/pedido/orden-venta/'+ $scope.orden_venta +'/producto/'+ $scope.producto.id +'/')
                        .then(function (data_pedido) {
                            console.log(data_pedido.data);
                            $scope.pedido.id = data_pedido.data.id;
                            $scope.pedido.cantidad = data_pedido.data.cantidad;
                            $scope.pedido.proveedor = data_pedido.data.proveedor.toString();
                            $scope.pedido.almacen_destino = data_pedido.data.almacen_destino.toString();
                            $scope.pedido.fecha_compromiso = moment(data_pedido.data.fecha_compromiso).format('DD/MM/YYYY H:mm');
                            $scope.pedido.fecha_compromiso_original = moment(data_pedido.data.fecha_compromiso);
                            $scope.pedido.fecha_compromiso_cambio = false;
                            $scope.pedido.precio_unidad = parseFloat(data_pedido.data.precio_unidad);
                        }, function (fail) {
                            $scope.pedido = {'id':null};
                            $scope.pedido.cantidad = $scope.producto.cantidad - $scope.producto.disponibilidad;
                            console.log('fallo en conseguir la orden de venta', fail);
                        })
                    }
                }else{
                    $scope.pedido.cantidad = $scope.producto.cantidad - $scope.producto.disponibilidad;
                }

            }, function () {
                BootstrapDialog.alert({
                                type: BootstrapDialog.TYPE_DANGER,
                                title: '¡¡¡ERROR!!!',
                                message: 'Se ha producido un error, por favor compruebe su conexión a internet',
                            });
            });
            contenido = $('.producto select option:selected', row).text();
            $scope.producto.nombre = contenido;
        }else{
            $scope.producto = {};
            $scope.producto.id = null;
            $scope.producto.cantidad = 0;
            $scope.producto.disponibilidad = 0;
        }
    }

});
