angular.module('tabla-pedido')
.controller('ProductocompraController', function ($scope, $location, HttpLocal) {

    $scope.empresas = [];
    $scope.empresa = null;

    $scope.proveedores = [];
    $scope.proveedor_selected = null;

    $scope.productos = [];
    $scope.productos_filtered = [];
    $scope.productos_adds = [];

    $scope.periodo_selected = null;

    $scope.almacenes = [];
    $scope.almacen_selected = null;

    $scope.data = {
        empresa: null,
        almacen_destino: null,
        proveedor: null,
        fecha_compromiso: null
    }

    /**
     *  Comprobar si un proveedor ya se encuentra en la Lista
     * de proveedores a partir de us id
     */
    function in_proveedores(proveedor_id) {
        for (var i = 0; i < $scope.proveedores.length; i++) {
            if ($scope.proveedores[i].id === proveedor_id)
                return true;
        }
        return false;
    }

    /**
     *  Comprobar si un almacen ya se encuentra en la Lista
     * de almacenes a partir de us id
     */
    function in_almacenes(almacen_id) {
        for (var i = 0; i < $scope.almacenes.length; i++) {
            if ($scope.almacenes[i].id === almacen_id)
                return true;
        }
        return false;
    }

    function set_almacen() {
        $scope.data.almacen_destino = this.almacen_selected;
    }

    function set_proveedor() {
        $scope.data.proveedor = this.proveedor_selected;
    }

    function set_fecha_compromiso() {
        var menor = null;
        for (var i = 0; i < $scope.productos.length; i++) {
            if (menor === null || menor.diff($scope.productos[i].fecha_compromiso) < 0)
                menor = $scope.productos[i].fecha_compromiso;
        }
        $scope.data.fecha_compromiso = menor !== null ? menor.format('DD/MM/YYYY H:mm') : "";
    }

    $scope.filtrar = function () {
        today = moment();
        periodo_selected = $scope.periodo_selected = this.periodo_selected;
        proveedor_selected = $scope.proveedor_selected = this.proveedor_selected;
        almacen_selected = $scope.almacen_selected = this.almacen_selected;

        filtro_productos = $scope.productos.filter(function(index) {
            if(proveedor_selected === null || proveedor_selected.length == 0)
                return true;
            return index.proveedor == proveedor_selected;
        });

        filtrado = filtro_productos.filter(function(index) {
            if(almacen_selected === null || almacen_selected.length == 0)
                return true;
            return index.almacen_destino == almacen_selected;
        });

        this.productos_filtered = filtrado.filter(function(index) {
            switch (periodo_selected) {
                case 'semana':
                    return today.diff(index.fecha_compromiso, 'days') < 7;
                case 'mes':
                    return today.diff(index.fecha_compromiso, 'months') < 1;
                case 'tresmeses':
                    return today.diff(index.fecha_compromiso, 'months') < 3;
                case 'vacio':
                    return index.fecha_compromiso === null || index.fecha_compromiso.length == 0;
                case '':
                    return true;
                case null:
                    return true;
                default:
                    return false;
            }
        });

        this.toggle();
    }

    $scope.toggle = function () {
        $scope.productos_adds = this.productos_filtered.filter(function(index) {
            return index.add;
        });
        set_almacen();
        set_proveedor();
        set_fecha_compromiso();
    }


    get_ids = function () {
        ids = [];
        for (var i = 0; i < $scope.productos_adds.length; i++) {
            console.log($scope.productos_adds[i]);
            ids.push($scope.productos_adds[i].id);
        }
        return ids;
    }


    HttpLocal
    .get('/api/compras/producto-compra/sin-pedido/')
    .then(function (res) {
        $scope.productos = res.data;
        $scope.productos_filtered = $scope.productos;
        for (var i = 0; i < $scope.productos.length; i++) {

            // Configurando el formato de la fecha
            if($scope.productos[i].fecha_compromiso !== null)
                $scope.productos[i].fecha_compromiso = moment($scope.productos[i].fecha_compromiso);

            // Si no se encuentra en la lista de proveedores se agrega
            if( ! in_proveedores($scope.productos[i].proveedor) )
                $scope.proveedores.push({
                    id: $scope.productos[i].proveedor,
                    nombre: $scope.productos[i].proveedor_verbose
                })

            // Si no se encuentra en la lista de almacenes se agrega
            if( ! in_almacenes($scope.productos[i].almacen_destino) )
                $scope.almacenes.push({
                    id: $scope.productos[i].almacen_destino,
                    descripcion: $scope.productos[i].almacen_verbose
                })
        }
    }, function (errors) {
        console.log(errors)
    });

    HttpLocal
    .get('/api/empresa/')
    .then(function (res) {
        $scope.empresas = res.data;
    }, function (errors) {
        console.log(errors)
    })

    $scope.steps = [
        {
            templateUrl: '/static/js/compras/pedido/app/steps/1.html',
            hasForm: true,
            title: 'Seleccionar productos'
        },
        {
            templateUrl: '/static/js/compras/pedido/app/steps/2.html',
            hasForm: true,
            title: 'Crear pedido de compra'
        },
        {
            templateUrl: '/static/js/compras/pedido/app/steps/3.html',
            title: 'Confirmación'
        }
    ];

    $scope.cancel = function () {
        BootstrapDialog.confirm({
            title: 'Confirmación',
            message: '¿Confirma que desea cancelar la operación?',
            type: BootstrapDialog.TYPE_WARNING,
            btnCancelLabel: 'Cancelar',
            btnOKLabel: 'Aceptar',
            callback: function(result) {
                if(result) {
                    window.location.href = '/compras/pedido/atender-productos/';
                }
            }
        });
    }

    $scope.finish = function () {

        ids = get_ids();
        data = {
            productos: ids
        };
        console.log('data', data);

        $scope.data.fecha_compromiso = moment($scope.data.fecha_compromiso, 'DD/MM/YYYY H:mm').format('YYYY-MM-DD hh:mm');
        HttpLocal.post('/api/compras/pedido/', $scope.data)
        .then(function (res) {

            $.ajax({
                url: '/api/compras/pedido/'+ res.data.id +'/asociar-productos/',
                type: 'POST',
                data: data
            })
            .done(function(res2) {
                window.location.href = '/compras/pedido/'+ res.data.id +'/modify/';
            })
            .fail(function(errors) {
                console.log(erros);
            })
            .always(function() {
                console.log("complete");
            });

        }, function (errors) {
            console.log(errors);
        })
    }

})
