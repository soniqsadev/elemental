angular.module('tabla-pedido')
.service('HttpLocal', function ($http) {
    this.host = window.location.protocol + '//' + window.location.host;
    this.get = function (path, data) {
        return $http.get(this.host + path, data);
    }
    this.post = function (path, data) {
        return $http.post(this.host + path, data);
    }
})
.factory("AlmacenResource", function ($resource) {
    return $resource(window.location.protocol + '//' + window.location.host + '/api/almacen/almacen/:id/', {id: '@id'}, { update:{method:'patch'} }, { stripTrailingSlashes: false });
})
.factory("ProveedorResource", function ($resource) {
    return $resource(window.location.protocol + '//' + window.location.host + '/api/compras/proveedor/:id/', {id: '@id'}, { update:{method:'patch'} }, { stripTrailingSlashes: false });
})
.factory("PedidoCompraResource", function ($resource) {
    return $resource(window.location.protocol + '//' + window.location.host + '/api/compras/pedido/:id/', {id: '@id'}, { update:{method:'patch'} }, { stripTrailingSlashes: false });
});
