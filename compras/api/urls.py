from django.conf.urls import url

from . import views


urlpatterns = [
    url(regex=r'^proveedor/$',
        view=views.ProveedorList.as_view(),
        name='api.proveedor.list'),

    url(regex=r'^proveedor/(?P<pk>[0-9]+)/$',
        view=views.ProveedorDetail.as_view(),
        name='api.proveedor.detail'),

    url(regex=r'^proveedor/(?P<pk>[0-9]+)/productos/$',
        view=views.get_productos_por_proveedor,
        name='api.proveedor.productos'),


    url(regex=r'^pedido/$',
        view=views.PedidoCompraList.as_view(),
        name='api.compras.pedido.list'),
    url(regex=r'^pedido/(?P<pk>[0-9]+)/$',
        view=views.PedidoCompraDetail.as_view(),
        name='api.compras.pedido.detail'),

    url(regex=r'^pedido/(?P<pk>[0-9]+)/asociar-productos/$',
        view=views.set_productoscompra_enpedido,
        name='api.compras.pedido.productos_a_pedido'),


    url(regex=r'^producto-compra/$',
        view=views.ProductoCompraList.as_view(),
        name='api.compras.producto.list'),

    url(regex=r'^producto-compra/(?P<pk>[0-9]+)/$',
        view=views.ProductoCompraDetail.as_view(),
        name='api.compras.producto.detail'),

    url(regex=r'^producto-compra/sin-pedido/$',
        view=views.get_productoscompra_sinpedido,
        name='api.compras.producto.sin_pedido'),

    url(regex=r'^producto-compra/pedido/(?P<pedido_id>[0-9]+)/$',
        view=views.get_productocompra_for_pedido,
        name='api.productocompra_por_orden.detail'),

    url(regex=r'^producto-compra/orden-venta/(?P<orden_id>[0-9]+)/producto/(?P<producto_id>[0-9]+)/$',
        view=views.get_productocompra_for_orden_and_producto,
        name='api.productocompra_por_orden_y_producto.detail'),

    url(regex=r'^producto-compra/(?P<pk>[0-9]+)/proveedores/$',
        view=views.get_proveedores_por_producto,
        name='api.proveedores_por_productocompra.detail'),

    url(regex=r'^postulacion-proveedor/proveedor/(?P<proveedor_id>[0-9]+)/estereotipo/(?P<estereotipo_id>[0-9]+)/$',
        view=views.get_postulaciones_aprobadas_for_proveedor_and_estereotipo,
        name='api.postulaciones_por_proveedor_y_estereotipo.detail'),

    #
    url(regex=r'^requisicion/$',
        view=views.RequisicionCompraList.as_view(),
        name='api.compras.requisicion.list'),
    url(regex=r'^requisicion/(?P<pk>[0-9]+)/$',
        view=views.RequisicionCompraDetail.as_view(),
        name='api.compras.requisicion.detail'),
    url(regex=r'^requisicion/(?P<orden_id>[0-9]+)/producto/(?P<producto_id>[0-9]+)/$',
        view=views.get_requisicion_for_orden_and_producto,
        name='api.requisicion_por_orden_y_producto.detail'),
]
