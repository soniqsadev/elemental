from rest_framework import serializers
from core.utils.strings import key_generator

from core.models import Moral
from ..models import (PedidoCompra,
                      ProductoCompra,
                      RequisicionCompra,
                      PostulacionProveedor)


class ProveedorSerializer(serializers.ModelSerializer):
    """ Serialiador del modelo Persona Moral (Proveedor) """

    class Meta:
        model = Moral
        fields = '__all__'

    def create(self, validate_data):
        validate_data['is_proveedor'] = True
        return Moral.objects.create(**validate_data)


class PedidoCompraSerializer(serializers.ModelSerializer):
    """ Serialiador del modelo PedidoCompra """

    class Meta:
        model = PedidoCompra
        fields = '__all__'
        # exclude = ['orden_venta',]


class ProductoCompraSerializer(serializers.ModelSerializer):
    """ Serialiador del modelo ProductoCompra """
    estereotipo_verbose = serializers.CharField(required=False)
    proveedor_verbose = serializers.CharField(required=False)
    almacen_verbose = serializers.CharField(required=False)

    class Meta:
        model = ProductoCompra
        fields = '__all__'
        # exclude = ['orden_venta',]


class RequisicionCompraSerializer(serializers.ModelSerializer):
    """ Serialiador del modelo Persona Moral (Proveedor) """

    class Meta:
        model = RequisicionCompra
        fields = '__all__'

    def create(self, validate_data):
        return Moral.objects.create(**validate_data)


class PostulacionProveedorSerializer(serializers.ModelSerializer):

    class Meta:
        model = PostulacionProveedor
        fields = '__all__'
