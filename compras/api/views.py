import json

from django.views.generic.base import TemplateView

from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from core.models import Moral, EstereotipoProducto
from ..models import PedidoCompra, ProductoCompra, Proveedor, RequisicionCompra
from .serializers import (ProveedorSerializer,
                          PedidoCompraSerializer,
                          ProductoCompraSerializer,
                          RequisicionCompraSerializer,
                          PostulacionProveedorSerializer)
from core.api.serializers import EstereotipoProductoSerializer


class ProveedorList(generics.ListCreateAPIView):
    """
    Lista de todos los Morals
    """
    queryset = Moral.objects.filter(is_proveedor=True)
    serializer_class = ProveedorSerializer


class ProveedorDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Ver, Editar y Eliminar un Moral
    """
    queryset = Moral.objects.filter(is_proveedor=True)
    serializer_class = ProveedorSerializer


class PedidoCompraList(generics.ListCreateAPIView):
    """
    Lista de todos los pedidos de compra
    """
    queryset = PedidoCompra.objects.all()
    serializer_class = PedidoCompraSerializer


class PedidoCompraDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Ver, Editar y Eliminar un pedido de compra
    """
    queryset = PedidoCompra.objects.all()
    serializer_class = PedidoCompraSerializer


class ProductoCompraList(generics.ListCreateAPIView):
    """
    Lista de todos los pedidos de compra
    """
    queryset = ProductoCompra.objects.all()
    serializer_class = ProductoCompraSerializer


class ProductoCompraDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Ver, Editar y Eliminar un pedido de compra
    """
    queryset = ProductoCompra.objects.all()
    serializer_class = ProductoCompraSerializer



@api_view(['GET'])
def get_productoscompra_sinpedido(request, *args, **kwargs):

    queryset = ProductoCompra.objects.filter(pedido_compra__isnull=True, proveedor__isnull=False)
    #queryset = ProductoCompra.objects.filter(pedido_compra__isnull=True)

    for elemento in queryset:
        elemento.estereotipo_verbose = elemento.estereotipo.nombre
        elemento.proveedor_verbose = elemento.proveedor.nombre if elemento.proveedor else 'Sin proveedor asignado'
        elemento.almacen_verbose = elemento.almacen_destino.descripcion if elemento.almacen_destino else 'Sin almacen de destino asignado'

    serializer = ProductoCompraSerializer(queryset, many=True)

    return Response( serializer.data )


@api_view(['POST'])
def set_productoscompra_enpedido(request, *args, **kwargs):
    """
    Asignar a un pedido de compra
    """
    print('POST', request.POST)
    try:
        pedido = PedidoCompra.objects.get(pk=kwargs['pk'])
        productos_ids = request.POST.getlist('productos[]', 'vacio')

        productos = []

        for id in productos_ids:
            try:
                producto = ProductoCompra.objects.get(pk=id)
                producto.pedido_compra = pedido
                producto.save()
                productos.append(producto)
            except ProductoCompra.DoesNotExist:
                pass

        serializer = ProductoCompraSerializer(productos, many=True)
        return Response(serializer.data)
    except PedidoCompra.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def get_productocompra_for_pedido(request, *args, **kwargs):
    try:
        producto_compra = ProductoCompra.objects.filter(
                                pedido_compra=kwargs['pedido_id']
                          )
        for producto in producto_compra:
            producto.estereotipo_verbose = producto.estereotipo.nombre

        serializer = ProductoCompraSerializer(producto_compra,
                                              many=True)
        return Response(serializer.data)
    except ProductoCompra.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def get_productocompra_for_orden_and_producto(request, *args, **kwargs):
    try:
        print(kwargs)
        producto_compra = ProductoCompra.objects.get(
                                orden_venta=kwargs['orden_id'],
                                estereotipo=kwargs['producto_id']
                          )
        serializer = ProductoCompraSerializer(producto_compra)
        return Response(serializer.data)
    except ProductoCompra.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def get_productos_por_proveedor(request, *args, **kwargs):
    try:
        proveedor = Proveedor.objects.get(pk=kwargs['pk'])
        productos = EstereotipoProducto.objects.filter(
                        categoria__proveedores=proveedor,
                        marca__proveedores=proveedor)
        serializer = EstereotipoProductoSerializer(productos, many=True)
        return Response(serializer.data)
    except Proveedor.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def get_proveedores_por_producto(request, *args, **kwargs):
    try:
        estereotipo = EstereotipoProducto.objects.get(pk=kwargs['pk'])
        proveedores = []
        if hasattr(estereotipo, 'requisicion_compra'):
            postulaciones = estereotipo.requisicion_compra.postulaciones.filter(aprobada=True)
            for postulacion in postulaciones:
                proveedores.append(postulacion.proveedor)
        serializer = ProveedorSerializer(proveedores, many=True)
        return Response(serializer.data)

    except EstereotipoProducto.DoesNotExist as e:
        return Response(status=status.HTTP_404_NOT_FOUND)


class RequisicionCompraList(generics.ListCreateAPIView):
    """
    Lista de todos los requisiciones de compra
    """
    queryset = RequisicionCompra.objects.all()
    serializer_class = RequisicionCompraSerializer


class RequisicionCompraDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Ver, Editar y Eliminar una requisicion de compra
    """
    queryset = RequisicionCompra.objects.all()
    serializer_class = RequisicionCompraSerializer


@api_view(['GET'])
def get_requisicion_for_orden_and_producto(request, *args, **kwargs):
    """
    Obtener requisicion dada la orden de venta y el estereotipo
    """
    try:
        pedido = RequisicionCompra.objects.get(
                    orden_venta=kwargs['orden_id'],
                    estereotipo=kwargs['producto_id']
                )
        serializer = PedidoCompraSerializer(pedido)
        return Response(serializer.data)
    except PedidoCompra.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def get_postulaciones_aprobadas_for_proveedor_and_estereotipo(request, *args, **kwargs):
    try:
        proveedor = Proveedor.objects.get(pk=kwargs['proveedor_id'])
    except Proveedor.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    try:
        estereotipo = EstereotipoProducto.objects.get(pk=kwargs['estereotipo_id'])
    except EstereotipoProducto.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    postulaciones = proveedor.get_postulacion_por_estereotipo(estereotipo)
    serializer = PostulacionProveedorSerializer(postulaciones)
    return Response(serializer.data)
