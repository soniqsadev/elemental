from django.conf.urls import url

from .views import Proveedor, PedidoCompra, RequisicionCompra


urlpatterns = [

    url(regex=r'^pedido/$',
        view=PedidoCompra.ListView.as_view(),
        name='compras.pedido.list'),

    url(regex=r'^pedido/create/$',
        view=PedidoCompra.CreateView.as_view(),
        name='compras.pedido.create'),

    url(regex=r'^pedido/new/$',
        view=PedidoCompra.NewView.as_view(),
        name='compras.pedido.new'),

    url(regex=r'^pedido/atender-productos/$',
        view=PedidoCompra.AtenderProductosView.as_view(),
        name='compras.pedido.atender_productos'),

    url(regex=r'^pedido/(?P<pk>[0-9]+)/$',
        view=PedidoCompra.DetailView.as_view(),
        name='compras.pedido.detail'),

    url(regex=r'^pedido/(?P<pk>[0-9]+)/activar/$',
        view=PedidoCompra.ActivarView.as_view(),
        name='compras.pedido.activar'),


    url(regex=r'^pedido/(?P<pk>[0-9]+)/edit/$',
        view=PedidoCompra.UpdateView.as_view(),
        name='compras.pedido.update'),

    url(regex=r'^pedido/(?P<pk>[0-9]+)/modify/$',
        view=PedidoCompra.ModifyView.as_view(),
        name='compras.pedido.modify'),

    url(regex=r'^pedido/(?P<pk>[0-9]+)/delete/$',
        view=PedidoCompra.DeleteView.as_view(),
        name='compras.pedido.delete'),


    url(regex=r'^proveedor/$',
        view=Proveedor.ListView.as_view(),
        name='proveedor.list'),

    url(regex=r'^proveedor/create/$',
        view=Proveedor.CreateView.as_view(),
        name='proveedor.create'),

    url(regex=r'^proveedor/(?P<pk>[0-9]+)/$',
        view=Proveedor.DetailView.as_view(),
        name='proveedor.detail'),

    url(regex=r'^proveedor/(?P<pk>[0-9]+)/edit/$',
        view=Proveedor.UpdateView.as_view(),
        name='proveedor.update'),

    url(regex=r'^proveedor/(?P<pk>[0-9]+)/delete/$',
        view=Proveedor.DeleteView.as_view(),
        name='proveedor.delete'),


    url(regex=r'^requisicion/$',
        view=RequisicionCompra.RequisicionCompraListView.as_view(),
        name='compras.requisicion.list'),

    url(regex=r'^requisicion/create/$',
        view=RequisicionCompra.RequisicionCompraCreateView.as_view(),
        name='compras.requisicion.create'),

    url(regex=r'^requisicion/(?P<pk>[0-9]+)/edit/$',
        view=RequisicionCompra.RequisicionCompraUpdateView.as_view(),
        name='compras.requisicion.edit'),

    url(regex=r'^requisicion/(?P<pk>[0-9]+)/cotizadores/$',
        view=RequisicionCompra.AsociarCotizadoresView.as_view(),
        name='compras.requisicion.cotizadores'),

    url(regex=r'^requisicion/(?P<pk>[0-9]+)/cotizadores/productos-cotizadores/$',
        view=RequisicionCompra.ProductosCatizadoresView.as_view(),
        name='compras.requisicion.cotizadores.productos'),

    url(regex=r'^requisicion/(?P<pk>[0-9]+)/cotizadores/aprobar/$',
        view=RequisicionCompra.AprobarPostulacionView.as_view(),
        name='compras.requisicion.cotizadores.aprobar'),

]
