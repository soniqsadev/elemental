from django.views.generic.list import ListView as GenericListView
from django.views.generic.detail import DetailView as GenericDetailView
from django.views.generic.edit import DeleteView as GenericDeleteView
from django.views.generic import View
from django.forms import modelformset_factory
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse_lazy

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from core.models import Telefono
from core.views import Moral as MoralViews
from core.models import Moral as MoralModel
from ..models import Proveedor
from ..forms.Proveedor import ProveedorForm

SUCCESS_URL = reverse_lazy('proveedor.list')

def permissions_show(user):
    return user.has_perm('compras.can_add_proveedor') or user.has_perm('compras.can_change_proveedor') or user.has_perm('compras.can_delete_proveedor')

class ListView(GenericListView):
    context_object_name = 'proveedores'
    model = Proveedor
    page_kwarg = 'page'
    paginate_by = 10
    template_name = 'compras/proveedor/list.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(ListView, self).dispatch(*args, **kwargs)


class CreateView(View):
    template_name = 'compras/proveedor/create.html'
    success_url = SUCCESS_URL

    def __init__(self, *args, **kwargs):
        super(CreateView, self).__init__(*args, **kwargs)
        self.TelefonoFormSet = modelformset_factory(Telefono,
                                                         can_delete=True,
                                                         max_num=5,
                                                         min_num=0,
                                                         validate_max=True,
                                                         fields= ('id', 'tipo', 'numero')
                                                    )

    def get(self, *args, **kwargs):
        form = ProveedorForm()
        form.telefono_form = self.TelefonoFormSet(queryset=Telefono.objects.none())
        return render(self.request, self.template_name, {'form':form})

    def post(self, *args, **kwargs):
        print(self.request.POST)
        form = ProveedorForm(self.request.POST, self.request.FILES)
        telefonos = self.TelefonoFormSet(self.request.POST)
        if form.is_valid() and telefonos.is_valid():
            form.instance.is_proveedor = True
            form.save()
            for telefono in telefonos:
                telefono.instance.persona = form.instance
            telefonos.save()
            route_base = self.request.POST.get('route_base', None)
            route_redirect = self.request.POST.get('route_redirect', None)
            if route_base is not None and len(route_base) > 0 and route_redirect is not None and len(route_redirect) > 0:
                url_base = reverse_lazy(route_base, args=[form.instance.id, ])
                url_redirect = reverse_lazy(route_redirect, args=[form.instance.id, ])
                url = "%s?redirection=%s" % (url_base, url_redirect)
            else:
                url = self.success_url
            return redirect(url)
        form.telefono_form = telefonos
        return render(self.request, self.template_name, {'form': form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('compras.can_add_proveedor'))
    def dispatch(self, *args, **kwargs):
        return super(CreateView, self).dispatch(*args, **kwargs)


class DetailView(GenericDetailView):
    model = Proveedor
    context_object_name = 'proveedor'
    template_name = 'compras/proveedor/show.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator(user_passes_test(permissions_show))
    def dispatch(self, *args, **kwargs):
        return super(DetailView, self).dispatch(*args, **kwargs)


class UpdateView(View):
    template_name = 'compras/proveedor/create.html'
    success_url = SUCCESS_URL

    def __init__(self, *args, **kwargs):
        super(UpdateView, self).__init__(*args, **kwargs)
        self.TelefonoFormSet = modelformset_factory(Telefono,
                                                    can_delete=True,
                                                    max_num=5,
                                                    min_num=0,
                                                    validate_max=True,
                                                    fields=('id', 'tipo', 'numero')
                                                    )

    def get(self, *args, **kwargs):
        proveedor = get_object_or_404(Proveedor, pk=kwargs['pk'])
        form = ProveedorForm(instance=proveedor)
        form.telefono_form = self.TelefonoFormSet(queryset=proveedor.telefonos.all())
        return render(self.request, self.template_name, {'form':form})


    def post(self, *args, **kwargs):
        proveedor = get_object_or_404(Proveedor, pk=kwargs['pk'])
        form = ProveedorForm(self.request.POST, self.request.FILES, instance=proveedor)
        telefonos = self.TelefonoFormSet(self.request.POST)
        for telefono in telefonos:
            telefono.instance.persona = proveedor
        if form.is_valid() and telefonos.is_valid():
            form.save()
            telefonos.save()
            return redirect(self.success_url)

        form.telefono_form = telefonos
        return render(self.request, self.template_name, {'form': form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('compras.can_change_proveedor'))
    def dispatch(self, *args, **kwargs):
        return super(UpdateView, self).dispatch(*args, **kwargs)


class DeleteView(GenericDeleteView):
    model = Proveedor
    context_object_name = 'proveedor'
    success_url = SUCCESS_URL
    template_name = 'compras/proveedor/delete.html'

    def get_context_data(self, **kwargs):
        ctx = super(DeleteView, self).get_context_data(**kwargs)
        ctx['route_success'] = 'proveedor.list'
        return ctx

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('compras.can_delete_proveedor'))
    def dispatch(self, *args, **kwargs):
        return super(DeleteView, self).dispatch(*args, **kwargs)
