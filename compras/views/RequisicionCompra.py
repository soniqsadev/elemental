from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import (login_required,
                                            permission_required,
                                            user_passes_test)
from django.views.decorators.http import (require_http_methods,
                                          require_GET,
                                          require_POST)

from django.views.generic import View
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.forms import modelformset_factory

from ..models import RequisicionCompra, PostulacionProveedor
from ..forms.RequisicionCompra import RequisicionCompraForm, ActivarForm


SUCCESS_URL = reverse_lazy('compras.requisicion.list')


def permissions_show(user):
    return user.has_perm('compras.can_add_requisicioncompra') or user.has_perm('compras.can_change_requisicioncompra') or user.has_perm('compras.can_delete_requisicioncompra')


class RequisicionCompraListView(ListView):
    context_object_name = 'requisiciones'
    model = RequisicionCompra
    template_name = 'compras/requisicion/list.html'


class RequisicionCompraCreateView(CreateView):
    model = RequisicionCompra
    form_class = RequisicionCompraForm
    success_url = SUCCESS_URL
    template_name = 'compras/requisicion/form.html'


class RequisicionCompraUpdateView(UpdateView):
    model = RequisicionCompra
    form_class = RequisicionCompraForm
    template_name = 'compras/requisicion/form.html'
    success_url = SUCCESS_URL


class AsociarCotizadoresView(View):
    template_name = 'compras/requisicion/asociar_cotizadores.html'
    success_url = SUCCESS_URL

    def __init__(self, *args, **kwargs):
        super(AsociarCotizadoresView, self).__init__(*args, **kwargs)
        self.PostulacionProveedorFormSet = modelformset_factory(
                                                PostulacionProveedor,
                                                max_num=50,
                                                extra=0,
                                                # extra=1,
                                                can_delete=True,
                                                validate_max=True,
                                                fields=('proveedor', )
                                            )

    def get(self, *args, **kwargs):
        requisicion = get_object_or_404(RequisicionCompra, pk=kwargs['pk'])

        form_postulaciones = self.PostulacionProveedorFormSet(
                                    queryset=requisicion.postulaciones.all()
                                )
        return render(self.request,
                      self.template_name,
                      {
                            'requisicion': requisicion,
                            'form': form_postulaciones
                      })

    def post(self, *args, **kwargs):
        requisicion = get_object_or_404(RequisicionCompra, pk=kwargs['pk'])
        print(self.request.POST)
        form_postulaciones = self.PostulacionProveedorFormSet(
                                                    self.request.POST
                                                )
        for postulacion in form_postulaciones:
            postulacion.instance.requisicion = requisicion

        if form_postulaciones.is_valid():
            # Salvar proveedores
            form_postulaciones.save()
            # redireccionar a la interfaz principal
            return redirect(self.success_url)

        return render(self.request,
                      self.template_name,
                      {
                            'requisicion': requisicion,
                            'form': form_postulaciones
                      })


class ProductosCatizadoresView(View):
    template_name = 'compras/requisicion/productos_cotizadores.html'
    success_url = SUCCESS_URL

    def __init__(self, *args, **kwargs):
        super(ProductosCatizadoresView, self).__init__(*args, **kwargs)
        self.ProductoProveedorFormSet = modelformset_factory(
                                            PostulacionProveedor,
                                            max_num=50,
                                            validate_max=True,
                                            extra=0,
                                            fields=(
                                                'precio_unidad',
                                                'tiempo_entrega',
                                                'stock_estado',
                                                'stock_ciudad',
                                                'efectivo',
                                                'prepago',
                                                'pago_entrega',
                                                'credito',
                                                'plazo_pago',
                                                'cotizacion_fisica',
                                                )
                                            )

    def get(self, *args, **kwargs):
        requisicion = get_object_or_404(RequisicionCompra, pk=kwargs['pk'])
        proveedores = requisicion.get_proveedores()

        form_precios = self.ProductoProveedorFormSet(
                            queryset=requisicion.postulaciones.all()
                        )
        return render(self.request,
                      self.template_name,
                      {
                            'form': form_precios,
                            'proveedores': proveedores
                      })

    def post(self, *args, **kwargs):
        requisicion = get_object_or_404(RequisicionCompra, pk=kwargs['pk'])

        form_precios = self.ProductoProveedorFormSet(self.request.POST, self.request.FILES)
        print(self.request.FILES)

        if form_precios.is_valid():
            form_precios.save()
            requisicion.por_validar_productos()
            return redirect(self.success_url)

        proveedores = requisicion.get_proveedores()
        return render(self.request,
                      self.template_name,
                      {
                            'form': form_precios,
                            'proveedores': proveedores
                      })


class AprobarPostulacionView(View):
    template_name = 'compras/requisicion/aprobar_postulacion.html'
    success_url = SUCCESS_URL

    def __init__(self, *args, **kwargs):
        super(AprobarPostulacionView, self).__init__(*args, **kwargs)
        self.ProductoProveedorFormSet = modelformset_factory(
                                            PostulacionProveedor,
                                            max_num=50,
                                            validate_max=True,
                                            extra=0,
                                            fields=(
                                                'aprobada',
                                                )
                                            )

    def get(self, *args, **kwargs):
        requisicion = get_object_or_404(RequisicionCompra, pk=kwargs['pk'])
        proveedores = requisicion.get_proveedores()

        postulaciones_form = self.ProductoProveedorFormSet(
                            queryset=requisicion.postulaciones.all()
                        )
        return render(self.request,
                      self.template_name,
                      {
                            'form': postulaciones_form,
                            'proveedores': proveedores
                      })

    def post(self, *args, **kwargs):
        requisicion = get_object_or_404(RequisicionCompra, pk=kwargs['pk'])
        print(self.request.POST)

        postulaciones_form = self.ProductoProveedorFormSet(self.request.POST)

        if postulaciones_form.is_valid():
            postulaciones_form.save()
            requisicion.aprobada()
            return redirect(self.success_url)

        proveedores = requisicion.get_proveedores()
        return render(self.request,
                      self.template_name,
                      {
                            'form': postulaciones_form,
                            'proveedores': proveedores
                      })
