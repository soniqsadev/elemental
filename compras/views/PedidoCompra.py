from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from django.forms import modelformset_factory
from django.views.generic import View
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView

from ..models import PedidoCompra, ProductoCompra
from ..forms.PedidoCompra import PedidoCompraForm, ActivacionForm
from almacen.models import ActualizacionCompra


SUCCESS_URL = reverse_lazy('compras.pedido.list')

def permissions_show(user):
    return user.has_perm('compras.can_add_pedidocompra') or user.has_perm('compras.can_change_pedidocompra') or user.has_perm('compras.can_delete_pedidocompra')

class ListView(ListView):
    context_object_name = 'pedidos'
    model = PedidoCompra
    template_name = 'compras/pedido/list.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(ListView, self).dispatch(*args, **kwargs)

class CreateView(CreateView):
    model = PedidoCompra
    form_class = PedidoCompraForm
    success_url = SUCCESS_URL
    template_name = 'compras/pedido/create.html'

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('compras.can_add_pedidocompra'))
    def dispatch(self, *args, **kwargs):
        return super(CreateView, self).dispatch(*args, **kwargs)

class NewView(View):
    template_name = 'compras/pedido/new.html'

    def get(self, *args, **kwargs):
        form = PedidoCompraForm()
        return render(self.request, self.template_name, {'form':form})

    def post(self, *args, **kwargs):
        form = PedidoCompraForm(self.request.POST)
        if form.is_valid():
            form.save()
            return redirect( reverse_lazy('compras.pedido.modify', args=[form.instance.id]) )

        return render(self.request, self.template_name, {'form': form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('compras.can_add_pedidocompra'))
    def dispatch(self, *args, **kwargs):
        return super(NewView, self).dispatch(*args, **kwargs)


class AtenderProductosView(View):
    template_name = 'compras/pedido/atender_productos.html'

    def get(self, *args, **kwargs):
        return render(self.request, self.template_name)

    def post(self, *args, **kwargs):
        print(self.request.POST)
        return render(self.request, self.template_name)


class DetailView(DetailView):
    model = PedidoCompra
    context_object_name = 'pedido'
    template_name = 'compras/pedido/show.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(DetailView, self).dispatch(*args, **kwargs)

class UpdateView(UpdateView):
    model = PedidoCompra
    form_class = PedidoCompraForm
    context_object_name = 'hospital'
    success_url = SUCCESS_URL
    template_name = 'compras/pedido/create.html'

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('compras.can_change_pedidocompra'))
    def dispatch(self, *args, **kwargs):
        return super(UpdateView, self).dispatch(*args, **kwargs)


class ModifyView(View):
    template_name = 'compras/pedido/modify.html'

    def __init__(self, *args, **kwargs):
        super(ModifyView, self).__init__(*args, **kwargs)
        self.ProductoOrdenFormSet = modelformset_factory(ProductoCompra,
                                                             can_delete=True,
                                                             max_num=20,
                                                             validate_max=True,
                                                             fields= ('estereotipo', 'cantidad', 'precio_unidad')
                                                        )

    def get(self, *args, **kwargs):
        pedido_compra = get_object_or_404(PedidoCompra, pk=kwargs['pk'])
        form = PedidoCompraForm(instance=pedido_compra)
        form.form_productos = self.ProductoOrdenFormSet(queryset=pedido_compra.productos_compras.all())
        return render(self.request, self.template_name, {'form':form})

    def post(self, *args, **kwargs):
        redireccion = self.request.POST.get('redireccion', 'list')
        pedido_compra = get_object_or_404(PedidoCompra, pk=kwargs['pk'])
        form = PedidoCompraForm(self.request.POST, instance=pedido_compra)
        productos = self.ProductoOrdenFormSet(self.request.POST)
        for producto in productos:
            producto.instance.pedido_compra = form.instance
        if form.is_valid() and productos.is_valid():
            form.save()
            productos.save()
            if redireccion == 'list':
                return redirect( reverse_lazy('compras.pedido.detail', args=[form.instance.id]) )
            else:
                return redirect( reverse_lazy('compras.pedido.modify', args=[form.instance.id]) )

        form.form_productos = productos
        return render(self.request, self.template_name, {'form': form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('compras.can_change_pedidocompra'))
    def dispatch(self, *args, **kwargs):
        return super(ModifyView, self).dispatch(*args, **kwargs)


class DeleteView(DeleteView):
    model = PedidoCompra
    success_url = SUCCESS_URL
    context_object_name = 'pedido'
    template_name = 'compras/pedido/delete.html'

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('compras.can_delete_pedidocompra'))
    def dispatch(self, *args, **kwargs):
        return super(DeleteView, self).dispatch(*args, **kwargs)


class ActivarView(View):

    def get(self, *args, **kwargs):
        pedido = get_object_or_404(PedidoCompra, pk=kwargs['pk'])
        form = ActivacionForm(instance=pedido)
        return render(self.request, 'compras/pedido/confirmar_activacion.html', {'form':form, 'pedido':pedido})

    def post(self, *args, **kwargs):
        pedido = get_object_or_404(PedidoCompra, pk=kwargs['pk'])
        form = ActivacionForm(self.request.POST, instance=pedido)
        if form.is_valid():
            pedido.activar(form.instance.almacen_destino, fecha_compromiso=form.instance.fecha_compromiso)
            return redirect( reverse_lazy('compras.pedido.detail', args=[pedido.id,]) )
        # si falla la validación del formulario
        return render(self.request, 'compras/pedido/confirmar_activacion.html', {'form':form, 'pedido':pedido})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('compras.can_change_pedidocompra'))
    def dispatch(self, *args, **kwargs):
        return super(ActivarView, self).dispatch(*args, **kwargs)
