from django.apps import AppConfig


class ComprasConfig(AppConfig):
    name = 'compras'
    verbose_name = 'Compras'

    # def ready(self):
    #     from .signals.Receivers import requisicioncompra_post_save, postulacionproveedor_post_save
