from decimal import Decimal

from django.db import models
from django.core.validators import MinValueValidator

from core.models import (Moral,
                         CategoriaProducto,
                         MarcaProducto,
                         EstereotipoProducto)
from division_territorial.models import Estado, Ciudad
from .RequisicionCompra import PostulacionProveedor


class Proveedor(Moral):

    class Meta:
        verbose_name = 'Proveedor'
        verbose_name_plural = 'Proveedores'

    def get_postulacion_por_estereotipo(self, estereotipo):
        try:
            postulacion_aprobada = PostulacionProveedor.objects.get(
                                    proveedor=self,
                                    aprobada=True,
                                    requisicion=estereotipo.requisicion_compra)
        except PostulacionProveedor.DoesNotExist:
            postulacion_aprobada = []

        return postulacion_aprobada


class ProveedorEsteretipo(models.Model):
    TIPOS = (
        ('distribuidor', 'Distribuidor'),
        ('fabricante', 'Fabricante')
    )
    proveedor = models.ForeignKey(Proveedor, related_name='proveedores_estereotipos', on_delete=models.CASCADE)
    estereotipo = models.ForeignKey(EstereotipoProducto, related_name='proveedores_estereotipos', on_delete=models.CASCADE)
    tipo = models.CharField(max_length=16, choices=TIPOS, default='distribuidor')
    precio_unidad = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Precio por unidad', validators=[MinValueValidator(Decimal('0.01'))])
    tiempo_entrega = models.CharField(max_length=16, verbose_name='Tiempo de entrega', null=True, blank=True)
    estado_stock = models.ForeignKey(Estado, verbose_name='Localización del stock(Estado)', null=True, blank=True)
    ciudad_stock = models.ForeignKey(Ciudad, verbose_name='Localizacion del stock(Delegación)', null=True, blank=True)
    pagos_antes = models.BooleanField(verbose_name='Pagar antes de envío del producto', default=True)
    pagos_despues = models.BooleanField(verbose_name='Pagar despues del envío del producto', default=False)
    pago_efectivo = models.BooleanField(verbose_name='Acepta pagos en efectivo', null=False)
    pago_electronico = models.BooleanField(verbose_name='Acepta pagos por via electrónica',default=False)

    class Meta:
        verbose_name = 'ProveedorEsteretipo'
        verbose_name_plural = 'ProveedorEsteretipos'

    def __str__(self):
        return "%s: %s" % (self.proveedor, self.estereotipo)
