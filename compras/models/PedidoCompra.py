from decimal import Decimal

from django.shortcuts import get_object_or_404
from django.db import models
from django.core.validators import MinValueValidator

from .Proveedor import Proveedor
from core.models import Natural, Moral, EstereotipoProducto, Producto, PropiedadEstereotipo
from ventas.models import OrdenVenta
from almacen.models import ActualizacionCompra, Almacen


class PedidoCompra(models.Model):
    ESTADOS = [
        ('porenviar', 'Por enviar al proveedor'),
        ('enesperaproducto', 'Esperando productos'),
        ('parcialmentecumplida', 'Productos entregados parcialmente'),
        ('productosentregados', 'Productos entregados'),
        ('cancelado', 'Cancelado'),
    ]
    status = models.CharField(max_length=100, choices=ESTADOS, default='porenviar')
    empresa = models.ForeignKey(Moral, related_name='compras_de_producto')
    orden_venta = models.ForeignKey(OrdenVenta, related_name='pedidos_de_compra', null=True, blank=True, on_delete=models.SET_NULL)
    proveedor = models.ForeignKey(Proveedor, related_name='compras_de_producto_como_proveedor', null=True)
    almacen_destino = models.ForeignKey(Almacen, related_name='pedidos_de_compra_como_destino', verbose_name='Almacén donde debe ser recibidos los producto')
    fecha_compromiso = models.DateTimeField(verbose_name='Fecha de compromiso', null=True, blank=True)
    fecha_recibido = models.DateTimeField(blank=True, null=True, verbose_name='Fecha de recepción')
    fecha_creacion = models.DateTimeField(blank=True, auto_now_add=True)
    fecha_modificacion = models.DateTimeField(blank=True, auto_now=True)

    def verbose_status(self):
        for estado in self.ESTADOS:
            if estado[0] == self.status:
                return estado[1]
        return None

    def total(self):
        """
        Total del pedido de venta entre todos los productos
        """
        total = 0
        for producto_compra in self.productos_compras.all():
            total = total + producto_compra.costo()
        return total

    def cantidad(self):
        suma = 0
        for producto_compra in self.productos_compras.all():
            suma = suma + producto_compra.cantidad
        return suma

    def generar_actualizacion_compra(self, almacen_destino, fecha_compromiso):
        return ActualizacionCompra.objects.create(**{
            'pedido': self,
            'tipo': 'entrada',
            'motivo': 'recepcionproductos',
            'almacen_origen': Almacen.objects.get(pk=1), # Almacen virtual del proveedor
            'almacen_destino': almacen_destino,
            'fecha_inicio': fecha_compromiso,
            'fecha_fin': fecha_compromiso,
        })


    #################### ESTADOS DEL PEDIDO #######################

    def activar(self, almacen_destino=None, fecha_compromiso=None):
        """
        Activar pedido de compra
        """
        if self.status == 'porenviar':
            # si no se suministra almacen de destino se usa el almacen de destino del pedido
            almacen = self.almacen_destino if almacen_destino == None else almacen_destino
            # actulizacion del pedido de compra
            self.fecha_compromiso = fecha_compromiso if fecha_compromiso!=None else self.fecha_compromiso
            fecha_compromiso = self.fecha_compromiso
            self.status = 'enesperaproducto'
            self.save()
            # creación de la actualización futura en el almacén
            self.generar_actualizacion_compra(almacen, fecha_compromiso)

    def productos_entregados(self):
        self.status = 'productosentregados'
        self.save()

    def cancelar(self):
        self.status = 'cancelado'
        self.save()

    ##################################################################

    class Meta:
        verbose_name = 'Orden de compra'
        verbose_name_plural = 'Ordenes de compra'

    # def __str__(self):
    #     return str( self.total() )


class ProductoCompra(models.Model):
    """
    Lineas de la ordend de compra
    """
    estereotipo = models.ForeignKey(EstereotipoProducto,
                                    related_name='productos_compras',
                                    verbose_name='Producto',
                                    on_delete=models.DO_NOTHING)
    pedido_compra = models.ForeignKey(PedidoCompra,
                                      related_name='productos_compras',
                                      null=True,
                                      blank=True,
                                      on_delete=models.SET_NULL)
    orden_venta = models.ForeignKey(OrdenVenta,
                                    related_name='productos_compras',
                                    null=True,
                                    blank=True,
                                    on_delete=models.SET_NULL)
    proveedor = models.ForeignKey(Proveedor,
                                  related_name='productos_compras',
                                  null=True)
    fecha_compromiso = models.DateTimeField(verbose_name='Fecha de compromiso',
                                            null=True,
                                            blank=True)
    almacen_destino = models.ForeignKey(Almacen,
                                        null=True,
                                        related_name='productos_de_compra_como_destino',
                                        verbose_name='Almacén donde debe ser recibido el producto')
    cantidad = models.IntegerField(default=1)
    precio_unidad = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Precio por unidad', validators=[MinValueValidator(Decimal('0.01'))], null=True)
    propiedades = models.ManyToManyField(PropiedadEstereotipo, related_name='productos_compras', null=True)

    def costo(self):
        if self.precio_unidad:
            return self.cantidad * self.precio_unidad
        return self.cantidad

    class Meta:
        verbose_name = 'ProductoCompra'
        verbose_name_plural = 'ProductoCompras'

    def __str__(self):
        return str(self.id)
