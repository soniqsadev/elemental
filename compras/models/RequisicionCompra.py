import os
from decimal import Decimal

from django.db import models
from django.core.validators import MinValueValidator

# from .Proveedor import Proveedor
from core.models import EstereotipoProducto
from ventas.models import OrdenVenta
from division_territorial.models import Estado, Ciudad


class RequisicionCompra(models.Model):
    """

    """
    STATUS = (
        ('poriniciar', 'Por iniciar'),
        ('esperandovalidacion', 'Esperando validación de productos'),
        ('aprobado', 'Aprobado')
    )
    codigo = models.CharField(max_length=32,
                              null=True,
                              blank=True,
                              verbose_name='Código')
    status = models.CharField(max_length=32,
                              choices=STATUS,
                              default='poriniciar')
    orden_venta = models.ForeignKey(OrdenVenta,
                                    related_name='requisiciones_compra',
                                    null=True,
                                    blank=True,
                                    on_delete=models.SET_NULL)
    estereotipo = models.OneToOneField(EstereotipoProducto,
                                       related_name='requisicion_compra',
                                       null=True,
                                       verbose_name='Producto',
                                       on_delete=models.CASCADE)
    cantidad = models.IntegerField(default=1)

    def verbose_status(self):
        for estado in self.STATUS:
            if estado[0] == self.status:
                return estado[1]
        return None

    def por_validar_productos(self):
        self.status = 'esperandovalidacion'
        self.save()

    def aprobada(self):
        self.status = 'aprobado'
        self.save()

    class Meta:
        verbose_name = 'Requisicion de Compra'
        verbose_name_plural = 'Requisiciones de Compras'

    def get_proveedores(self):
        proveedores = []
        for postulacion in self.postulaciones.all():
            proveedores.append(postulacion.proveedor)
        return proveedores




class PostulacionProveedor(models.Model):
    """

    """

    def path_to_cotizacion_proveedor(self, instance, filename):
        return os.path.join('proveedores', instance.id, 'cotizaciones', filename)

    requisicion = models.ForeignKey(RequisicionCompra,
                                    related_name='postulaciones',
                                    on_delete=models.DO_NOTHING)
    proveedor = models.ForeignKey('Proveedor', related_name='postulaciones', on_delete=models.DO_NOTHING)
    aprobada = models.BooleanField(default=False)
    precio_unidad = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Precio por unidad', null=True, validators=[MinValueValidator(Decimal('0.01'))])
    tiempo_entrega = models.CharField(max_length=64, null=True)
    stock_estado = models.ForeignKey(Estado, null=True, blank=True)
    stock_ciudad = models.ForeignKey(Ciudad, null=True, blank=True, verbose_name='Delegación')
    efectivo = models.BooleanField(default=False)
    prepago = models.BooleanField(default=True, verbose_name='Pago anticipado')
    pago_entrega = models.BooleanField(default=False, verbose_name='Pago contra entrega')
    credito = models.BooleanField(default=False)
    plazo_pago = models.CharField(blank=True, null=True, max_length=64)
    cotizacion_fisica = models.FileField(upload_to=os.path.join('proveedores', 'cotizaciones'), null=True, blank=True)

    class Meta:
        verbose_name = 'Postulacion de Proveedor'
        verbose_name_plural = 'Postulaciones de Proveedors'

    def add_precios_proveedor(self, proveedor):
        """
        Crea los registros con los datos propuestos por un proveedor para un producto
        """
        for especificacion_producto in self.requisicion.lineas.all():
            print('proveedor: %s <-> especificacion de producto: %s' % (proveedor, especificacion_producto  ) )
            obj, created = ProductoProveedor.objects.get_or_create(
                                    proveedor=proveedor,
                                    especificacion_producto=especificacion_producto
                                )

    def __str__(self):
        return "%s" % self.proveedor
