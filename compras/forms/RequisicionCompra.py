from django import forms

from core.forms.custom import Searchable
from core.models import EstereotipoProducto
from division_territorial.forms import CiudadField, EstadoField
from ..models import RequisicionCompra, Proveedor

class RequisicionCompraForm(forms.ModelForm):
    estereotipo = Searchable.ChoiceSearchableField(
                            queryset=EstereotipoProducto.objects.all()
                    )

    class Meta:
        model = RequisicionCompra
        fields = ['codigo', 'estereotipo', 'cantidad']

    def __init__(self, *args, **kwargs):
        super(RequisicionCompraForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(RequisicionCompraForm, self).clean()
        return cleaned_data


class ActivarForm(forms.Form):
    proveedores = forms.ModelMultipleChoiceField(
                                queryset=None,
                                widget=forms.CheckboxSelectMultiple
                            )

    def __init__(self, *args, **kwargs):
        queryset_proveedores = kwargs.pop('queryset_proveedores', Proveedor.objects.none())
        super(ActivarForm, self).__init__(*args, **kwargs)
        print('queryset_proveedores', queryset_proveedores)
        self.fields['proveedores'].queryset = queryset_proveedores

    def clean(self):
        cleaned_data = super(ActivarForm, self).clean()
        return cleaned_data
