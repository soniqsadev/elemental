from django.forms import ModelForm, ModelChoiceField
from django import forms

from core.forms.custom import AddMore, DatePicker, SelectDependent, Searchable
from core.forms.Moral import MoralSimpleForm
from core.models import Moral, EstereotipoProducto
from ..models import PedidoCompra, Proveedor
from almacen.models import Almacen


class PedidoCompraForm(ModelForm):
    # proveedor = AddMore.ChoiceAddMoreField(
    #                                 ruta='api.proveedor.list',
    #                                 form=MoralSimpleForm(),
    #                                 kv={'key':'id', 'value':'nombre'},
    #                                 queryset=Moral.objects.filter(is_proveedor=True),
    #                             )
    empresa = ModelChoiceField(queryset=Moral.objects.filter(is_me=True))
    proveedor = Searchable.ChoiceSearchableField(queryset=Proveedor.objects.all())
    almacen_destino = Searchable.ChoiceSearchableField(queryset=Almacen.objects.filter(is_logical=False))
    fecha_compromiso = DatePicker.DateTimePickerField(required=False)
    # producto = SelectDependent.ChoiceSearchableDependentField(
    #                                 queryset=EstereotipoProducto.objects.all(),
    #                                 url='/api/compras/proveedor/__id__/productos/',
    #                                 target_id='id_proveedor',
    #                                 kv={'key':'id', 'value':'nombre'}
    #     )

    class Meta:
        model = PedidoCompra
        exclude = ('fecha_creacion', 'fecha_modificacion', 'fecha_recibido', 'status')

    def __init__(self, *args, **kwargs):
        super(PedidoCompraForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(PedidoCompraForm, self).clean()
        return cleaned_data


class ActivacionForm(forms.ModelForm):
    # TODO: Define other fields here

    class Meta:
        model = PedidoCompra
        fields = ['almacen_destino', 'fecha_compromiso']

    def __init__(self, *args, **kwargs):
        super(ActivacionForm, self).__init__(*args, **kwargs)
        self.fields['almacen_destino'].queryset = Almacen.objects.filter(is_logical=False)

    def clean(self):
        cleaned_data = super(ActivacionForm, self).clean()
        return cleaned_data
