from django.forms import ModelForm

from core.forms.custom import Searchable
from division_territorial.forms import CiudadField, EstadoField
from core.models import MarcaProducto, CategoriaProducto
from ..models import Proveedor


class ProveedorForm(ModelForm):
    estado = EstadoField()
    ciudad = CiudadField()

    class Meta:
        model = Proveedor
        exclude = ('is_me', 'is_cliente', 'is_proveedor')

    def __init__(self, *args, **kwargs):
        super(ProveedorForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(ProveedorForm, self).clean()
        return cleaned_data
