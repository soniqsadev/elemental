from django import forms
from django.db.models import Q

from core.forms.custom import Searchable
from ..models import Medico, EspecialidadMedica


class EspecialidadMedicaForm(forms.ModelForm):
    parent = Searchable.ChoiceSearchableField(
                            queryset=EspecialidadMedica.objects.all()
                            )

    class Meta:
        model = EspecialidadMedica
        fields = ['descripcion', 'parent']

    def __init__(self, *args, **kwargs):
        super(EspecialidadMedicaForm, self).__init__(*args, **kwargs)
        instancia = kwargs.get('instance', None)
        if instancia is not None:
            self.fields['parent'].queryset = EspecialidadMedica.objects.exclude(
                                                    pk=instancia.id
                                                )

    def clean(self):
        cleaned_data = super(EspecialidadMedicaForm, self).clean()
        return cleaned_data


class SubEspecialidadMedicaForm(forms.ModelForm):
    # TODO: Define other fields here

    class Meta:
        model = EspecialidadMedica
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(SubEspecialidadMedicaForm, self).__init__(*args, **kwargs)
        instancia = kwargs.get('instance', None)
        if instancia is not None:
            # self.fields['parent'].queryset = EspecialidadMedica.objects.filter( ~Q(id=instancia.id) )
            # self.fields['parent'].queryset = EspecialidadMedica.objects.exclude(id__in = [instancia.id,] + [a.id for a in instancia.hijos.all()])
            self.fields['parent'].queryset = EspecialidadMedica.objects.filter(
                                                parent__isnull=True).exclude(
                                                    pk=instancia.id
                                                )

    def clean(self):
        cleaned_data = super(SubEspecialidadMedicaForm, self).clean()
        return cleaned_data
