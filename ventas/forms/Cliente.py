from django.forms import ModelForm

from division_territorial.models import Ciudad
from core.forms.custom.SelectDependent import ChoiceDependentField
from division_territorial.forms import CiudadField, EstadoField
from ..models import Cliente


class ClienteForm(ModelForm):
    ciudad = CiudadField()
    estado = EstadoField()

    class Meta:
        model = Cliente
        exclude = ('is_me', 'is_cliente', 'is_proveedor', 'datos_entrega')

    def __init__(self, *args, **kwargs):
        super(ClienteForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(ClienteForm, self).clean()
        return cleaned_data


class SimpleClienteForm(ModelForm):

    class Meta:
        model = Cliente
        exclude = ('is_me', 'is_cliente', 'is_proveedor', 'datos_entrega')
