from django.forms import ModelForm
from division_territorial.forms import CiudadField, EstadoField
from ..models import Paciente

class PacienteForm(ModelForm):
    ciudad = CiudadField()
    estado = EstadoField()

    class Meta:
        model = Paciente
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(PacienteForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(PacienteForm, self).clean()
        return cleaned_data
