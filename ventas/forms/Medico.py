from django.forms import ModelForm, ModelChoiceField
from core.forms.custom import SelectDependent, Searchable
from division_territorial.forms import CiudadField, EstadoField
from ..models import Medico, EspecialidadMedica, Hospital


class MedicoForm(ModelForm):
    ciudad = CiudadField()
    estado = EstadoField()
    hospital = Searchable.ChoiceSearchableField(queryset=Hospital.objects.all())
    especialidad = Searchable.ChoiceSearchableField(queryset=EspecialidadMedica.objects.filter(parent__isnull=True))
    subespecialidad = SelectDependent.ChoiceSearchableDependentField(
                                        queryset=EspecialidadMedica.objects.filter(parent__isnull=False),
                                        url='/api/ventas/medico/especialidad/__id__/subespecialidades',
                                        target_id='id_especialidad',
                                        kv={'key':'id', 'value':'descripcion'},
                                        required=False
                                    )

    class Meta:
        model = Medico
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(MedicoForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(MedicoForm, self).clean()
        return cleaned_data
