from django.forms import ModelForm, ModelChoiceField, CharField, RadioSelect, Form
from .. import models
from . import Hospital, Medico, Paciente
from core.forms.custom import AddMore, DatePicker, SelectDependent, Searchable
from core.models import Moral, Contacto, Empresa
from core.forms.Moral import MoralSimpleForm
from ..forms import Cliente as ClienteForm
from almacen.models import Almacen


class OrdenVentaForm(ModelForm):
    # cliente = AddMore.ChoiceAddMoreField(
    #                             ruta='api.cliente.list',
    #                             # form=MoralSimpleForm(prefix='modal'),
    #                             form=ClienteForm.ClienteForm(prefix='modal'),
    #                             kv={'key': 'id', 'value': 'nombre'},
    #                             queryset=Moral.objects.filter(is_cliente=True),
    #                     )
    cliente = Searchable.ChoiceSearchableField(
                            queryset=models.Cliente.objects.all())
    contacto_cliente = SelectDependent.ChoiceSearchableDependentField(
                                queryset=Contacto.objects.all(),
                                url='/api/ventas/cliente/__id__/contactos',
                                target_id='id_cliente',
                                kv={
                                    'key': 'id',
                                    'value': 'cargo,nombres,apellidos'},
                                label='Persona de contacto con cliente',
                                required=False
                            )
    # hospital = AddMore.ChoiceAddMoreField(
    #                                 ruta='api.hospital.list',
    #                                 form=Hospital.HospitalForm(prefix='modal'),
    #                                 kv={'key':'id', 'value':'nombre'},
    #                                 queryset=models.Hospital.objects.all(),
    #                             )
    hospital = Searchable.ChoiceSearchableField(queryset=models.Hospital.objects.all())
    contacto_hospital =  SelectDependent.ChoiceSearchableDependentField(
                                queryset=Contacto.objects.all(),
                                url='/api/ventas/hospital/__id__/contactos',
                                target_id='id_hospital',
                                kv={'key':'id', 'value':'cargo,nombres,apellidos'},
                                label='Persona de contacto con hospital',
                                required=False
                            )
    # medico = AddMore.ChoiceAddMoreField(
    #                                 ruta='api.medico.list',
    #                                 form=Medico.MedicoForm(prefix='modal'),
    #                                 kv={'key':'id', 'value':'nombres,apellidos'},
    #                                 queryset=models.Medico.objects.all(),
    #                             )
    # paciente = AddMore.ChoiceAddMoreField(
    #                                 ruta='api.paciente.list',
    #                                 form=Paciente.PacienteForm(prefix='modal'),
    #                                 kv={'key':'id', 'value':'nombres,apellidos'},
    #                                 queryset=models.Paciente.objects.all(),
    #                             )
    medico = Searchable.ChoiceSearchableField(queryset=models.Medico.objects.all())
    paciente = Searchable.ChoiceSearchableField(queryset=models.Paciente.objects.all())
    empresa = ModelChoiceField(queryset=Empresa.objects.all())
    fecha_cirugia = DatePicker.DateTimePickerField(required=False)

    class Meta:
        model = models.OrdenVenta
        exclude = ('status','razon_rechazo', 'datos_entrega', 'codigo')

    def __init__(self, *args, **kwargs):

        super(OrdenVentaForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(OrdenVentaForm, self).clean()
        return cleaned_data


class ConfirmacionRechazoForm(ModelForm):

    class Meta:
        model = models.OrdenVenta
        fields = ['razon_rechazo', 'comentario_rechazo']

    def __init__(self, *args, **kwargs):
        super(ConfirmacionRechazoForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(ConfirmacionRechazoForm, self).clean()
        return cleaned_data


class AdicionalAdjudicacionForm(Form):
    almacen_origen = ModelChoiceField(label='Almacén de origen', queryset=Almacen.objects.filter(is_logical=False))
    fecha_compromiso = DatePicker.DateTimePickerField(label='Fecha de entrega')

    def __init__(self, *args, **kwargs):
        super(AdicionalAdjudicacionForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(AdicionalAdjudicacionForm, self).clean()
        return cleaned_data

    def save(self):
        cleaned_data = super(AdicionalAdjudicacionForm, self).clean()
