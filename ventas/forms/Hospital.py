from django.forms import ModelForm

from core.forms.custom import Searchable
from division_territorial.forms import CiudadField, EstadoField
from ..models import Hospital, Cliente

class HospitalForm(ModelForm):
    ciudad = CiudadField()
    estado = EstadoField()
    cliente_asociado = Searchable.ChoiceSearchableField(queryset=Cliente.objects.all())

    class Meta:
        model = Hospital
        exclude = ('is_me', 'is_cliente', 'is_proveedor')

    def __init__(self, *args, **kwargs):
        super(HospitalForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(HospitalForm, self).clean()
        return cleaned_data
