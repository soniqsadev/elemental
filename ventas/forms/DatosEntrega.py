from django.forms import ModelForm
from ..models import DatosEntrega

class DatosEntregaForm(ModelForm):
    # TODO: Define other fields here

    class Meta:
        model = DatosEntrega
        exclude = ['moral',]

    def __init__(self, *args, **kwargs):
        super(DatosEntregaForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(DatosEntregaForm, self).clean()
        return cleaned_data
