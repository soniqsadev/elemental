from django.conf.urls import url

from .views import default, Hospital, Medico, Paciente, Cliente, OrdenVenta, EspecialidadMedica


urlpatterns = [

    url(regex=r'^$', view=default.IndexTemplateView.as_view(), name='ventas.index'),

    url(regex=r'^orden/$', view=OrdenVenta.ListView.as_view(), name='ventas.orden.list'),
    url(regex=r'^orden/new/$', view=OrdenVenta.NewView.as_view(), name='ventas.orden.new'),
    url(regex=r'^orden/(?P<pk>[0-9]+)/$', view=OrdenVenta.DetailView.as_view(), name='ventas.orden.detail'),
    url(regex=r'^orden/(?P<pk>[0-9]+)/html/$', view=OrdenVenta.generate_html, name='ventas.orden.detail.html'),
    url(regex=r'^orden/(?P<pk>[0-9]+)/pdf/$', view=OrdenVenta.generate_pdf, name='ventas.orden.detail.pdf'),
    url(regex=r'^orden/(?P<pk>[0-9]+)/adjudicar/$', view=OrdenVenta.AdjudicarLicitacionView.as_view(), name='ventas.orden.adjudicar'),
    url(regex=r'^orden/(?P<pk>[0-9]+)/rechazar/$', view=OrdenVenta.RechazarLicitacionView.as_view(), name='ventas.orden.rechazar'),
    url(regex=r'^orden/(?P<pk>[0-9]+)/modify/$', view=OrdenVenta.ModifyView.as_view(), name='ventas.orden.modify'),
    url(regex=r'^orden/(?P<pk>[0-9]+)/delete/$', view=OrdenVenta.DeleteView.as_view(), name='ventas.orden.delete'),

    url(regex=r'^cliente/$', view=Cliente.ListView.as_view(), name='cliente.list'),
    url(regex=r'^cliente/create/$', view=Cliente.CreateView.as_view(), name='cliente.create'),
    url(regex=r'^cliente/(?P<pk>[0-9]+)/$', view=Cliente.DetailView.as_view(), name='cliente.detail'),
    url(regex=r'^cliente/(?P<pk>[0-9]+)/edit/$', view=Cliente.UpdateView.as_view(), name='cliente.update'),
    url(regex=r'^cliente/(?P<pk>[0-9]+)/delete/$', view=Cliente.DeleteView.as_view(), name='cliente.delete'),

    url(regex=r'^hospital/$', view=Hospital.ListView.as_view(), name='hospital.list'),
    url(regex=r'^hospital/create/$', view=Hospital.CreateView.as_view(), name='hospital.create'),
    url(regex=r'^hospital/(?P<pk>[0-9]+)/$', view=Hospital.DetailView.as_view(), name='hospital.detail'),
    url(regex=r'^hospital/(?P<pk>[0-9]+)/edit/$', view=Hospital.UpdateView.as_view(), name='hospital.update'),
    url(regex=r'^hospital/(?P<pk>[0-9]+)/delete/$', view=Hospital.DeleteView.as_view(), name='hospital.delete'),

    url(regex=r'^medico/$', view=Medico.ListView.as_view(), name='medico.list'),
    url(regex=r'^medico/create/$', view=Medico.CreateView.as_view(), name='medico.create'),
    url(regex=r'^medico/(?P<pk>[0-9]+)/$', view=Medico.DetailView.as_view(), name='medico.detail'),
    url(regex=r'^medico/(?P<pk>[0-9]+)/edit/$', view=Medico.UpdateView.as_view(), name='medico.update'),
    url(regex=r'^medico/(?P<pk>[0-9]+)/delete/$', view=Medico.DeleteView.as_view(), name='medico.delete'),

    url(regex=r'^medico/especialidad/$', view=EspecialidadMedica.ListView.as_view(), name='medico.especialidad.list'),
    url(regex=r'^medico/especialidad/create/$', view=EspecialidadMedica.CreateView.as_view(), name='medico.especialidad.create'),
    url(regex=r'^medico/especialidad/(?P<pk>[0-9]+)/$', view=EspecialidadMedica.DetailView.as_view(), name='medico.especialidad.detail'),
    url(regex=r'^medico/especialidad/(?P<pk>[0-9]+)/edit/$', view=EspecialidadMedica.UpdateView.as_view(), name='medico.especialidad.update'),
    url(regex=r'^medico/especialidad/(?P<pk>[0-9]+)/delete/$', view=EspecialidadMedica.DeleteView.as_view(), name='medico.especialidad.delete'),

    url(regex=r'^paciente/$', view=Paciente.ListView.as_view(), name='paciente.list'),
    url(regex=r'^paciente/create/$', view=Paciente.CreateView.as_view(), name='paciente.create'),
    url(regex=r'^paciente/(?P<pk>[0-9]+)/$', view=Paciente.DetailView.as_view(), name='paciente.detail'),
    url(regex=r'^paciente/(?P<pk>[0-9]+)/edit/$', view=Paciente.UpdateView.as_view(), name='paciente.update'),
    url(regex=r'^paciente/(?P<pk>[0-9]+)/delete/$', view=Paciente.DeleteView.as_view(), name='paciente.delete'),

]
