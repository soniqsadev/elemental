from django.apps import AppConfig


class VentasConfig(AppConfig):
    name = 'ventas'
    verbose_name = 'Ventas'

    def ready(self):
        from ventas.signals.Receivers import ordenventa_pre_save, metainformacion_pre_save
