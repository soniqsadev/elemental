import os

from django.conf import settings
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from django.views.generic import View
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.forms import modelformset_factory

from core.utils.pdf import RouteToPDF

from ..models import OrdenVenta, ProductoVenta
from ..forms.OrdenVenta import OrdenVentaForm, ConfirmacionRechazoForm, AdicionalAdjudicacionForm
from ..forms.DatosEntrega import DatosEntregaForm

from compras.models.RequisicionCompra import RequisicionCompra

SUCCESS_URL = reverse_lazy('ventas.orden.list')


def permissions_show(user):
    return user.has_perm('ventas.can_add_ordenventa') or user.has_perm('ventas.can_change_ordenventa') or user.has_perm('ventas.can_delete_ordenventa')


class ListView(ListView):
    context_object_name = 'ordenes'
    model = OrdenVenta
    template_name = 'ventas/orden/list.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator(user_passes_test(permissions_show))
    def dispatch(self, *args, **kwargs):
        return super(ListView, self).dispatch(*args, **kwargs)


class NewView(View):
    template_name = 'ventas/orden/new.html'

    def __init__(self, *args, **kwargs):
        super(NewView, self).__init__(*args, **kwargs)

    def get(self, request):
        form = OrdenVentaForm()
        form.datos_entrega_form = DatosEntregaForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = OrdenVentaForm(request.POST)
        datos_entrega = DatosEntregaForm(request.POST)
        if form.is_valid() and datos_entrega.is_valid():
            datos_entrega.save()
            form.instance.datos_entrega = datos_entrega.instance
            form.save()
            return redirect(reverse_lazy('ventas.orden.modify', args=[form.instance.id]))

        form.datos_entrega_form = datos_entrega
        return render(request, self.template_name, {'form': form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('ventas.can_add_ordenventa'))
    def dispatch(self, *args, **kwargs):
        return super(NewView, self).dispatch(*args, **kwargs)


class DetailView(DetailView):
    model = OrdenVenta
    context_object_name = 'orden'
    template_name = 'ventas/orden/show.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator(user_passes_test(permissions_show))
    def dispatch(self, *args, **kwargs):
        return super(DetailView, self).dispatch(*args, **kwargs)


###############################################################################
#                           Generar vista HTML
###############################################################################
def generate_html(request, *args, **kwargs):
    orden_venta = get_object_or_404(OrdenVenta, pk=kwargs['pk'])
    subtemplate = os.path.join(settings.BASE_TEMPLATES, "uploads", str(orden_venta.empresa.template_cotizacion))
    css_path = orden_venta.empresa.style_cotizacion
    return render(request, 'ventas/orden/show_pdf.html', {'orden': orden_venta, 'subtemplate': subtemplate, 'css_path':css_path})


###############################################################################
#              Generar documento PDF a partir de vista HTML
###############################################################################
@require_GET
@login_required
@user_passes_test(permissions_show)
def generate_pdf(request, pk):
    orden_venta = get_object_or_404(OrdenVenta, pk=pk)
    if orden_venta.status == 'porenviar':
        orden_venta.status = 'enlicitacion'
        orden_venta.save()
    return RouteToPDF('ventas.orden.detail.html',
                      args=[pk, ],
                      orientation='Portrait').generate(orden_venta.codigo)


class AdjudicarLicitacionView(View):
    """
    Procesar adjudicación de la cotización
    """
    template_name = 'ventas/orden/confirmacion_adjudicacion.html'

    def create_formset(self, cantidad):
        return modelformset_factory(ProductoVenta,
                                    can_delete=False,
                                    max_num=cantidad,
                                    min_num=cantidad,
                                    validate_max=True,
                                    fields=('incluido',))

    def get(self, *args, **kwargs):
        orden_venta = get_object_or_404(OrdenVenta, pk=kwargs['pk'])
        form = AdicionalAdjudicacionForm(
                        initial={'fecha_compromiso': orden_venta.fecha_cirugia}
                    )
        productos_form = self.create_formset(
                                orden_venta.productos_ventas.all().count())(
                                    queryset=orden_venta.productos_ventas.all()
                                )
        return render(self.request,
                      self.template_name,
                      {
                        'form': form,
                        'productos_form': productos_form,
                        'orden_venta': orden_venta
                      })

    def post(self, *args, **kwargs):
        orden_venta = get_object_or_404(OrdenVenta, pk=kwargs['pk'])
        form = AdicionalAdjudicacionForm(self.request.POST)
        productos_form = self.create_formset(
                            orden_venta.productos_ventas.all().count()
                            )(
                                self.request.POST
                            )
        if productos_form.is_valid() and form.is_valid():
            productos_form.save()
            orden_venta.adjudicar(form.cleaned_data['almacen_origen'],
                                  form.cleaned_data['fecha_compromiso'])
            return redirect(reverse_lazy('ventas.orden.detail',
                            args=[orden_venta.id, ])
                            )

        print(orden_venta.productos_ventas.all().count())
        print(form.errors)
        print(productos_form.errors)
        return render(self.request,
                      self.template_name,
                      {
                        'form': form,
                        'productos_form': productos_form,
                        'orden_venta': orden_venta
                      })

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('ventas.can_change_ordenventa'))
    def dispatch(self, *args, **kwargs):
        return super(AdjudicarLicitacionView, self).dispatch(*args, **kwargs)


class RechazarLicitacionView(View):
    """
    Maneja el escesario cuando una cotización es rechazada
    """
    def __init__(self, *args, **kwargs):
        super(RechazarLicitacionView, self).__init__(*args, **kwargs)

    def get(self, *args, **kwargs):
        orden_venta = get_object_or_404(OrdenVenta, pk=kwargs['pk'])
        form = ConfirmacionRechazoForm(instance=orden_venta)
        return render(self.request, 'ventas/orden/confirmacion_rechazo.html', {'form':form})

    def post(self, *args, **kwargs):
        orden_venta = get_object_or_404(OrdenVenta, pk=kwargs['pk'])
        form = ConfirmacionRechazoForm(self.request.POST, instance=orden_venta)
        if form.is_valid():
            if form.instance.status == 'enlicitacion':
                form.instance.status = 'rechazada'
            form.save()
            return redirect( reverse_lazy('ventas.orden.detail', args=[form.instance.id]) )

        return render(self.request, 'ventas/orden/confirmacion_rechazo.html', {'form':form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('ventas.can_change_ordenventa'))
    def dispatch(self, *args, **kwargs):
        return super(RechazarLicitacionView, self).dispatch(*args, **kwargs)


class ModifyView(View):
    template_name = 'ventas/orden/modify.html'

    def __init__(self, *args, **kwargs):
        super(ModifyView, self).__init__(*args, **kwargs)
        self.ProductoVentaFormSet = modelformset_factory(ProductoVenta,
                                                         can_delete=True,
                                                         max_num=20,
                                                         validate_max=True,
                                                         fields=(
                                                            'alias',
                                                            'partida',
                                                            'estereotipo',
                                                            'cantidad',
                                                            'precio_unidad')
                                                         )

    def get(self, request, pk):
        orden_venta = get_object_or_404(OrdenVenta, pk=pk)
        form = OrdenVentaForm(instance=orden_venta)
        form.datos_entrega_form = DatosEntregaForm(
                                        instance=orden_venta.datos_entrega)
        form.form_productos = self.ProductoVentaFormSet(
                                queryset=orden_venta.productos_ventas.all())
        return render(request, self.template_name, {'form': form})

    def post(self, request, pk):
        redireccion = request.POST.get('redireccion', 'detail')
        orden_venta = get_object_or_404(OrdenVenta, pk=pk)
        form = OrdenVentaForm(request.POST, instance=orden_venta)
        datos_entrega = DatosEntregaForm(request.POST,
                                         instance=orden_venta.datos_entrega)
        productos = self.ProductoVentaFormSet(request.POST)
        for producto in productos:
            producto.instance.orden_venta = form.instance

        if form.is_valid() and datos_entrega.is_valid() and productos.is_valid():
            form.save()
            datos_entrega.save()
            productos.save()

            for producto in productos:
                # Comprobar si hay algun producto sin proveedor
                if hasattr(producto.instance, 'estereotipo'):
                    if hasattr(producto.instance.estereotipo, 'requisicion_compra'):
                        postulaciones_aprobadas = producto.instance.estereotipo.requisicion_compra.postulaciones.filter(aprobada=True)
                        print('len(postulaciones_aprobadas)', len(postulaciones_aprobadas))
                        if len(postulaciones_aprobadas) == 0:
                            form.instance.status = 'porprocurar'
                            requisicion, created = RequisicionCompra.objects.get_or_create(
                                                            estereotipo=producto.instance.estereotipo,
                                                            defaults={
                                                                'orden_venta': form.instance,
                                                                'cantidad': producto.instance.cantidad,
                                                            }
                                                        )
                            if not created:
                                requisicion.cantidad = producto.instance.cantidad
                                requisicion.save()
                    else:
                        RequisicionCompra.objects.create(estereotipo=producto.instance.estereotipo, orden_venta=form.instance, cantidad=producto.instance.cantidad)

            if redireccion == 'detail':
                return redirect(reverse_lazy('ventas.orden.detail', args=[form.instance.id]))
            else:
                return redirect(reverse_lazy('ventas.orden.modify', args=[form.instance.id]))

        form.form_productos = productos
        form.datos_entrega_form = datos_entrega
        return render(request, self.template_name, {'form': form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('ventas.can_change_ordenventa'))
    def dispatch(self, *args, **kwargs):
        return super(ModifyView, self).dispatch(*args, **kwargs)


class DeleteView(DeleteView):
    model = OrdenVenta
    success_url = SUCCESS_URL
    context_object_name = 'orden'
    template_name = 'ventas/orden/delete.html'

    @method_decorator(login_required)
    @method_decorator(permission_required('ventas.can_delete_ordenventa'))
    def dispatch(self, *args, **kwargs):
        return super(DeleteView, self).dispatch(*args, **kwargs)
