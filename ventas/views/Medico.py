from django.core.urlresolvers import reverse_lazy

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView

from ..models import Medico
from ..forms.Medico import MedicoForm


SUCCESS_URL = reverse_lazy('medico.list')


def permissions_show(user):
    return user.has_perm('ventas.can_add_medico') or user.has_perm('ventas.can_change_medico') or user.has_perm('ventas.can_delete_medico')


class ListView(ListView):
    context_object_name = 'medicos'
    model = Medico
    page_kwarg = 'page'
    paginate_by = 10
    template_name = 'ventas/medico/list.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator(user_passes_test(permissions_show))
    def dispatch(self, *args, **kwargs):
        return super(ListView, self).dispatch(*args, **kwargs)


class CreateView(CreateView):
    model = Medico
    form_class = MedicoForm
    success_url = SUCCESS_URL
    template_name = 'ventas/medico/create.html'

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('ventas.can_add_medico'))
    def dispatch(self, *args, **kwargs):
        return super(CreateView, self).dispatch(*args, **kwargs)


class DetailView(DetailView):
    model = Medico
    context_object_name = 'medico'
    template_name = 'ventas/medico/show.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator(user_passes_test(permissions_show))
    def dispatch(self, *args, **kwargs):
        return super(DetailView, self).dispatch(*args, **kwargs)


class UpdateView(UpdateView):
    model = Medico
    form_class = MedicoForm
    context_object_name = 'medico'
    success_url = SUCCESS_URL
    template_name = 'ventas/medico/create.html'

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('ventas.can_change_medico'))
    def dispatch(self, *args, **kwargs):
        return super(UpdateView, self).dispatch(*args, **kwargs)


class DeleteView(DeleteView):
    model = Medico
    success_url = SUCCESS_URL
    template_name = 'ventas/medico/delete.html'

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('ventas.can_delete_medico'))
    def dispatch(self, *args, **kwargs):
        return super(DeleteView, self).dispatch(*args, **kwargs)
