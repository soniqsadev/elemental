from django.core.urlresolvers import reverse_lazy

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView

from ..models import Paciente
from ..forms.Paciente import PacienteForm

SUCCESS_URL = reverse_lazy('paciente.list')

def permissions_show(user):
    return user.has_perm('ventas.can_add_paciente') or user.has_perm('ventas.can_change_paciente') or user.has_perm('ventas.can_delete_paciente')

class ListView(ListView):
    context_object_name = 'pacientes'
    model = Paciente
    page_kwarg = 'page'
    paginate_by = 10
    template_name = 'ventas/paciente/list.html'

    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(ListView, self).dispatch(*args, **kwargs)

class CreateView(CreateView):
    model = Paciente
    form_class = PacienteForm
    success_url = SUCCESS_URL
    template_name = 'ventas/paciente/create.html'

    @method_decorator(login_required)
    @method_decorator(permission_required('ventas.can_add_paciente'))
    def dispatch(self, *args, **kwargs):
        return super(CreateView, self).dispatch(*args, **kwargs)

class DetailView(DetailView):
    model = Paciente
    context_object_name = 'paciente'
    template_name = 'ventas/paciente/show.html'

    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(DetailView, self).dispatch(*args, **kwargs)

class UpdateView(UpdateView):
    model = Paciente
    form_class = PacienteForm
    context_object_name = 'paciente'
    success_url = SUCCESS_URL
    template_name = 'ventas/paciente/create.html'

    @method_decorator(login_required)
    @method_decorator(permission_required('ventas.can_change_paciente'))
    def dispatch(self, *args, **kwargs):
        return super(UpdateView, self).dispatch(*args, **kwargs)

class DeleteView(DeleteView):
    model = Paciente
    success_url = SUCCESS_URL
    template_name = 'ventas/paciente/delete.html'

    @method_decorator(login_required)
    @method_decorator(permission_required('ventas.can_delete_paciente'))
    def dispatch(self, *args, **kwargs):
        return super(DeleteView, self).dispatch(*args, **kwargs)
