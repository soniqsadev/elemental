from django.shortcuts import render

from django.views.generic.base import TemplateView


class IndexTemplateView(TemplateView):
    template_name = 'body.html'

class ConfigTemplateView(TemplateView):
    template_name = 'core/configuracion.html'
