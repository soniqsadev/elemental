from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse_lazy
from django.forms import modelformset_factory

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from django.views.generic import View
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView

from core.models import Telefono

from ..models import Hospital
from ..forms.Hospital import HospitalForm


SUCCESS_URL = reverse_lazy('hospital.list')

def permissions_show(user):
    return user.has_perm('ventas.can_add_hospital') or user.has_perm('ventas.can_change_hospital') or user.has_perm('ventas.can_delete_hospital')

class ListView(ListView):
    context_object_name = 'hospitales'
    model = Hospital
    page_kwarg = 'page'
    paginate_by = 10
    template_name = 'ventas/hospital/list.html'

    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(ListView, self).dispatch(*args, **kwargs)

class CreateView(View):
    model = Hospital
    form_class = HospitalForm
    success_url = SUCCESS_URL
    template_name = 'ventas/hospital/create.html'

    def __init__(self, *args, **kwargs):
        super(CreateView, self).__init__(*args, **kwargs)
        self.TelefonoFormSet = modelformset_factory(Telefono,
                                                         can_delete=True,
                                                         max_num=5,
                                                         min_num=0,
                                                         validate_max=True,
                                                         fields= ('id', 'tipo', 'numero')
                                                    )

    def get(self, *args, **kwargs):
        form = HospitalForm()
        form.telefono_form = self.TelefonoFormSet(queryset=Telefono.objects.none())
        return render(self.request, self.template_name, {'form':form})


    def post(self, *args, **kwargs):
        form = HospitalForm(self.request.POST, self.request.FILES)
        telefonos = self.TelefonoFormSet(self.request.POST)
        if form.is_valid() and telefonos.is_valid():
            form.save()
            for telefono in telefonos:
                telefono.instance.persona = form.instance
            telefonos.save()
            return redirect( self.success_url )
        form.telefono_form = telefonos
        return render(self.request, self.template_name, {'form': form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('ventas.can_add_hospital'))
    def dispatch(self, *args, **kwargs):
        return super(CreateView, self).dispatch(*args, **kwargs)


class DetailView(DetailView):
    model = Hospital
    context_object_name = 'hospital'
    template_name = 'ventas/hospital/show.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(DetailView, self).dispatch(*args, **kwargs)

class UpdateView(View):
    model = Hospital
    form_class = HospitalForm
    context_object_name = 'hospital'
    success_url = SUCCESS_URL
    template_name = 'ventas/hospital/create.html'

    def __init__(self, *args, **kwargs):
        super(UpdateView, self).__init__(*args, **kwargs)
        self.TelefonoFormSet = modelformset_factory(Telefono,
                                                    can_delete=True,
                                                    max_num=5,
                                                    min_num=0,
                                                    validate_max=True,
                                                    fields= ('id', 'tipo', 'numero')
                                                    )

    def get(self, *args, **kwargs):
        hospital = get_object_or_404(Hospital, pk=kwargs['pk'])
        form = HospitalForm(instance=hospital)
        form.telefono_form = self.TelefonoFormSet(queryset=hospital.telefonos.all())
        return render(self.request, self.template_name, {'form':form})

    def post(self, *args, **kwargs):
        hospital = get_object_or_404(Hospital, pk=kwargs['pk'])
        form = HospitalForm(self.request.POST, self.request.FILES, instance=hospital)
        telefonos = self.TelefonoFormSet(self.request.POST)
        for telefono in telefonos:
            telefono.instance.persona = hospital
        if form.is_valid() and telefonos.is_valid():
            form.save()
            telefonos.save()
            return redirect(self.success_url)

        form.telefono_form = telefonos
        return render(self.request, self.template_name, {'form': form})

        # Permisos
        @method_decorator(login_required)
        @method_decorator(permission_required('ventas.can_change_hospital'))
        def dispatch(self, *args, **kwargs):
            return super(UpdateView, self).dispatch(*args, **kwargs)


class DeleteView(DeleteView):
    model = Hospital
    success_url = SUCCESS_URL
    template_name = 'ventas/hospital/delete.html'

    @method_decorator(login_required)
    @method_decorator(permission_required('ventas.can_delete_hospital'))
    def dispatch(self, *args, **kwargs):
        return super(DeleteView, self).dispatch(*args, **kwargs)
