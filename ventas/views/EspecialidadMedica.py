from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from django.forms import modelformset_factory
from django.views.generic import View
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView

from ..models import EspecialidadMedica
from ..forms.EspecialidadMedica import EspecialidadMedicaForm


SUCCESS_URL = reverse_lazy('medico.especialidad.list')


def permissions_show(user):
    return user.has_perm('ventas.can_add_especialidadmedica') or user.has_perm('ventas.can_change_especialidadmedica') or user.has_perm('ventas.can_delete_especialidadmedica')


class ListView(ListView):
    context_object_name = 'especialidades'
    model = EspecialidadMedica
    page_kwarg = 'page'
    paginate_by = 10
    template_name = 'ventas/medico/especialidad/list.html'

    def get_queryset(self):
        return EspecialidadMedica.objects.filter(parent__isnull=True)

    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(ListView, self).dispatch(*args, **kwargs)


class CreateView(View):
    success_url = SUCCESS_URL
    template_name = 'ventas/medico/especialidad/create.html'

    def get(self, *args, **kwargs):
        parent_id = self.request.GET.get('parent', None)
        initial = {}
        if parent_id is not None:
            parent = get_object_or_404(EspecialidadMedica, pk=parent_id)
            initial = {'parent': parent}
        form = EspecialidadMedicaForm(initial=initial)
        return render(self.request, self.template_name, {'form': form})

    def post(self, *args, **kwargs):
        form = EspecialidadMedicaForm(self.request.POST, self.request.FILES)
        if form.is_valid():
            form.save()
            return redirect(self.success_url)
        return render(self.request, self.template_name, {'form': form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('ventas.can_add_especialidadmedica'))
    def dispatch(self, *args, **kwargs):
        return super(CreateView, self).dispatch(*args, **kwargs)


class DetailView(DetailView):
    model = EspecialidadMedica
    context_object_name = 'especialidad'
    template_name = 'ventas/medico/especialidad/show.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator(user_passes_test(permissions_show))
    def dispatch(self, *args, **kwargs):
        return super(DetailView, self).dispatch(*args, **kwargs)


class UpdateView(View):
    model = EspecialidadMedica
    form_class = EspecialidadMedicaForm
    context_object_name = 'especialidad'
    success_url = SUCCESS_URL
    template_name = 'ventas/medico/especialidad/create.html'

    def __init__(self, *args, **kwargs):
        super(UpdateView, self).__init__(*args, **kwargs)
        self.EspecialidadMedicaFormSet = modelformset_factory(
                                                    EspecialidadMedica,
                                                    can_delete=True,
                                                    max_num=10,
                                                    min_num=0,
                                                    validate_max=True,
                                                    fields=(
                                                        'id',
                                                        'descripcion')
                                                )

    def get(self, *args, **kwargs):
        especialidad = get_object_or_404(EspecialidadMedica, pk=kwargs['pk'])
        form = EspecialidadMedicaForm(instance=especialidad)
        return render(self.request, self.template_name, {'form': form})

    def post(self, *args, **kwargs):
        especialidad = get_object_or_404(EspecialidadMedica, pk=kwargs['pk'])
        form = EspecialidadMedicaForm(self.request.POST,
                                      self.request.FILES,
                                      instance=especialidad)
        if form.is_valid():
            form.save()
            return redirect(self.success_url)

        return render(self.request, self.template_name, {'form': form})

        # Permisos
        @method_decorator(login_required)
        @method_decorator(permission_required('ventas.can_change_especialidadmedica'))
        def dispatch(self, *args, **kwargs):
            return super(UpdateView, self).dispatch(*args, **kwargs)


class DeleteView(DeleteView):
    model = EspecialidadMedica
    context_object_name = 'especialidad'
    success_url = SUCCESS_URL
    template_name = 'ventas/medico/especialidad/delete.html'

    @method_decorator(login_required)
    @method_decorator(permission_required('ventas.can_delete_especialidadmedica'))
    def dispatch(self, *args, **kwargs):
        return super(DeleteView, self).dispatch(*args, **kwargs)
