from rest_framework import serializers

from ..models import Hospital, Medico, EspecialidadMedica, Paciente, DatosEntrega, ProductoVenta
from core.api.serializers import EstereotipoProductoSerializer


class HospitalSerializer(serializers.ModelSerializer):
    """docstring for HospitalSerializer"""
    class Meta:
        model = Hospital
        fields = '__all__'


class MedicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medico
        fields = '__all__'


class EspecialidadMedicaSerializer(serializers.ModelSerializer):
    class Meta:
        model = EspecialidadMedica
        fields = ('id', 'descripcion', 'parent')


class PacienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Paciente
        fields = '__all__'


class DatosEntregaSerializer(serializers.ModelSerializer):
    class Meta:
        model = DatosEntrega
        fields = '__all__'


class ProductoVentaSerializer(serializers.ModelSerializer):
    estereotipo = EstereotipoProductoSerializer()

    class Meta:
        model = ProductoVenta
        fields = '__all__'
