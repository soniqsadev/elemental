from django.http import Http404

from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models import Contacto
from core.api.serializers import ContactoSerializer

from ..models import Medico, Hospital, EspecialidadMedica, Paciente, Cliente, ProductoVenta
from .serializers import HospitalSerializer, MedicoSerializer, EspecialidadMedicaSerializer, PacienteSerializer, DatosEntregaSerializer, ProductoVentaSerializer


# Create your views here.
class MedicoList(generics.ListCreateAPIView):
    """
    Lista de todos los medicos
    """
    queryset = Medico.objects.all()
    serializer_class = MedicoSerializer

class MedicoDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Ver, Editar y Eliminar un medico
    """
    queryset = Medico.objects.all()
    serializer_class = MedicoSerializer


class HospitalList(generics.ListCreateAPIView):
    """
    Lista de todos los hospitales
    """
    queryset = Hospital.objects.all()
    serializer_class = HospitalSerializer


class HospitalDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Ver, Editar y Eliminar un hospital
    """
    queryset = Hospital.objects.all()
    serializer_class = HospitalSerializer


class ListEspecialidadMedicaView(APIView):
    """
    Lista de especialidades de medicos
    """

    def get(self, request, format=None):
        especialidades = EspecialidadMedica.objects.filter(parent__isnull=True)
        serializer = EspecialidadMedicaSerializer(especialidades, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def list_subespecialidades_medicos_view(request, parent):
    subespecialidades = EspecialidadMedica.objects.filter(parent=parent)
    serializer = EspecialidadMedicaSerializer(subespecialidades, many=True)
    return Response(serializer.data)


class PacienteList(generics.ListCreateAPIView):
    """
    Lista de todos los pacientes
    """
    queryset = Paciente.objects.all()
    serializer_class = PacienteSerializer


class PacienteDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Ver, Editar y Eliminar un paciente
    """
    queryset = Paciente.objects.all()
    serializer_class = PacienteSerializer


@api_view(['GET'])
def get_productoventa_for_orden(request, *args, **kwargs):
    try:
        producto_venta = ProductoVenta.objects.filter(
                                orden_venta=kwargs['cotizacion_id']
                         )

        serializer = ProductoVentaSerializer(producto_venta,
                                             many=True)

        return Response(serializer.data)
    except ProductoVenta.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def list_contactos_cliente_view(request, cliente_id):
    contactos = Contacto.objects.filter(persona_moral=cliente_id)
    serializer = ContactoSerializer(contactos, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def list_contactos_hospital_view(request, hospital_id):
    contactos = Contacto.objects.filter(persona_moral=hospital_id)
    serializer = ContactoSerializer(contactos, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def datos_entrega_cliente_view(request, cliente_id):
    cliente = Cliente.objects.get(pk=cliente_id)
    serializer = DatosEntregaSerializer(cliente.datos_entrega, many=False)
    return Response(serializer.data)
