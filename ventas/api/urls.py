from django.conf.urls import url

from . import views

urlpatterns = [
    url(regex=r'^cliente/(?P<cliente_id>[0-9]+)/contactos/$',
        view=views.list_contactos_cliente_view,
        name='api.cliente.contactos'),

    url(regex=r'^cliente/(?P<cliente_id>[0-9]+)/datos-entrega/$',
        view=views.datos_entrega_cliente_view,
        name='api.cliente.datos_entrega'),

    url(regex=r'^medico/$',
        view=views.MedicoList.as_view(),
        name='api.medico.list'),

    url(regex=r'^medico/(?P<pk>[0-9]+)/$',
        view=views.MedicoDetail.as_view(),
        name='api.medico.detail'),

    url(regex=r'^hospital/$',
        view=views.HospitalList.as_view(),
        name='api.hospital.list'),

    url(regex=r'^hospital/(?P<pk>[0-9]+)/$',
        view=views.HospitalDetail.as_view(),
        name='api.hospital.detail'),

    url(regex=r'^hospital/(?P<hospital_id>[0-9]+)/contactos/$',
        view=views.list_contactos_hospital_view,
        name='api.hospital.contactos'),

    url(regex=r'^medico/especialidad/$',
        view=views.ListEspecialidadMedicaView.as_view(),
        name='api.especialidad.list'),

    url(regex=r'^medico/especialidad/(?P<parent>[0-9]+)/subespecialidades/$',
        view=views.list_subespecialidades_medicos_view,
        name='api.subespecialidad.list'),

    url(regex=r'^paciente/$',
        view=views.PacienteList.as_view(),
        name='api.paciente.list'),

    url(regex=r'^paciente/(?P<pk>[0-9]+)/$',
        view=views.PacienteDetail.as_view(),
        name='api.paciente.detail'),

    url(regex=r'^producto-venta/cotizacion/(?P<cotizacion_id>[0-9]+)/$',
        view=views.get_productoventa_for_orden,
        name='api.paciente.orden.detail'),
]
