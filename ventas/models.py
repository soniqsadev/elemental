from decimal import Decimal
from datetime import date, datetime

from django.db import models
from django.core.validators import MinValueValidator

from core.models import Natural, Moral, EstereotipoProducto, Producto, Contacto, MetaInformacion, Empresa
from almacen.models import ActualizacionVenta, Almacen, Estereotipo as EstereotipoAlmacen


class DatosEntrega(models.Model):
    plazo_condiciones = models.TextField(blank=True, null=True, verbose_name='Plazo y condiciones de entrega')
    forma_pago = models.TextField(blank=True, null=True, verbose_name='Forma de pago')
    garantia_tecnica = models.CharField(blank=True, max_length=100, verbose_name='Garantía técnica')
    norma_nacional = models.CharField(blank=True, null=True, max_length=100, verbose_name='Norma nacional')
    origen_bienes = models.CharField(blank=True, null=True, max_length=100, verbose_name='Origen de bienes')
    estratificacion = models.CharField(blank=True, null=True, max_length=100, verbose_name='Estratificación')
    vigencia_cotizacion = models.CharField(blank=True, max_length=100, verbose_name='Vigencia de la cotización')
    declaracion_buena_fe = models.TextField(blank=True, null=True, verbose_name='Declaración de buena fe')

    class Meta:
        verbose_name = 'Datos de entrega'
        verbose_name_plural = 'Datos de entregas'


class Cliente(Moral):
    datos_entrega = models.ForeignKey(DatosEntrega,
                                      related_name='clientes',
                                      null=True,
                                      on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'


class Hospital(Moral):
    cliente_asociado = models.ForeignKey(Cliente,
                                         null=True,
                                         blank=True,
                                         related_name='hospitales',
                                         on_delete=models.SET_NULL)
    abreviatura = models.CharField(max_length=64, null=True, blank=True)
    lugar_entrega = models.CharField(blank=True,
                                     null=True,
                                     max_length=100,
                                     verbose_name='Lugar de Entrega')

    class Meta:
        verbose_name = 'Hospital'
        verbose_name_plural = 'Hospitales'


#########################################################################
#                              Medicos
#########################################################################

class EspecialidadMedica(models.Model):
    descripcion = models.CharField(max_length=100, verbose_name='Descripción')
    parent = models.ForeignKey('EspecialidadMedica',
                               null=True,
                               blank=True,
                               related_name='subespecialidades',
                               on_delete=models.SET_NULL)

    class Meta:
        verbose_name = 'Especialidad Medica'
        verbose_name_plural = 'Especialidades Medicas'

    def __str__(self):
        return self.descripcion


class Medico(Natural):
    hospital = models.ForeignKey(Hospital)
    especialidad = models.ForeignKey(EspecialidadMedica, related_name='especialistas', on_delete=models.DO_NOTHING)
    subespecialidad = models.ForeignKey(EspecialidadMedica, related_name='subespecialistas', on_delete=models.DO_NOTHING, null=True, blank=True)

    class Meta:
        verbose_name = 'Medico'
        verbose_name_plural = 'Medicos'


#########################################################################
#                            Pacientes
#########################################################################
class Paciente(Natural):
    class Meta:
        verbose_name = 'Paciente'
        verbose_name_plural = 'Pacientes'


#########################################################################
#                  Log de la serie de orden de venta
#########################################################################
class LogSerieOrdenVenta(models.Model):
    nuevo_valor = models.CharField(blank=True, max_length=100)
    fecha_modificacion = models.DateTimeField(default=datetime.now)

    class Meta:
        verbose_name = 'Log de SerieCotizacion'
        verbose_name_plural = 'Logs de SerieCotizacion'

    def __str__(self):
        "cambio: %s el %s" %s (self.nuevo_valor, self.fecha_modificacion)


#########################################################################
#                           Orden de venta
#########################################################################
class OrdenVenta(models.Model):
    ESTADOS = [
        ('incompleta', 'Incompleta'),
        ('porprocurar', 'Por procurar'),
        ('porenviar', 'Por enviar'),
        ('enlicitacion', 'En licitación'),
        ('adjudicada', 'Adjudicada'), # Adjudicada
        ('productosentregados', 'Productos entregados'), # Adjudicada
        ('rechazada', 'Rechazada'), # Rechazada
    ]
    #
    OPCIONES_RECHAZO = [
        ('disponibilidad', 'Por disponibilidad'),
        ('precio', 'Por precio'),
        ('tiempo', 'Por tiempo de entrega')
    ]
    codigo = models.CharField(max_length=100, null=True, blank=True, verbose_name='Código')
    status = models.CharField(max_length=100, choices=ESTADOS, default='porenviar')
    empresa = models.ForeignKey(Empresa, related_name='ordenes_venta', on_delete=models.DO_NOTHING)
    cliente = models.ForeignKey(Cliente, related_name='ordenes_venta_como_cliente', on_delete=models.DO_NOTHING)
    contacto_cliente = models.ForeignKey(Contacto, related_name='ordenes_venta_como_contacto_cliente', null=True, blank=True, on_delete=models.DO_NOTHING)
    hospital = models.ForeignKey(Hospital, on_delete=models.DO_NOTHING)
    contacto_hospital = models.ForeignKey(Contacto, related_name='ordenes_venta_como_contacto_hospital', null=True, blank=True, on_delete=models.DO_NOTHING)
    medico = models.ForeignKey(Medico, null=True, blank=True, verbose_name='Médico', on_delete=models.SET_NULL)
    paciente = models.ForeignKey(Paciente, null=True, blank=True, on_delete=models.SET_NULL)
    datos_entrega = models.ForeignKey(DatosEntrega, related_name='ordenes_venta', on_delete=models.DO_NOTHING)
    razon_rechazo = models.CharField(max_length=100, choices=OPCIONES_RECHAZO, null=True, verbose_name='Razón')
    comentario_rechazo = models.TextField(blank=True, null=True, verbose_name='Observación')
    fecha_cirugia = models.DateTimeField(blank=True, null=True, verbose_name='Fecha de cirugía')
    fecha_creacion = models.DateTimeField(blank=True, auto_now_add=True)
    fecha_modificacion = models.DateTimeField(blank=True, auto_now=True)

    def verbose_status(self):
        for estado in self.ESTADOS:
            if estado[0] == self.status:
                return estado[1]
        return None

    def verbose_razon_rechazo(self):
        for razon in self.OPCIONES_RECHAZO:
            if razon[0] == self.razon_rechazo:
                return razon[0]
        return None

    def color_status(self):
        colors = {
            'porenviar': 'default',
            'enlicitacion': 'info',
            'adjudicada': 'success',
            'rechazada': 'danger'
        }
        return colors.get(self.status, 'default')

    def generar_codigo(self):
        """
        Genera código de la cotización
        """
        # ultimo_cambio = LogSerieCotizacion.objects.filter(fecha__lte=self.fecha_creacion).order_by('fecha').first()
        proximo, created = MetaInformacion.objects.get_or_create(
                                    clave='serie_cotizacion',
                                    defaults={'valor': 1}
                                )

        proximo_valor = str(proximo.valor).zfill(5)
        proximo.valor = int(proximo.valor) + 1
        proximo.save()

        hoy = date.today()
        mes = '0%d' % hoy.month if hoy.month < 10 else hoy.month
        anho = hoy.year % 100 # para obtener los ultimos dos digitos del año
        initial = self.empresa.nombre[0].upper()
        self.codigo = "%s-%s%s-%s" % (initial, mes, anho, proximo_valor)

    def adjudicar(self, almacen_origen, fecha_compromiso=None):
        """
        Adjudica licitación
        """
        if self.status == 'enlicitacion':
            self.status = 'adjudicada'
            self.save()
            self.activar_pedidos_compra()
            fecha_compromiso = fecha_compromiso or self.fecha_cirugia
            self.generar_actualizacion_venta(almacen_origen, fecha_compromiso)

    def activar_pedidos_compra(self):
        """
        Activa pedidos de compra asociados a la cotización
        """
        for pedido in self.pedidos_de_compra.all():
            pedido.activar()

    def generar_actualizacion_venta(self, almacen_origen, fecha_compromiso):
        """
        Crea una actualización de venta
        """
        ActualizacionVenta.objects.create(**{
            'orden': self,
            'tipo': 'salida',
            'motivo': 'entregaproductos',
            'almacen_origen': almacen_origen,
            'almacen_destino': Almacen.objects.get(pk=2), # Almacen virtual del cliente
            'fecha_inicio': fecha_compromiso,
            'fecha_fin': fecha_compromiso,
        })

    def puede_ser_entregado(self):
        todos = True
        for producto_venta in self.productos:
            if EstereotipoAlmacen.existencia_real(producto_venta) < producto_venta.cantidad:
                todos = False
                break
        return todos

    def productos_entregados(self):
        if self.status == 'enlicitacion':
            self.status = 'productosentregados'
            self.save()

    class Meta:
        verbose_name = 'Orden de Venta'
        verbose_name_plural = 'Ordenes de Venta'

    def __str__(self):
        self.codigo


#########################################################################
#                     Fila de la orden de venta
#########################################################################

class ProductoVenta(models.Model):
    incluido = models.BooleanField(default=True, verbose_name='Incluido')
    partida = models.IntegerField(null=True)
    estereotipo = models.ForeignKey(EstereotipoProducto,
                                    related_name='productos_ventas',
                                    verbose_name='Producto',
                                    on_delete=models.DO_NOTHING)
    alias = models.CharField(max_length=512, null=True, blank=True)
    orden_venta = models.ForeignKey(OrdenVenta,
                                    related_name='productos_ventas',
                                    on_delete=models.CASCADE
                                    )
    cantidad = models.IntegerField(default=1, null=True, blank=True)
    precio_unidad = models.DecimalField(max_digits=10,
                                        decimal_places=2,
                                        verbose_name='Precio por unidad',
                                        null=True,
                                        blank=True,
                                        validators=[MinValueValidator(Decimal('0.01'))])

    def total(self):
        if not self.cantidad or not self.precio_unidad:
            return 0
        return self.cantidad * self.precio_unidad

    class Meta:
        verbose_name = 'Producto en Venta'
        verbose_name_plural = 'Productos en Venta'

    def __str__(self):
        self.estereotipo
