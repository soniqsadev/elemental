H34.table_orden = function () {

    var sumar = function (row) {
        cantidad = $('.cantidad input', row).val();
        precio = $('.precio input', row).val();
        $('.total_fila input', row).val( (cantidad*precio).toFixed(2) );

        calculo_total();
    };

    var calculo_total = function () {
        tbody = $('table tbody#container_producto');
        // Calculo de subtotal
        subtotal = 0.0;
        $.each(tbody.children('tr'), function(index_tr, tr) {
            if( $(tr).hasClass('hidden') === false ){
                valor = parseFloat( $(".total_fila input[type=hidden]", tr).val() );
                if ( isNaN(valor) == false ){
                        subtotal = subtotal + valor;
                    }
            }
        });
        // Calculo de IVA y total
        iva = $("#iva_porcentaje select").val()
        $("#subtotal").html(subtotal.toFixed(2));
        $("#iva").html( ( subtotal*iva ).toFixed(2) );
        $("#total").html( (subtotal + subtotal*iva ).toFixed(2) );
    }

    tbody = $('table tbody#container_producto');
    $.each(tbody.children('tr'), function(index, el) {
        $(el).children('.precio').on('change', function (e) {
            e.preventDefault();
            sumar(e.target.parentNode.parentNode);
        });
        $(el).children('.cantidad').on('change', function (e) {
            e.preventDefault();
            sumar(e.target.parentNode.parentNode);
        });
        // sumar las filas cuando se carga la pagina
        sumar(el);
    });

    calculo_total();

}

$(H34.table_orden);
H34.eventos.addEvento(H34.table_orden);
