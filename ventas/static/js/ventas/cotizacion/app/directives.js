angular.module('tabla-cotizacion')
.directive('dateTimePicker', function ($timeout, $parse) {
    return {
        link: function($scope, element, $attrs) {
            console.log("attrs", $attrs);
            // console.log("element", element);
            // console.log("val()", element.val());
            $attrs.$observe('ngValue', function (v) {
                console.log('value dentro de observe', v);
                element.val(v);
            })
            return $timeout(function() {
                var ngModelGetter = $parse($attrs['ngModel']);

                return $(element).datetimepicker({
                        locale:"es"
                    }).on('dp.change', function(event) {
                        $scope.$apply(function() {
                            return ngModelGetter.assign($scope, event.target.value);
                        });
                });
            });
        }
    };
});
