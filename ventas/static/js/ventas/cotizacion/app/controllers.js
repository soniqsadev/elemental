angular.module('tabla-cotizacion')
.controller('TableController', function ($scope
                                        , $compile
                                        , HttpLocal
                                        , RequisicionCompraResource
                                        , AlmacenResource
                                        , ProveedorResource
                                        , ProductoCompraResource) {

    $scope.proveedores = {};
    $scope.almacenes = AlmacenResource.query();
    $scope.producto = {'id':null};
    $scope.producto_compra = {'id':null};
    $scope.orden_venta = $('#orden_venta').val();
    $scope.row = null;

    /************************************************************
     *                    Gestión de formset
     ************************************************************/

     propiedad_producto_formset = new H34.formset({
         container_id : "#container_producto",
         empty_form_id: "#empty_form_producto"
     });

    propiedad_producto_formset.addForm = function () {
        new_form = $compile( this.replaceTotalForm( this.getNewForm() ) )($scope);
        that = this;
        new_form.find(this.delete_class).on('click', function(e){
            e.preventDefault();
            that.deleteForm(this.parentNode.parentNode)
        });
        this.incrementTotalForms()
        this.getContainer().append(new_form)
        H34.eventos.DomModificado();
    }

    propiedad_producto_formset.deleteForm = function (form) {
        modal = BootstrapDialog;

        var enviar_formulario = function () {
            document.getElementById('redireccion').value = 'edit';
            document.getElementsByTagName("FORM")[0].submit();
        }

        var confirmacion = function(res){
            console.log(form)
            if(res){
                form = $(form)
                form.find('input[type=checkbox]').prop('checked', 'checked')
                form.addClass('hidden');
                H34.eventos.DomModificado();
                // Eliminar pedidos de compra asociados a este producto
                HttpLocal.get('/api/compras/producto-compra/orden-venta/'+ $scope.orden_venta +'/producto/'+ $('.producto select', form).val() +'/')
                .then(function (res) {
                    ProductoCompraResource.delete({id:res.data.id}, function (data) {
                        console.log(data);
                        enviar_formulario();
                    })
                }, function (fail) {
                    console.log('fail', fail);
                    enviar_formulario();
                })
            }
        };

        modal.confirm({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'Advertencia',
                        message: "¿Confirma que deseas eliminar este elemento?",
                        callback: confirmacion
                    });
    }

    /************************************************************
     *
     ************************************************************/

    $scope.getRow = function ($event) {
        get_data_from_row($event.currentTarget);
    }

    $scope.getTd = function ($event) {
        get_data_from_row($event.currentTarget.parentNode);
    }

    $scope.getInputTd = function ($event) {
        get_data_from_row($event.currentTarget.parentNode.parentNode);
    }


    // Salva la orden de compra y la cotización asociada
    $scope.salvarOrden = function ($event) {

        var enviar_formulario = function () {
            document.getElementById('redireccion').value = 'edit';
            document.getElementsByTagName("FORM")[0].submit();
        }

        // Comprueba que no haya errores en la orden de compra
        var is_error_in_orden = function () {
            var errors = false;

            var proveedor = $scope.producto_compra.proveedor || null;
            var fecha_compromiso = $scope.producto_compra.fecha_compromiso || null;
            var precio_pedido = $scope.producto_compra.precio_unidad || null;
            var almacen_destino = $scope.producto_compra.almacen_destino || null;
            var precio_producto = $scope.producto.precio_unidad || null;

            //    Las los campos requerido para la orden de compra son solo requeridos
            // si el producto ya posee proveedores
            if ($scope.proveedores.length > 0) {
                if (   proveedor===null
                    || precio_pedido===null
                    || almacen_destino===null
                ){
                    errors = true;
                }

                var mensaje = 'Antes debe seleccionar:';
                mensaje += '<ul>';

                if(proveedor===null){
                    mensaje += '<li>Un proveedor</li>';
                    $('#proveedor').addClass('errores');
                }else{
                    $('#proveedor').removeClass('errores');
                }

                if(almacen_destino===null){
                    mensaje += '<li>Un almacén de destino</li>';
                    $('#almacen_destino').addClass('errores');
                }else{
                    $('#almacen_destino').removeClass('errores');
                }

                if(precio_pedido===null){
                    mensaje += '<li>Un precio por unidad para la compra de este producto</li>';
                    $('#precio').addClass('errores');
                }else{
                    $('#precio').removeClass('errores');
                }

                mensaje += '</ul>';

                if(errors){
                    BootstrapDialog.alert({
                                    type: BootstrapDialog.TYPE_DANGER,
                                    title: 'Error en campos de entrada',
                                    message: mensaje,
                                });
                }
            }

            return errors;
        }

        // Retorno los datos del pedido de compra
        var get_data = function () {
            data = {}
            data['empresa'] = $('#id_empresa').val();
            data['orden_venta'] = $scope.orden_venta;
            data['estereotipo'] = $scope.producto.id;
            data['cantidad'] = $scope.producto_compra.cantidad;
            data['proveedor'] = $scope.producto_compra.proveedor;
            data['almacen_destino'] = $scope.producto_compra.almacen_destino;
            if ( typeof($scope.producto_compra.fecha_compromiso) !== 'undefined' && $scope.producto_compra.fecha_compromiso.length > 0 ) {
                data['fecha_compromiso'] = moment($scope.producto_compra.fecha_compromiso, "DD/MM/YYYY HH:mm").format('YYYY-MM-DD HH:mm');
            }
            data['precio_unidad'] = $scope.producto_compra.precio_unidad;
            console.log('data', data);

            return data;
        }

        // Crear pedido de compra
        var crear = function (data) {
            $($event.currentTarget).text('Procesando...');
            HttpLocal.post('/api/compras/producto-compra/', data)
            .then(
                function (data) {
                    console.log(data);
                    $($event.currentTarget).text('Almacenado');
                    enviar_formulario();
                },
                function (res) {
                    $($event.currentTarget).text('No ha podido ser almacenado');
                    console.log(res);
                });
        };

        // Actualizar pedido de compra
        var actualizar = function (data) {
            $($event.currentTarget).text('Procesando...');
            ProductoCompraResource.update({id:$scope.producto_compra.id}, data, function (data) {
                console.log(data);
                $($event.currentTarget).text('Almacenado');
                enviar_formulario()
            }, function (data) {
                $($event.currentTarget).text('No pudo ser Almacenado');
                console.log('fail', data);
            });
        }

        // Salva el pedido de compra
        var salvar = function () {
            if(is_error_in_orden())
                return;

            data = get_data();
            console.log(data);
            if($scope.producto_compra.id === null){
                crear(data);
            }else{
                actualizar(data);
            }
        }

        // ejectamos nuesra tarea principal
        salvar();

    }


    // Comprueba que los datos para la cotización esten completos
    is_errors = function () {
        errors = false;
        empresa = $('#id_empresa').val() || null;
        cliente = $('#id_cliente').val() || null;
        hospital = $('#id_hospital').val() || null;
        medico = $('#id_medico').val() || null;
        paciente = $('#id_paciente').val() || null;

        if (   empresa===null
            || cliente===null
            || hospital===null
            || medico===null
            || paciente===null
        ){
            errors = true;
        }

        mensaje = 'Antes debe seleccionar:';
        mensaje += '<ul>';

        if(empresa===null){
            mensaje += '<li>Una empresa</li>';
            $('#id_empresa').addClass('errores');
        }else{
            $('#id_empresa').removeClass('errores');
        }
        if(cliente===null){
            mensaje += '<li>Un cliente</li>';
            $('#id_cliente').addClass('errores');
        }else{
            $('#id_cliente').removeClass('errores');
        }
        if(hospital===null){
            mensaje += '<li>Un hospital</li>';
            $('#id_hospital').addClass('errores');
        }else{
            $('#id_hospital').removeClass('errores');
        }
        if(medico===null){
            mensaje += '<li>Un médico</li>';
            $('#id_medico').addClass('errores');
        }else{
            $('#id_medico').removeClass('errores');
        }
        if(paciente===null){
            mensaje += '<li>Un paciente</li>';
            $('#id_paciente').addClass('errores');
        }else{
            $('#id_paciente').removeClass('errores');
        }

        mensaje += '</ul>';

        if(errors){
            BootstrapDialog.alert({
                            type: BootstrapDialog.TYPE_DANGER,
                            title: '¡¡¡ERROR!!!',
                            message: mensaje,
                        });
        }

        return errors;
    }


    get_data_from_row = function (row) {
        $scope.row = row;

        // Si hay errores se cancela el proceso
        if( is_errors() )
            return;

        // cambio comprueba si se ha seleccionado un nuevo producto
        var cambio = $scope.producto.id !== ( $('.producto select', row).val() || null );

        //  Se obtiene las especificaciones de la linea del producto
        // que se esta tabajando
        $scope.producto.id = $('.producto select', row).val() || null;
        var cantidad = $('.cantidad input', row).val() || null;
        var precio_unidad = $('.precio input', row).val() || null;

        //   Si el producto.id es null significa que se esta creando una nueva
        // linea en la cotización
        if ($scope.producto.id !== null) {

            // Obtener la lista de proveedores para el producto seleccionado
            HttpLocal.get('/api/compras/producto-compra/'+ $scope.producto.id +'/proveedores/')
            .then(function (proveedores) {
                $scope.proveedores = proveedores.data
                for (var i = 0; i < $scope.proveedores.length; i++) {
                    // postulacion-proveedor/proveedor/(?P<proveedor_id>[0-9]+)/estereotipo/(?P<estereotipo_id>[0-9]+)/
                    HttpLocal.get('/api/compras/postulacion-proveedor/proveedor/'+ $scope.proveedores[i].id + '/estereotipo/' + $scope.producto.id +'/')
                    .then(function (res) {
                        postulacion = res.data
                        if (postulacion != null && postulacion.proveedor != null) {
                            proveedor = get_proveedor_por_id(postulacion.proveedor);
                            proveedor.precio_unidad = postulacion.precio_unidad;
                            console.log('proveedor', proveedor)
                        }
                    }, function () {
                        console.log('Fallo al conseguir los precios para el proveedor')
                    })

                    // $scope.proveedores[i]
                }
            }, function (fail) {
                console.log('fallo al intentar conseguir los proveedores', fail);
            });

            // Obtener la existencia del producto en almacen
            HttpLocal.get('/api/almacen/producto/' + $scope.producto.id + '/existencia/')
            .then(function (res) {

                disponibilidad = JSON.parse(res.data).count;
                $scope.producto.disponibilidad = isNaN(disponibilidad) ? 0 : parseInt(disponibilidad);
                $scope.producto.cantidad = isNaN(cantidad) ? 0 : parseInt(cantidad);
                $scope.producto.precio_unidad = isNaN(precio_unidad) ? 0 : parseInt(precio_unidad);

                if(cambio){
                    console.log('cambio')
                    if($scope.producto.cantidad > $scope.producto.disponibilidad){
                        HttpLocal.get('/api/compras/producto-compra/orden-venta/'+ $scope.orden_venta +'/producto/'+ $scope.producto.id +'/')
                        .then(function (data_producto_compra) {
                            // console.log(data_producto_compra.data);
                            $('#submit-pedido').text('Modificar orden de compra')

                            if ( typeof(data_producto_compra.data.proveedor) !== 'undefined' && data_producto_compra.data.proveedor !== null ) {
                                $scope.producto_compra.proveedor = data_producto_compra.data.proveedor.toString();
                            }
                            $scope.producto_compra.id = data_producto_compra.data.id;
                            $scope.producto_compra.cantidad = data_producto_compra.data.cantidad;
                            $scope.producto_compra.almacen_destino = data_producto_compra.data.almacen_destino !== null ? data_producto_compra.data.almacen_destino.toString() : "";
                            $scope.producto_compra.fecha_compromiso = data_producto_compra.data.fecha_compromiso !== null ? moment(data_producto_compra.data.fecha_compromiso).format('DD/MM/YYYY H:mm') : "";
                            $scope.producto_compra.precio_unidad = parseFloat(data_producto_compra.data.precio_unidad);
                        }, function (fail) {
                            $('#submit-pedido').text('Crear orden de compra')
                            $scope.producto_compra = {'id':null};
                            $scope.producto_compra.cantidad = $scope.producto.cantidad - $scope.producto.disponibilidad;
                            console.log('fallo en conseguir la orden de venta', fail);
                        })
                    }
                }else{
                    $scope.producto_compra.cantidad = $scope.producto.cantidad - $scope.producto.disponibilidad;
                }

            }, function () {
                BootstrapDialog.alert({
                                type: BootstrapDialog.TYPE_DANGER,
                                title: '¡¡¡ERROR!!!',
                                message: 'Se ha producido un error, por favor compruebe su conexión a internet',
                            });
            });
            contenido = $('.producto select option:selected', row).text();
            $scope.producto.nombre = contenido;
        }else{
            $scope.producto = {};
            $scope.producto.id = null;
            $scope.producto.cantidad = 0;
            $scope.producto.disponibilidad = 0;
        }
    }

    $scope.cambio_proveedor = function () {
        console.log('cambio el proveedor', $scope.producto_compra.proveedor)
        if($scope.producto_compra.proveedor.length > 0 && $scope.producto_compra.proveedor != null){
            proveedor = get_proveedor_por_id($scope.producto_compra.proveedor);
            $scope.producto_compra.precio_unidad = parseFloat(proveedor.precio_unidad);
        }else {
            $scope.producto_compra.precio_unidad = '';
        }
    }

    get_proveedor_por_id = function (id) {
        for (var i = 0; i < $scope.proveedores.length; i++) {
            if ($scope.proveedores[i].id == id)
                return $scope.proveedores[i];
        }
        return null;
    }

});
