$(function () {
    $("#id_especialidad").on('change', function(event) {
        event.preventDefault();
        selected_id = this.options[this.selectedIndex].value;
        $.getJSON('/api/ventas/medico/especialidad/' + selected_id + '/subespecialidades', {}, function(json, textStatus) {
                $.get('/static/js/ventas/templates/subespecialidades.ejs', function (template) {
                    options = ejs.render(template, {'subespecialidades':json})
                    $('#id_subespecialidad').html($(options));
                })
        });
    });
})
