$(function (e) {

    var modal = $('.modal.add_new_producto');

    function get_modal(id) {
        id = id || 'modal';
        modal = '';

        modal += '<div class="modal add_new_producto fade" id="'+ id +'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">';
        modal += '  <div class="modal-dialog modal-lg" role="document">';
        modal += '      <div class="modal-content">';
        modal += '          <div class="modal-header">';
        modal += '              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        modal += '              <h4 class="modal-title" id="exampleModalLabel">Nuevo producto</h4>';
        modal += '          </div>';
        modal += '          <div class="modal-body">';
        modal += '              <form>';
        modal += '                  <div class="form-group">';
        modal += '                      <label for="nombre" class="control-label">Nombre:</label>';
        modal += '                      <input type="text" class="form-control" id="nombre">';
        modal += '                  </div>';
        modal += '                  <div class="form-group">';
        modal += '                      <label for="descripcion" class="control-label">Descripción:</label>';
        modal += '                      <textarea class="form-control" id="descripcion"></textarea>';
        modal += '                  </div>';
        modal += '              </form>';
        modal += '          </div>';
        modal += '          <div class="modal-footer">';
        modal += '              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>';
        modal += '              <button type="button" class="btn btn-primary" id="guardar">Guardar</button>';
        modal += '          </div>';
        modal += '      </div>';
        modal += '  </div>';
        modal += '</div>';

        modal = $(modal);

        $('#guardar', modal).on('click', function (e) {
            salvar_producto();
            modal.modal('hide');
        })

        return modal;
    }


    function get_data_producto() {
        data = {};
        data['nombre'] = $('#nombre', modal).val();
        data['descripcion'] = $('#descripcion', modal).val();

        return data;
    }


    function limpiar_campos_modal() {
        $('#nombre', modal).val('');
        $('#descripcion', modal).val('');
    }


    function obj2htmlselect(productos) {
        html  = '';
        html += '<select class="form-control searchable input-sm">';
        console.log('productos.length', productos.length);
        for (var i = 0; i < productos.length; i++) {
            console.log(productos[i]);
            html += ( '<option value="' + productos[i].id + '">' + productos[i].nombre + '</option>' );
        }
        html += '</select>';
        return html;
    }


    function actualizar_listados_productos() {
        $.ajax({
            url: '/api/producto/'
        })
        .done(function(res) {
            $('.producto').html( obj2htmlselect(res) );

            H34.eventos.DomModificado();
            limpiar_campos_modal();
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    }


    function salvar_producto() {
        $.ajax({
            url: '/api/producto/',
            type: 'POST',
            data: get_data_producto()
        })
        .done(function() {
            BootstrapDialog.alert({
                type: BootstrapDialog.TYPE_INFO,
                title: 'Información',
                message: "Se ha completado la creación del nuevo producto",
            });
            actualizar_listados_productos();
            console.log("success");
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    }


    $('a.add_producto').on('click', function (e) {
        e.preventDefault();

        // Generación del identificador
        rango = 10000;
        identificador = parseInt( (Math.random() * rango) + 1 );

        // Se comprueba que ya haya sido generado el modal
        if ( modal.length <= 0 ) {
            modal = get_modal('new_producto_'+identificador);

            body = $('body');
            $(body).append( modal );
        }

        // Se hace visible el modal
        modal.modal({
            show:true
        })

    });

});
