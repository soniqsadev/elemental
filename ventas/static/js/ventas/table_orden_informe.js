$(function () {
    var numberWithCommas = function(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    };

    var subtotal = 0.00;
    $.each($(".total_fila"), function(index, el) {
        cleaned = $(el).html().replace(/\,/g,'');
        console.log( cleaned );
        valor = parseFloat( cleaned );
        console.log(valor);
        if ( isNaN(valor) == false ){
            subtotal = subtotal + valor;
        }
    });
    $('#subtotal').html( numberWithCommas(subtotal.toFixed(2)) );
    iva_porcentaje = parseFloat( $("#iva_porcentaje input[type=hidden]").val() );
    console.log(iva_porcentaje);
    $('#iva').html( numberWithCommas( (subtotal*iva_porcentaje).toFixed(2) ) );
    $('#total').html( numberWithCommas( (subtotal*iva_porcentaje + subtotal).toFixed(2) ) );
})
