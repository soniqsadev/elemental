from django.contrib import admin

from .models import Hospital, Medico, EspecialidadMedica

@admin.register(Hospital)
class HospitalAdmin(admin.ModelAdmin):
    pass

@admin.register(Medico)
class MedicoAdmin(admin.ModelAdmin):
    pass

@admin.register(EspecialidadMedica)
class EspecialidadMedicaAdmin(admin.ModelAdmin):
    pass
