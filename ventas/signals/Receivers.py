from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from core.models import MetaInformacion
from ..models import LogSerieOrdenVenta, OrdenVenta

@receiver(pre_save, sender=MetaInformacion)
def metainformacion_pre_save(sender, **kwargs):
    instance = kwargs.get('instance', None)

    if instance.clave == 'serie_cotizacion' and not instance.id:
        LogSerieOrdenVenta.objects.create(**{'nuevo_valor': instance.valor})


@receiver(pre_save, sender=OrdenVenta)
def ordenventa_pre_save(sender, **kwargs):
    instance = kwargs.get('instance', None)

    if not instance.id:
        instance.generar_codigo()
