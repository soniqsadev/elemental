from django.conf.urls import url

from .views import Almacen, Actualizacion, Producto, Conciliacion

from .api import views


urlpatterns = [
    # Almacenes
    url(regex=r'^almacen/$',
        view=Almacen.ListView.as_view(),
        name='almacen.list'),
    url(regex=r'^almacen/create/$',
        view=Almacen.CreateView.as_view(),
        name='almacen.create'),
    url(regex=r'^almacen/(?P<pk>[0-9]+)/$',
        view=Almacen.DetailView.as_view(),
        name='almacen.detail'),
    url(regex=r'^almacen/(?P<pk>[0-9]+)/edit/$',
        view=Almacen.UpdateView.as_view(),
        name='almacen.update'),
    url(regex=r'^almacen/(?P<pk>[0-9]+)/delete/$',
        view=Almacen.DeleteView.as_view(),
        name='almacen.delete'),

    # Actualizaciones
    url(regex=r'^actualizacion/$',
        view=Actualizacion.ListView.as_view(),
        name='actualizacion.list'),
    url(regex=r'^actualizacion/compra/$',
        view=Actualizacion.ListCompraView.as_view(),
        name='actualizacion.compra.list'),
    url(regex=r'^actualizacion/venta/$',
        view=Actualizacion.ListVentaView.as_view(),
        name='actualizacion.venta.list'),
    url(regex=r'^actualizacion/interna/$',
        view=Actualizacion.ListInternaView.as_view(),
        name='actualizacion.interna.list'),

    url(regex=r'^actualizacion/compra/(?P<pk>[0-9]+)/procesar/new/$',
        view=Actualizacion.ProcesarCompraNewView.as_view(),
        name='actualizacion.compra.procesar.new'),

    url(regex=r'^actualizacion/compra/(?P<pk>[0-9]+)/procesar/$',
        view=Actualizacion.ProcesarCompraView.as_view(),
        name='actualizacion.compra.procesar'),

    url(regex=r'^actualizacion/venta/(?P<pk>[0-9]+)/procesar/new/$',
        view=Actualizacion.ProcesarVentaNewView.as_view(),
        name='actualizacion.venta.procesar'),

    url(regex=r'^conciliacion/$',
        view=Conciliacion.ListView.as_view(),
        name='almacen.conciliacion.list'),
    url(regex=r'^conciliacion/create/$',
        view=Conciliacion.CreateView.as_view(),
        name='almacen.conciliacion.create'),
    url(regex=r'^conciliacion/(?P<pk>[0-9]+)/$',
        view=Conciliacion.DetailView.as_view(),
        name='almacen.conciliacion.detail'),
    url(regex=r'^conciliacion/(?P<pk>[0-9]+)/edit/$',
        view=Conciliacion.UpdateView.as_view(),
        name='almacen.conciliacion.update'),
    url(regex=r'^conciliacion/(?P<pk>[0-9]+)/delete/$',
        view=Conciliacion.DeleteView.as_view(),
        name='almacen.conciliacion.delete'),

    #
    url(regex=r'^producto/$',
        view=Producto.ListView.as_view(),
        name='producto.list'),
    url(regex=r'^producto/(?P<pk>[0-9]+)/$',
        view=Producto.IndividualView.as_view(),
        name='producto.individual'),

]
