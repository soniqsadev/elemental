from rest_framework import serializers

from core.models import Producto
from ..models import Almacen


class AlmacenSerializer(serializers.ModelSerializer):
    """docstring for AlmacenSerializer"""
    class Meta:
        model = Almacen
        fields = '__all__'


class ProductoSerializer(serializers.ModelSerializer):
    """
    Serializador para la clase Producto
    """
    estereotipo_verbose = serializers.CharField(required=False)

    class Meta:
        model = Producto
        fields = '__all__'
