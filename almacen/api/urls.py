from django.conf.urls import url
from . import views

urlpatterns = [
    # existencia fisica
    url(regex=r'^producto/(?P<pk>[0-9]+)/existencia/fisica/$',
        view=views.existencia_producto_fisica,
        name='api.producto.existencia.fisica'),

    url(regex=r'^producto/(?P<pk>[0-9]+)/existencia/$',
        view=views.existencia_producto_view,
        name='api.producto.existencia'),

    url(regex=r'^producto/existencia/$',
        view=views.existencia_productos,
        name='api.productos.existencia'),

    url(regex=r'^almacen/$',
        view=views.AlmacenList.as_view(),
        name='api.almacen.list'),

    url(regex=r'^almacen/(?P<pk>[0-9]+)/productos/$',
        view=views.productos_por_almacen,
        name='api.almacen.list'),

    # Búsqueda por código
    url(regex=r'^almacen/(?P<almacen_id>[0-9]+)/producto/(?P<codigo>[\s\w]+)/codigo/$',
        view=views.producto_por_codigo,
        name='api.producto.existencia'),

    # Conciliación
    url(regex=r'^almacen/(?P<pk>[0-9]+)/productos/conciliacion/$',
        view=views.sincronizar_almacen,
        name='api.almacen.conciliacion'),

    # Actualización de compra (entrada)
    url(regex=r'^almacen/(?P<pk>[0-9]+)/actualizacion-compra/entrada/$',
        view=views.procesar_actualizacion_compra,
        name='api.almacen.conciliacion'),

    # Actualización de venta (salida)
    url(regex=r'^almacen/(?P<pk>[0-9]+)/actualizacion-venta/salida/$',
        view=views.procesar_actualizacion_venta,
        name='api.almacen.conciliacion'),

    url(regex=r'^almacen/(?P<pk>[0-9]+)/productos/is/$',
        view=views.sincronizar_almacen,
        name='api.almacen.is'),
]
