import json

from django.http import JsonResponse

from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from ..models import (Estereotipo,
                      Producto,
                      Almacen,
                      AlmacenQueries,
                      ActualizacionInterna,
                      ActualizacionCompra,
                      ActualizacionVenta)
from .serializers import AlmacenSerializer, ProductoSerializer


@api_view(['GET'])
def existencia_producto_fisica(request, *args, **kwargs):
    productos = Estereotipo.productos_existencia_fisica(
                                    Estereotipo.get_object(kwargs['pk'])
                                )
    serializer = ProductoSerializer(productos, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def existencia_producto_view(request, *args, **kwargs):
    count = Estereotipo.existencia_real(Estereotipo.get_object(kwargs['pk']))
    res = {'count': count}
    return Response(json.dumps(res))


@api_view(['GET'])
def existencia_productos(request, *args, **kwargs):
    productos = Estereotipo.get_productos()
    # return Response(json.dumps(productos))
    return JsonResponse(productos, safe=False)


@api_view(['GET'])
def producto_por_codigo(request, *args, **kwargs):
    print('codigo', kwargs['codigo'])
    producto = AlmacenQueries.is_in_almacen(
                            AlmacenQueries.get_object(kwargs['almacen_id']),
                            kwargs['codigo'])
    if producto:
        producto.estereotipo_verbose = producto.estereotipo.nombre
        serializers = ProductoSerializer(producto)
        return Response(serializers.data)
    return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def productos_por_almacen(request, *args, **kwargs):
    """
    Usada para retornar los productos presentes en un almacen
    """
    productos = AlmacenQueries.get_productos(
                                AlmacenQueries.get_object(kwargs['pk'])
                            )
    for producto in productos:
        producto.estereotipo_verbose = producto.estereotipo.nombre
    serializer = ProductoSerializer(productos, many=True)
    return Response(serializer.data)


################################################################
#                      SINCRONIZAR ALMACEN
################################################################
@api_view(['POST'])
def sincronizar_almacen(request, *args, **kwargs):
    """
    Funcion usada para la sincronizacion de almancen
    """
    conciliacion_id = request.POST.getlist('conciliacion', None)
    nuevos_cantidad = request.POST.getlist('nuevoscantidad', None)
    confirmados = request.POST.getlist('confirmados[]')

    if nuevos_cantidad is None or conciliacion_id is None:
        return JsonResponse({'message': 'data incompleta!'})

    nuevos_cantidad = nuevos_cantidad.pop()
    conciliacion_id = conciliacion_id.pop()
    nuevos = []

    for n in range(0, int(nuevos_cantidad)):
        codigo = request.POST.getlist('nuevos[%s][codigo]' % n, None)
        estereotipo = request.POST.getlist('nuevos[%s][estereotipo]' % n, None)
        fecha_vencimiento = request.POST.getlist(
                                        'nuevos[%s][vencimiento]' % n,
                                        None
                                    )
        if codigo is None or estereotipo is None:
            return JsonResponse({'message': 'data incompleta!'})

        nuevos.append({
            "codigo": codigo.pop(),
            "estereotipo": estereotipo.pop(),
            "vencimiento": fecha_vencimiento.pop()
        })

    conciliacion = ActualizacionInterna.objects.get(pk=conciliacion_id)

    print('confirmados', confirmados)
    print('nuevos', nuevos)

    productos_eliminar = AlmacenQueries.get_productos(
                                    conciliacion.almacen_origen
                                ).exclude(id__in=confirmados)
    conciliacion.retirar(productos_eliminar)

    for nuevo in nuevos:
        conciliacion.ingresar(
                        nuevo['codigo'],
                        Estereotipo.get_object(nuevo['estereotipo']),
                        nuevo['vencimiento']
                    )

    conciliacion.completar()

    return JsonResponse({'message': 'to bien!'})


################################################################
#           PROCESAR ACTUALIZACON DE COMPRA (ENTRADA)
################################################################
def procesar_actualizacion_compra(request, *args, **kwargs):
    """
    Función para procesar el ingreso de productos en el almacen
    """
    print(request.POST)
    actualizacion_id = request.POST.getlist('actualizacion', None)
    nuevos_cantidad = request.POST.getlist('nuevoscantidad', None)

    if nuevos_cantidad is None or actualizacion_id is None:
        return JsonResponse({'message': 'data incompleta!'})

    nuevos_cantidad = nuevos_cantidad.pop()
    actualizacion_id = actualizacion_id.pop()
    nuevos = []

    for n in range(0, int(nuevos_cantidad)):
        codigo = request.POST.getlist('nuevos[%s][codigo]' % n, None)
        estereotipo = request.POST.getlist('nuevos[%s][estereotipo]' % n, None)
        fecha_vencimiento = request.POST.getlist(
                                        'nuevos[%s][vencimiento]' % n,
                                        None
                                    )
        ubicacion = request.POST.getlist('nuevos[%s][ubicacion]' % n, None)

        if codigo is None or estereotipo is None:
            return JsonResponse({'message': 'data incompleta!'})

        nuevo = {
            "codigo": codigo.pop(),
            "estereotipo": estereotipo.pop(),
        }

        if fecha_vencimiento is not None and len(fecha_vencimiento) > 0:
            nuevo['vencimiento'] = fecha_vencimiento.pop()
        else:
            nuevo['vencimiento'] = None

        if ubicacion is not None and len(ubicacion) > 0:
            nuevo['ubicacion'] = ubicacion.pop()
        else:
            nuevo['ubicacion'] = None

        nuevos.append(nuevo)

    actualizacion = ActualizacionCompra.objects.get(pk=actualizacion_id)

    print('nuevos', nuevos)

    for nuevo in nuevos:
        actualizacion.ingresar(
                        nuevo['codigo'],
                        Estereotipo.get_object(nuevo['estereotipo']),
                        nuevo['vencimiento']
                    )

    actualizacion.completar()

    return JsonResponse({'message': 'Actualización completada!'})


################################################################
#           PROCESAR ACTUALIZACON DE VENTA (SALIDA)
################################################################
@api_view(['POST'])
def procesar_actualizacion_venta(request, *args, **kwargs):
    """
    Funcion usada para la sincronizacion de almancen
    """
    actualizacion_id = request.POST.getlist('actualizacion', None)
    confirmados = request.POST.getlist('confirmados[]')

    if actualizacion_id is None:
        return JsonResponse({'message': 'data incompleta!'})

    confirmados = confirmados.pop()
    actualizacion_id = actualizacion_id.pop()
    nuevos = []

    actualizacion = ActualizacionVenta.objects.get(pk=actualizacion_id)

    print('confirmados', confirmados)

    productos_eliminar = AlmacenQueries.get_productos(
                                    actualizacion.almacen_origen
                                ).exclude(id__in=confirmados)
    actualizacion.retirar(productos_eliminar)

    actualizacion.completar()

    return JsonResponse({'message': 'Actualización completada!'})


class AlmacenList(generics.ListCreateAPIView):
    """
    Lista de todos los medicos
    """
    queryset = Almacen.objects.filter(is_logical=False)
    serializer_class = AlmacenSerializer
