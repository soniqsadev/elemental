angular.module('almacen')
.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$eval(attrs.myEnter+ '("' + element.val() + '")');
                element.val('');
                event.preventDefault();
            }
        });
    };
});
