angular.module('almacen')
.service('HttpLocal', function ($http) {
    this.host = window.location.protocol + '//' + window.location.host;
    this.get = function (path, data) {
        return $http.get(this.host + path, data);
    }
    this.post = function (path, data) {
        return $http.post(this.host + path, data);
    }
})
.factory("MarcaProductoResource", function ($resource) {
    return $resource(window.location.protocol + '//' + window.location.host + '/api/producto/marca/:id/', {id: '@id'}, { update:{method:'patch'} }, { stripTrailingSlashes: false });
})
.factory("CategoriaProductoResource", function ($resource) {
    return $resource(window.location.protocol + '//' + window.location.host + '/api/producto/categoria/:id/', {id: '@id'}, { update:{method:'patch'} }, { stripTrailingSlashes: false });
})
;
