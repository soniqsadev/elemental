angular.module('almacen')
.controller('conciliacionController', function ($scope, $compile, HttpLocal, MarcaProductoResource, CategoriaProductoResource) {

    $scope.estereotipos = [];
    $scope.productos = [];
    $scope.productos_confirmados = [];
    $scope.nuevos_codigos = [];
    $scope.marcas = MarcaProductoResource.query();
    $scope.categorias = CategoriaProductoResource.query();
    $scope.subcategorias = [];
    // Temporal mientras se cargan los productos del almacen
    $scope.almacen_id = $('#almacen_id').val();
    $scope.conciliacion_id = $('#conciliacion_id').val();

    console.log('$scope.conciliacion_id', $scope.conciliacion_id);

    /**
     *  Conseguir la lista de estereotipos
     */
    function actualizar_listados_productos() {
        $.ajax({
            url: '/api/producto/',
        })
        .done(function(res) {
            $scope.$apply(function () {
                $scope.estereotipos = res;
            });
        })
        .fail(function(errors) {
            console.log(errors);
        })
        .always(function() {
            console.log("complete");
        });
    }
    actualizar_listados_productos();

    /**
     *  Función para capturar el codigo ingresado y comprobar si se encuentra
     * entre los registrados en el sistema
     */
    $scope.capturarCodigo = function (codigo) {
        if (codigo.length>0) {
            comprobar_codigo( codigo );
        }
    }

    $scope.eliminar = function (producto) {

        confirmacion = function (res) {
            if (res) {
                i = $scope.productos.indexOf(producto)
                console.log('i', i)
                if(i !== -1){
                    $scope.$apply(function () {
                        $scope.productos.splice(i, 1)
                    });
                    console.log('$scope.productos', $scope.productos);
                }
            } else {

            }
        }

        BootstrapDialog.confirm({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'Advertencia',
                        message: "¿Confirma que deseas eliminar este elemento?",
                        callback: confirmacion
                    });

    }

    $scope.obtener_subcaterias = function () {

        console.log('modal_categoria', $scope.modal_categoria);

        HttpLocal.get('/api/producto/categoria/'+ $scope.modal_categoria +'/subcategorias/')
        .then(function (res) {

            console.log(res);
            $scope.subcategorias = res.data;

        }, function (errors) {

            console.log(errors);

        });

    }
    //$scope.obtener_subcaterias();

    comprobar_codigo = function (codigo) {

        $.ajax({
            url: '/api/almacen/almacen/'+ $scope.almacen_id + '/producto/' + codigo + '/codigo/',
        })
        .done(function(res) {
            producto = res;
            $scope.$apply(function () {
                $scope.productos_confirmados.push(producto);
                $scope.productos.push({
                    data: producto,
                    tipo: 'confirmado'
                })
            });
            console.log(codigo, 'esta en el almacen');
            console.log('productos confirmados', $scope.productos_confirmados);
        })
        .fail(function(errors) {

            $scope.$apply(function () {
                $scope.nuevos_codigos.push( {
                    codigo: codigo,
                    estereotipo: null
                })
                $scope.productos.push({
                    data: {
                        codigo: codigo,
                        estereotipo: null
                    },
                    tipo: 'nuevo'
                })
            });
            console.log(codigo, 'NO esta en el almacen')
            console.log('nuevos codigos', $scope.nuevos_codigos )
        })
        .always(function() {
            console.log("complete");
        });
    }

    function get_data() {
        confirmados = [];
        nuevos = [];

        for (var i = 0; i < $scope.productos.length; i++) {
            if ($scope.productos[i].tipo == 'confirmado'){
                confirmados.push($scope.productos[i].data.id)
            }else{
                year = $scope.productos[i].data.fecha_vencimiento.getFullYear();
                month = $scope.productos[i].data.fecha_vencimiento.getMonth();
                day = $scope.productos[i].data.fecha_vencimiento.getDate();
                nuevos.push({
                    codigo: $scope.productos[i].data.codigo,
                    estereotipo: $scope.productos[i].data.estereotipo.id,
                    vencimiento: year + "-" + month + "-" + day
                })
            }
        };

        return {
            conciliacion: $scope.conciliacion_id,
            confirmados: confirmados,
            nuevos: nuevos,
            nuevoscantidad: nuevos.length,
        };
    };

    /**
     *  Enviar la información al servidor para que sea almacenada
     */
    $scope.save = function () {

        data = get_data()
        console.log(data);

        $.ajax({
            url: '/api/almacen/almacen/'+ $scope.almacen_id +'/productos/conciliacion/',
            type: 'post',
            data: data
        })
        .done(function(res) {
            console.log(res);
            window.location.href = '/almacen/conciliacion/';
        })
        .fail(function(errors) {
            console.log(errors);
        })
        .always(function() {
            console.log("complete");
        });

    }

    /******************************************************************************************
     *             FUNCIONALIDADES DEL MODAL PARA LA CREACIÓN DE NUEVOS PRODUCTOS
     ******************************************************************************************/

     var modal = $('.modal.add_new_producto');

     function get_modal(id) {
         id = id || 'modal';
         modal = '';

         modal += '<div class="modal add_new_producto fade" id="'+ id +'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">';
         modal += '  <div class="modal-dialog modal-lg" role="document">';
         modal += '      <div class="modal-content">';
         modal += '          <div class="modal-header">';
         modal += '              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
         modal += '              <h4 class="modal-title" id="exampleModalLabel">Nuevo producto</h4>';
         modal += '          </div>';
         modal += '          <div class="modal-body">';
         modal += '              <form>';
         modal += '                  <div class="form-group">';
         modal += '                      <label for="nombre" class="control-label">Nombre:</label>';
         modal += '                      <input type="text" class="form-control" id="nombre" ng-model="modal_nombre">';
         modal += '                  </div>';
         modal += '                  <div class="form-group">';
         modal += '                      <label for="marca" class="control-label">Marca:</label>';
         modal += '                      <select type="text" class="form-control" id="marca" ng-model="modal_marca">';
         modal += '                          <option ng-repeat="marca in marcas" value="{{marca.id}}">{{marca.nombre}}</option>';
         modal += '                      </select>';
         modal += '                  </div>';
        //  modal += '                  <hr />';
         modal += '                  <div class="form-group">';
         modal += '                      <label for="categoria" class="control-label">Tipo:</label>';
         modal += '                      <select type="text" class="form-control" id="categoria" ng-model="modal_categoria" ng-click="obtener_subcaterias()">';
         modal += '                          <option ng-repeat="categoria in categorias" value="{{categoria.id}}">{{categoria.nombre}}</option>';
         modal += '                      </select>';
         modal += '                  </div>';
         modal += '                  <div class="form-group">';
         modal += '                      <label for="subcategoria" class="control-label">categoría:</label>';
         modal += '                      <select type="text" class="form-control" id="subcategoria" ng-model="modal_subcategoria">';
         modal += '                          <option ng-repeat="categoria in subcategorias" value="{{categoria.id}}">{{categoria.nombre}}</option>';
         modal += '                      </select>';
         modal += '                  </div>';
         modal += '                  <div class="form-group">';
         modal += '                      <label for="descripcion" class="control-label">Descripción:</label>';
         modal += '                      <textarea class="form-control" id="descripcion" ng-model="modal_descripcion"></textarea>';
         modal += '                  </div>';
         modal += '              </form>';
         modal += '          </div>';
         modal += '          <div class="modal-footer">';
         modal += '              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>';
         modal += '              <button type="button" class="btn btn-primary" id="guardar">Guardar</button>';
         modal += '          </div>';
         modal += '      </div>';
         modal += '  </div>';
         modal += '</div>';

        //  modal = $(modal);
         modal = $compile(modal)($scope)

         $('#guardar', modal).on('click', function (e) {
             salvar_producto();
             modal.modal('hide');
         })

         return modal;

     }

     function get_data_producto() {
         data = {};
         data['nombre'] = $scope.modal_nombre;
         data['descripcion'] = $scope.modal_descripcion;
         data['categoria'] = $scope.modal_categoria;
         data['subcategorias'] = $scope.modal_subcategoria;
         data['marca'] = $scope.modal_marca;

         console.log(data);

         return data;
     }

     function limpiar_campos_modal() {
         $scope.modal_nombre = '';
         $scope.modal_descripcion = '';
         $scope.modal_categoria = '';
         $scope.modal_subcategoria = '';
         $scope.modal_marca = '';
     }


     function salvar_producto() {
         $.ajax({
             url: '/api/producto/',
             type: 'POST',
             data: get_data_producto()
         })
         .done(function() {
             BootstrapDialog.alert({
                 type: BootstrapDialog.TYPE_INFO,
                 title: 'Información',
                 message: "Se ha completado la creación del nuevo producto",
             });
             actualizar_listados_productos();
             console.log("success");
         })
         .fail(function() {
             console.log("error");
         })
         .always(function() {
             console.log("complete");
         });
     }


     $('a.add_producto').on('click', function (e) {
         e.preventDefault();

         // Generación del identificador
         rango = 10000;
         identificador = parseInt( (Math.random() * rango) + 1 );

         // Se comprueba que ya haya sido generado el modal
         if ( modal.length <= 0 ) {
             modal = get_modal('new_producto_'+identificador);

             body = $('body');
             $(body).append( modal );
         }

         // Se hace visible el modal
         modal.modal({
             show:true
         })

     });


})
;
