angular.module('almacen')
.controller('procesarActualizacionVentaController', function ($scope, HttpLocal) {

    $scope.almacen_id = $('#almacen_id').val();
    $scope.cotizacion_id = $('#cotizacion_id').val();
    $scope.actualizacion_id = $('#actualizacion_id').val();
    $scope.productos_cotizacion = [];
    $scope.productos = [];

    /**
     *  Obtiene los productos de la cotizacion
     */
    HttpLocal.get('/api/ventas/producto-venta/cotizacion/'+ $scope.cotizacion_id +'/')
    .then(function (res) {

        $scope.productos_cotizacion = res.data;
        comprobar_cantidades();

    }, function (errors) {

        console.log(errors);

    });

    /**
     *  Se ejecuta cuando el estereotipo de una linea de entrada es
     * cambiada
     */
    $scope.cambio_estereotipo = function (producto) {
        console.log(producto);
        comprobar_cantidades(producto);
    }

    /**
     *  Función llamada por al directiva
     */
    $scope.capturarCodigo = function (codigo) {
        console.log(codigo);
        comprobar_codigo(codigo);
    }

    /**
     *  Comprueba si el codigo se encuentra en la base de datos
     */
    comprobar_codigo = function (codigo) {

        $.ajax({
            url: '/api/almacen/almacen/'+ $scope.almacen_id +'/producto/'+ codigo +'/codigo/',
        })
        .done(function(res) {
            producto = res;

            $scope.$apply(function () {

                $scope.productos.push({
                    data: producto,
                    tipo: 'confirmado'
                })
            });

            comprobar_cantidades();
            console.log(codigo, 'ecomprobar_codigo(codigo);sta en el almacen')
            console.log('productos confirmados', $scope.productos_confirmados)
        })
        .fail(function(errors) {

            BootstrapDialog.alert({
                            type: BootstrapDialog.TYPE_DANGER,
                            title: 'Producto no encontrado',
                            message : "El código '"+ codigo +"' no se encuantra asociado a ningun producto en este almacen",
                        });

            console.log(codigo, 'NO está en el almacen')
            console.log( 'nuevos codigos' , $scope.nuevos_codigos )
        })
        .always(function() {
            console.log("complete");
        });
    }

    /**
     *  Comprueba que las cantidades de los productos se corresponden con las
     * cantidades especificadas en el pedido de compra
     */
    comprobar_cantidades = function () {
        for (var i = 0; i < $scope.productos_cotizacion.length; i++) {
            obtener_cantidad_actual($scope.productos_cotizacion[i]);
        }
    }

    /**
     *  Cantidad por producto de cotización
     */
    obtener_cantidad_actual = function(producto_cotizacion){
        var cantidad_actual = 0;
        for (var i = 0; i < $scope.productos.length; i++) {
            console.log('======', producto_cotizacion.estereotipo.id, $scope.productos[i].data.estereotipo)
            if ( producto_cotizacion.estereotipo.id == $scope.productos[i].data.estereotipo )
                cantidad_actual++;
        }
        producto_cotizacion.cantidad_actual = cantidad_actual;
    }


    function get_data() {
        confirmados = [];

        for (var i = 0; i < $scope.productos.length; i++) {

            confirmados.push($scope.productos[i].data.id)

        };

        return {
            actualizacion: $scope.actualizacion_id,
            confirmados: confirmados,
        };
    };


    /**
     *  Eliminar lineas en la entrada de productos al almacen
     */
    $scope.eliminar = function (producto) {

        confirmacion = function (res) {
            if (res) {
                i = $scope.productos.indexOf(producto)
                if(i !== -1){
                    $scope.$apply(function () {
                        $scope.productos.splice(i, 1)
                    });
                    console.log('$scope.productos', $scope.productos);
                }
            } else {

            }
        }

        BootstrapDialog.confirm({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'Advertencia',
                        message: "¿Confirma que deseas eliminar este elemento?",
                        callback: confirmacion
                    });
    }

    /**
     *
     */
    $scope.save = function () {

        data = get_data();
        console.log(data);

        $.ajax({
            url: '/api/almacen/almacen/'+ $scope.almacen_id +'/actualizacion-venta/salida/',
            type: 'post',
            data: data
        })
        .done(function(res) {
            console.log(res);
            window.location.href = '/almacen/actualizacion/venta/';
        })
        .fail(function(errors) {
            console.log(errors);
        })
        .always(function() {
            console.log("complete");
        });
    }

})
;
