angular.module('almacen')
.controller('procesarActualizacionCompraController', function ($scope, HttpLocal) {

    $scope.almacen_id = $('#almacen_id').val();
    $scope.pedido_id = $('#pedido_id').val();
    $scope.actualizacion_id = $('#actualizacion_id').val();
    $scope.productos_pedido = [];
    $scope.productos = [];

    /**
     *  Obtiene los productos del pedido
     */
    HttpLocal.get('/api/compras/producto-compra/pedido/'+ $scope.pedido_id +'/')
    .then(function (res) {

        $scope.productos_pedido = res.data;
        comprobar_cantidades();

    }, function (errors) {

        console.log(errors);

    });

    /**
     *  Se ejecuta cuando el estereotipo de una linea de entrada es
     * cambiada
     */
    $scope.cambio_estereotipo = function (producto) {
        console.log(producto);
        comprobar_cantidades(producto);
    }

    /**
     *  Función llamada por al directiva
     */
    $scope.capturarCodigo = function (codigo) {
        comprobar_codigo(codigo);
    }

    /**
     *  Comprueba si el codigo se encuentra en la base de datos
     */
    comprobar_codigo = function (codigo) {

        $.ajax({
            url: '/api/almacen/almacen/'+ $scope.almacen_id +'/producto/'+ codigo +'/codigo/',
        })
        .done(function(res) {
            producto = res;
            BootstrapDialog.alert({
                            type: BootstrapDialog.TYPE_DANGER,
                            title: 'Producto registrado',
                            message: "Este código está registrado para un producto del tipo: <br />"+ producto.estereotipo_verbose,
                        });
            // $scope.$apply(function () {
            //     $scope.productos_confirmados.push(producto);
            //     $scope.productos.push({
            //         data: producto,
            //         tipo: 'confirmado'
            //     })
            // });
            // console.log(codigo, 'esta en el almacen')
            // console.log('productos confirmados', $scope.productos_confirmados)
        })
        .fail(function(errors) {

            $scope.$apply(function () {
                $scope.productos.push({
                    data: {
                        codigo: codigo,
                        estereotipo: null
                    },
                    tipo: 'nuevo'
                });
            });
            console.log(codigo, 'NO está en el almacen')
            console.log( 'nuevos codigos' , $scope.nuevos_codigos )
        })
        .always(function() {
            console.log("complete");
        });
    }

    /**
     *  Comprueba que las cantidades de los productos se corresponden con las
     * cantidades especificadas en el pedido de compra
     */
    comprobar_cantidades = function (producto) {
        for (var i = 0; i < $scope.productos_pedido.length; i++) {
            obtener_cantidad_actual($scope.productos_pedido[i]);
        }
    }

    /**
     *
     */
    obtener_cantidad_actual = function(producto_compra){
        var cantidad_actual = 0;
        for (var i = 0; i < $scope.productos.length; i++) {
            if ( producto_compra.estereotipo == $scope.productos[i].data.estereotipo )
                cantidad_actual++;
        }
        producto_compra.cantidad_actual = cantidad_actual;
    }

    /**
     *    Obtener data suministrada por el usuario a traves de los diferentes
     *  controles de la interfaz
     */
    function get_data() {

        nuevos = [];

        for (var i = 0; i < $scope.productos.length; i++) {

            // Registro a ser ingresado en la lista de elementos a ser enviados
            nuevo_registro = {
                codigo: $scope.productos[i].data.codigo,
                estereotipo: $scope.productos[i].data.estereotipo
            }

            // Si el registro tiene fecha de vencimiento se agrega al nuevo registro
            if ( typeof($scope.productos[i].data.fecha_vencimiento) !== 'undefined' &&  $scope.productos[i].data.fecha_vencimiento !== null ) {
                year = $scope.productos[i].data.fecha_vencimiento.getFullYear();
                month = $scope.productos[i].data.fecha_vencimiento.getMonth();
                day = $scope.productos[i].data.fecha_vencimiento.getDate();

                nuevo_registro.vencimiento = year + "-" + month + "-" + day;
            }

            // Si el registro tiene ubicacion se agrega al nuevo registro
            if ( typeof($scope.productos[i].data.ubicacion) !== 'undefined' && $scope.productos[i].data.ubicacion !== null ) {
                nuevo_registro.ubicacion = $scope.producto[i].data.ubicacion
            }

            // El registro es agregado a la lista de elementos
            nuevos.push(nuevo_registro);
        };


        return {
            actualizacion: $scope.actualizacion_id,
            nuevos: nuevos,
            nuevoscantidad: nuevos.length,
        };
    };

    /**
     *  Eliminar lineas en la entrada de productos al almacen
     */
    $scope.eliminar = function (producto) {

        confirmacion = function (res) {
            if (res) {
                i = $scope.productos.indexOf(producto)
                if(i !== -1){
                    $scope.$apply(function () {
                        $scope.productos.splice(i, 1)
                    });
                    console.log('$scope.productos', $scope.productos);
                }
            } else {

            }
        }

        BootstrapDialog.confirm({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'Advertencia',
                        message: "¿Confirma que deseas eliminar este elemento?",
                        callback: confirmacion
                    });

    }

    /**
     *
     */
    $scope.save = function () {

        data = get_data();
        console.log(data);

        $.ajax({
            url: '/api/almacen/almacen/'+ $scope.almacen_id +'/actualizacion-compra/entrada/',
            type: 'post',
            data: data
        })
        .done(function(res) {
            console.log(res);
            window.location.href = '/almacen/actualizacion/compra/';
        })
        .fail(function(errors) {
            console.log(errors);
        })
        .always(function() {
            console.log("complete");
        });
    }

})
;
