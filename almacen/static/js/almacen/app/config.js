angular.module('almacen', ['ngResource', 'ngRoute', 'localytics.directives'])
.config(function ($routeProvider) {
    $routeProvider
    .when('/', {
        controller: "indexController",
        templateUrl: "/static/js/almacen/conciliacion/app/templates/list.html"
    })
    .when('/:id', {
        controller: "detailController",
        templateUrl: "/static/js/almacen/conciliacion/app/templates/detail.html"
    })
    .otherwise({
            redirectTo: '/'
        });
})
.filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});
;
