from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from ..models import Movimiento


# @receiver(post_save, sender=RequisicionCompra)
# def requisicioncompra_post_save(sender, **kwargs):
#
#     for postulacion in instance.postulaciones.all():
#         postulacion.add_precios_proveedor(
#                     postulacion.proveedor
#                 )
#
#
@receiver(post_save, sender=Movimiento)
def movimiento_post_save(sender, **kwargs):

    producto = instance.producto
    producto.almacen = instance.almacen_destino
    producto.save()
