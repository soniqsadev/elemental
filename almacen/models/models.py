import datetime

from django.db import models

from core.models import Producto
from division_territorial.models import Estado, Ciudad
#from ventas.models import OrdenVenta
#from compras.models import PedidoCompra


class Almacen(models.Model):
    """
    Modelo de almacén, local en el cual se guardan los productos para su posterior venta
    """
    descripcion = models.CharField(max_length=100, verbose_name='Descripción')
    estado = models.ForeignKey(Estado, related_name='almacenes')
    ciudad = models.ForeignKey(Ciudad, related_name='almacenes', verbose_name='Delegación', blank=True, null=True)
    direccion = models.TextField(blank=True, null=True, verbose_name='Dirección')
    is_logical = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Almacen'
        verbose_name_plural = 'Almacenes'

    def __str__(self):
        return self.descripcion


class Ubicacion(models.Model):
    """
    Divisiones internas de los almacenes
    """
    descripcion = models.CharField(max_length=100)
    almacen = models.ForeignKey(Almacen, related_name='ubicaciones')
    is_logical = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Ubicacion'
        verbose_name_plural = 'Ubicaciones'

    def __str__(self):
        self.descripcion


class Actualizacion(models.Model):
    """
    [Clase madre]
    Cambios que se producen en el invenario de productos
    """
    ESTADOS = [
        ('porrealizarse', 'Por realizarse'),
        ('enproceso', 'En proceso'),
        ('completado', 'Completado'),
        ('cancelado', 'Cancelado'),
    ]
    TIPOS = [
        ('interna', 'Interna'),
        ('entrada', 'Entrada'),
        ('salida', 'Salida')
    ]
    MOTIVOS = []
    status = models.CharField(max_length=100, choices=ESTADOS, default='porrealizarse')
    tipo = models.CharField(choices=TIPOS, max_length=32)
    motivo = models.CharField(choices=MOTIVOS, max_length=32)
    descripcion = models.CharField(blank=True, max_length=100)
    almacen_origen = models.ForeignKey(Almacen, related_name='actualizaciones_salida')
    almacen_destino = models.ForeignKey(Almacen, related_name='actualizaciones_entrada')
    fecha_inicio = models.DateTimeField(blank=True, default=datetime.datetime.now)
    fecha_fin = models.DateTimeField(null=True, blank=True)
    activa = models.BooleanField(default=True)
    fecha_creacion = models.DateTimeField(blank=True, auto_now_add=True)
    fecha_modificacion = models.DateTimeField(blank=True, auto_now=True)

    def verbose_status(self):
        for estado in self.ESTADOS:
            if estado[0] == self.status:
                return estado[1]
        return None

    def verbose_tipo(self):
        for tipo in self.TIPOS:
            if tipo[0] == self.tipo:
                return tipo[1]
        return None

    def verbose_motivo(self):
        for motivo in self.MOTIVOS:
            if motivo[0] == self.motivo:
                return motivo[1]
        return None

    def iniciar(self):
        if self.status == 'porrealizarse':
            self.status = 'enproceso'
            self.save()

    def completar(self):
        self.status = 'completado'
        self.save()
        self.actualizar_almacen_productos()

    def cancelar(self):
        self.status = 'cancelado'
        self.save()

    def actualizar_almacen_productos(self):
        """
        Actualiza el campo almacen en los productos de la actualización
        """
        for movimiento in self.movimientos.all():
            producto = movimiento.producto
            producto.almacen = movimiento.almacen_destino
            producto.save()

    def retirar(self, productos):
        """
        Sacar productos del almacen
        """
        print('retirar', productos)
        for producto in productos:
            Movimiento(actualizacion=self,
                       producto=producto,
                       almacen_origen=self.almacen_origen,
                       almacen_destino=Almacen.objects.get(pk=2)
                ).save()


    def ingresar(self, codigo, estereotipo, fecha_vencimiento=None):
        """
        Ingresar un producto en el almacen
        """
        print('ingresar', codigo, estereotipo, fecha_vencimiento)
        producto = Producto(codigo=codigo,
                            estereotipo=estereotipo,
                            fecha_vencimiento=fecha_vencimiento,
                            almacen=self.almacen_destino
                            )
        producto.save()
        print('producto_id', producto.id)
        Movimiento(actualizacion=self,
                   producto=producto,
                   almacen_origen=Almacen.objects.get(pk=1),
                   almacen_destino=self.almacen_destino
                   ).save()

    def __str__(self):
        return "[%s] %s => %s (%s)" % (self.verbose_tipo(), self.almacen_origen, self.almacen_destino, self.verbose_motivo())


class ActualizacionCompra(Actualizacion):
    """
    Cambios en el inventario de productos por operaciones de compra
    """
    MOTIVOS = [
        ('recepcionproductos', 'Recepción de productos'),
        ('devolucionproductos', 'Devolución de productos')
    ]
    pedido = models.ForeignKey('compras.PedidoCompra')
    is_parcial = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Actualización de venta'
        verbose_name_plural = 'Actualizaciones de venta'


class ActualizacionVenta(Actualizacion):
    """
    Cambios en el inventario de productos por operaciones de venta
    """
    MOTIVOS = [
        ('entregaproductos', 'Entrega de productos'),
        ('excedente', 'Excente de la venta'),
        ('devolucionproductos', 'Devolución de productos'),
    ]
    orden = models.ForeignKey('ventas.OrdenVenta')

    class Meta:
        verbose_name = 'Actualización de compra'
        verbose_name_plural = 'Actualizaciones de compra'


class ActualizacionInterna(Actualizacion):
    """
    Cambios en el inventario por motivos internos de la empresa
    """
    MOTIVOS = [
        ('conciliacion', 'Conciliación de inventario'),
        ('inventario', 'Revisión de inventario'),
        ('solicitudagencia', 'Solicitud de otra agencia'),
    ]

    class Meta:
        verbose_name = 'Actualización interna'
        verbose_name_plural = 'Actualizacines interanas'


class Movimiento(models.Model):
    """
    Cambio en un producto con respecto al almacén
    """
    MOTIVOS_DESCARTE = [
        ('vencido', 'El producto está vencido'),
        ('danado', 'El producto está dañado')
    ]
    actualizacion = models.ForeignKey(Actualizacion, related_name='movimientos')
    producto = models.ForeignKey(Producto, related_name='movimientos')
    motivo_descarte = models.CharField(blank=True, null=True, choices=MOTIVOS_DESCARTE, max_length=32)
    almacen_origen = models.ForeignKey(Almacen, related_name='movimientos_salida')
    ubicacion_origen = models.ForeignKey(Ubicacion, related_name='movimientos_salida', null=True, blank=True)
    almacen_destino = models.ForeignKey(Almacen, related_name='movimientos_entrada')
    ubicacion_destino = models.ForeignKey(Ubicacion, related_name='movimientos_entrada', null=True, blank=True)
    is_material_adicional = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Movimiento'
        verbose_name_plural = 'Movimientos'

    def __str__(self):
        return "%s:%s => %s:%s (%s)" % (self.almacen_origen, self.ubicacion_origen, self.almacen_destino, ubicacion_destino, self.producto)
