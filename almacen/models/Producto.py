from django.db.models import Q, Sum, Count
from core.models import EstereotipoProducto as CoreProducto, Producto as ProductoFisico
from .models import Movimiento, ActualizacionCompra, ActualizacionVenta, Almacen


class Estereotipo(object):
    """ Clase para consultas de los productos presentes en el almacen """
    @staticmethod
    def get_productos():
        lista = []
        for producto in CoreProducto.objects.all():
            lista.append({
                  'id': producto.id,
                  'nombre': producto.nombre,
                  'categoria': producto.categoria,
                  'marca': producto.marca,
                  'medidas': producto.medidas,
                  'existencia_fisica': Estereotipo.existencia_fisica(producto),
                  'existencia_real': Estereotipo.existencia_real(producto),
                  'existencia_virtual': Estereotipo.existencia_virtual(producto)
                })
        return lista

    @staticmethod
    def get_object(producto_id):
        """
        Obtener el objeto producto a partir de su id
        """
        return CoreProducto.objects.get(pk=producto_id)

    @staticmethod
    def existencia_fisica(estereotipo):
        """
        Lo que hay en el almacén
        """
        return Estereotipo.ingresados(estereotipo) - Estereotipo.enviados(estereotipo)

    @staticmethod
    def existencia_real(estereotipo):
        """
        Lo que hay en el almacén menos lo que ya está comprometido
        """
        return Estereotipo.existencia_fisica(estereotipo) - Estereotipo.por_enviar(estereotipo)

    @staticmethod
    def existencia_virtual(estereotipo):
        """
        Lo que hay en el almacén + lo que habrá
        """
        #return Producto.existencia_real(estereotipo) + Producto.por_ingresar(estereotipo)
        return Estereotipo.por_ingresar(estereotipo)


    # ******************************************************************* #
    #                    Busquedas a partir de estereotipo
    # ******************************************************************* #

    @staticmethod
    def enviados(estereotipo, almacen=None):
        """
        Todas las salidas de productos del almacen
        """
        if almacen == None:
            return Movimiento.objects.filter(producto__estereotipo=estereotipo,
                                             actualizacion__almacen_origen__is_logical=False,
                                             actualizacion__almacen_destino__is_logical=True
                                    ).count()
        else:
            return Movimiento.objects.filter(producto__estereotipo=estereotipo,
                                             actualizacion__almacen_origen=almacen,
                                             actualizacion__almacen_origen__is_logical=False,
                                             actualizacion__almacen_destino__is_logical=True
                                    ).count()

    @staticmethod
    def ingresados(estereotipo, almacen=None):
        """
        Todas los ingresos de productos del almacen
        """
        if almacen == None:
            return Movimiento.objects.filter(producto__estereotipo=estereotipo,
                                             actualizacion__almacen_origen__is_logical=True,
                                             actualizacion__almacen_destino__is_logical=False
                                        ).count()
        else:
            return Movimiento.objects.filter(producto__estereotipo=estereotipo,
                                             actualizacion__almacen_destino=almacen,
                                             actualizacion__almacen_origen__is_logical=True,
                                             actualizacion__almacen_destino__is_logical=False
                                        ).count()

    @staticmethod
    def por_enviar(estereotipo, almacen=None):
        """
        Productos comprometidos para ser enviados
        """
        # result = None
        if almacen == None:
            result_dict = ActualizacionVenta.objects.filter(orden__productos_ventas__estereotipo=estereotipo,
                                                            status='porrealizarse'
                                                    ).aggregate( Sum('orden__productos_ventas__cantidad') )
        else:
            result_dict = ActualizacionVenta.objects.filter(orden__productos_ventas__estereotipo=estereotipo,
                                                            almacen_origen=almacen,
                                                            almacen_destino__is_logical=True,
                                                            status='porrealizarse'
                                                    ).aggregate( Sum('orden__productos_ventas__cantidad') )
        result = result_dict.get('orden__productos_ventas__cantidad__sum', 0)
        if result == None:
            return 0
        return result

    @staticmethod
    def por_ingresar(estereotipo, almacen=None):
        """
        Productos pendientes por ingresar
        """
        # result = None
        if almacen == None:
            result_dict = ActualizacionCompra.objects.filter(pedido__productos_compras__estereotipo=estereotipo,
                                                             status='porrealizarse',
                                                        ).aggregate(Sum('pedido__productos_compras__cantidad'))
        else:
            result_dict = ActualizacionCompra.objects.filter(pedido__productos_compras__estereotipo=estereotipo,
                                                             almacen_origen__is_logical=True,
                                                             almacen_destino=almacen,
                                                             status='porrealizarse',
                                                        ).aggregate(Sum('pedido__productos_compras__cantidad'))
        result = result_dict.get('pedido__productos_compras__cantidad__sum', 0)
        if result == None:
            return 0
        return result


class Producto(object):
        # ******************************************************************* #
        #              Busquedas a partir de codigo de producto
        # ******************************************************************* #


    @staticmethod
    def get_object(codigo):
        return Producto.objects.get(codigo=codigo)


    @staticmethod
    def existencia_fisica(codigo):
        """
        Lo que hay en el almacén
        """
        total = Producto.ingresados(codigo) - Producto.enviados(codigo)
        if total > 0:
            return Producto.get_object(codigo)
        return False

    @staticmethod
    def enviados(codigo):
        """
        Todas las salidas de productos del almacen
        """
        return Movimiento.objects.filter(producto=Producto.get_object(codigo), actualizacion__almacen_origen__is_logical=False, actualizacion__almacen_destino__is_logical=True).count()

    @staticmethod
    def ingresados(codigo):
        """
        Todas los ingresos de productos del almacen
        """
        return Movimiento.objects.filter(producto=Producto.get_object(codigo), actualizacion__almacen_origen__is_logical=True, actualizacion__almacen_destino__is_logical=False).count()


class AlmacenQueries(object):
    @staticmethod
    def get_object(id):
        return Almacen.objects.get(pk=id)

    @staticmethod
    def get_productos(almacen):
        print('fue consultado get_productos')
        # productos = []
        #
        # productos_ingresados = ProductoFisico.objects.filter(movimientos__almacen_origen__is_logical=True, movimientos__almacen_destino=almacen)
        #
        # # productos = [producto_ingresado if producto_ingresado.movimientos.filter(almacen_origen__is_logical=True, almacen_destino=almacen).count() > producto_ingresado.movimientos.filter(almacen_origen=almacen, almacen_destino__is_logical=True).count() else None for producto_ingresado in productos_ingresados]
        #
        # for producto_ingresado in productos_ingresados:
        #     entradas = producto_ingresado.movimientos.filter(almacen_origen__is_logical=True, almacen_destino=almacen).count()
        #     salidas = producto_ingresado.movimientos.filter(almacen_origen=almacen, almacen_destino__is_logical=True).count()
        #     if entradas > salidas:
        #         producto_ingresado.estereotipo_verbose = producto_ingresado.estereotipo.nombre
        #         productos.append(producto_ingresado)
        productos = ProductoFisico.objects.filter(almacen=almacen)
        print('termino la consulta')
        return productos

    @staticmethod
    def is_in_almacen(almacen, codigo):
        try:
            producto = ProductoFisico.objects.get(almacen=almacen, codigo=codigo)
            return producto
        except ProductoFisico.DoesNotExist:
            return False
