from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404

from django.views.generic import View
from django.views.generic.list import ListView

from ..models import Estereotipo
from core.models import Producto as CoreProducto, EstereotipoProducto as EstereotipoCore


class ListView(ListView):
    context_object_name = 'productos'
    model = CoreProducto
    queryset = Estereotipo.get_productos()
    template_name = 'almacen/producto/list.html'


class IndividualView(View):
    template_name = 'almacen/producto/list_individual.html'

    def get(self, *args, **kwargs):
        estereotipo = get_object_or_404(EstereotipoCore, pk=kwargs['pk'])
        return render(self.request, self.template_name, {'estereotipo': estereotipo})
