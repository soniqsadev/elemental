from django.db.models import Avg, Max, Min, Sum
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View
from django.views.generic.list import ListView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from django.forms import modelformset_factory
from django.forms.formsets import formset_factory

from ..models import Actualizacion, ActualizacionCompra, ActualizacionVenta, ActualizacionInterna, Movimiento
from ..forms.Producto import ProductoForm
from core.models import Producto

class ListView(ListView):

    # @method_decorator(login_required)
    # def dispatch(self, *args, **kwargs):
    #     return super(ListView, self).dispatch(*args, **kwargs)

    context_object_name = 'actualizaciones'
    model = Actualizacion
    page_kwarg = 'page'
    paginate_by = 10
    template_name = 'almacen/actualizacion/list.html'


class ListCompraView(ListView):
    context_object_name = 'actualizaciones'
    model = ActualizacionCompra
    page_kwarg = 'page'
    paginate_by = 10
    template_name = 'almacen/actualizacion/list_compra.html'


class ListVentaView(ListView):
    context_object_name = 'actualizaciones'
    model = ActualizacionVenta
    page_kwarg = 'page'
    paginate_by = 10
    template_name = 'almacen/actualizacion/list_venta.html'


class ListInternaView(ListView):
    context_object_name = 'actualizaciones'
    model = ActualizacionInterna
    page_kwarg = 'page'
    paginate_by = 10
    template_name = 'almacen/actualizacion/list_interna.html'


#########################################################################
#             Vista para procesar actualizaciones de compra TEST
#########################################################################
class ProcesarCompraNewView(View):
    template_name = 'almacen/actualizacion/procesar_compra_new.html'
    success_url = reverse_lazy('actualizacion.compra.list')

    def get(self, *args, **kwargs):
        actualizacion = get_object_or_404(ActualizacionCompra,
                                          pk=kwargs.get('pk'))
        print("actualizacion.id", actualizacion.id)
        print("actualizacion.almacen_destino", actualizacion.almacen_destino)
        return render(self.request, self.template_name,
                      {'pedido': actualizacion.pedido,
                       'almacen': actualizacion.almacen_destino,
                       'actualizacion': actualizacion
                       })


#########################################################################
#             Vista para procesar actualizaciones de compra
#########################################################################

class ProcesarCompraView(View):
    template_name = 'almacen/actualizacion/procesar_compra.html'
    success_url = reverse_lazy('actualizacion.compra.list')

    def create_modelformset(self, cantidad):
        print(cantidad)
        return modelformset_factory(Producto
                                    , can_delete=False
                                    , max_num=cantidad
                                    , min_num=cantidad
                                    , validate_max=True
                                    , fields= ('codigo',)
                                )

    def get(self, *args, **kwargs):
        actualizacion = get_object_or_404(ActualizacionCompra, pk=kwargs.get('pk'))

        # El procesamiento de la actualizacion solo se llevará a cabo si el estados es 'porrealizarse'
        if actualizacion.status != 'porrealizarse':
            return redirect(self.success_url)

        ProductoFormSet = self.create_modelformset( actualizacion.pedido.cantidad() )
        formset = ProductoFormSet(queryset=Producto.objects.none())

        return render(self.request, self.template_name, {'formset':formset, 'instance':actualizacion})

    def post(self, *args, **kwargs):
        actualizacion = get_object_or_404(ActualizacionCompra, pk=kwargs.get('pk'))

        # El procesamiento de la actualizacion solo se llevará a cabo si el estados es 'porrealizarse'
        if actualizacion.status != 'porrealizarse':
            return redirect(self.success_url)

        ProductoFormSet = self.create_modelformset( actualizacion.pedido.cantidad() )
        formset = ProductoFormSet(self.request.POST)

        print(len(formset))
        count_productoscompras = 0
        count_formsets = 0
        if formset.is_valid():

            # for producto_compra in actualizacion.pedido.productos_compras.all():
            #     for n in range(1, producto_compra.cantidad ):
            #         print('index', count_formsets)
            #         formset[count_formsets].instance.estereotipo = producto_compra.estereotipo
            #         formset[count_formsets].instance.precio_compra = producto_compra.precio_unidad
            #         formset[count_formsets].save()
            #         count_formsets = count_formsets + 1
            #         Movimiento.objects.create(**{
            #             'actualizacion':actualizacion,
            #             'producto': formset[count_formsets].instance,
            #             'almacen_origen': actualizacion.almacen_origen,
            #             'almacen_destino': actualizacion.almacen_destino
            #         })

            productos_individuales = []

            for producto_compra in actualizacion.pedido.productos_compras.all():
                for n in range(1, producto_compra.cantidad + 1):
                    productos_individuales.append({
                        'estereotipo': producto_compra.estereotipo,
                        'precio_compra': producto_compra.precio_unidad
                    })

            print('cantidad de productos individuales', len(productos_individuales))


            for productoform in formset:
                # print('index', count_productoscompras)
                productoform.instance.estereotipo = productos_individuales[count_productoscompras]['estereotipo']
                productoform.instance.precio_compra = productos_individuales[count_productoscompras]['precio_compra']
                productoform.save()
                Movimiento.objects.create(**{
                    'actualizacion':actualizacion,
                    'producto': productoform.instance,
                    'almacen_origen': actualizacion.almacen_origen,
                    'almacen_destino': actualizacion.almacen_destino
                })
                count_productoscompras = count_productoscompras + 1

            actualizacion.completar() # Se completa la actualizacion
            actualizacion.pedido.productos_entregados()
            return redirect(self.success_url)

        return render(self.request,
                      self.template_name,
                      {'formset': formset, 'instance': actualizacion}
                      )


#########################################################################
#             Vista para procesar actualizaciones de venta TEST
#########################################################################
class ProcesarVentaNewView(View):
    template_name = 'almacen/actualizacion/procesar_venta_new.html'
    success_url = reverse_lazy('actualizacion.venta.list')

    def get(self, *args, **kwargs):
        actualizacion = get_object_or_404(ActualizacionVenta,
                                          pk=kwargs.get('pk'))
        print("actualizacion.id", actualizacion.id)
        print("actualizacion.almacen_origen", actualizacion.almacen_origen)
        return render(self.request, self.template_name,
                      {'cotizacion': actualizacion.orden,
                       'almacen': actualizacion.almacen_origen,
                       'actualizacion': actualizacion
                       })


#########################################################################
#             Vista para procesar actualizaciones de venta
#########################################################################
class ProcesarVentaView(View):
    template_name = 'almacen/actualizacion/procesar_venta.html'
    success_url = reverse_lazy('actualizacion.venta.list')

    def create_modelformset(self, cantidad):
        return formset_factory(ProductoForm, can_delete=False, max_num=cantidad, min_num=cantidad, validate_max=True)

    def get(self, *args, **kwargs):
        actualizacion = get_object_or_404(ActualizacionVenta, pk=kwargs.get('pk'))

        #   El procesamiento de la actualizacion solo se llevará a cabo
        # si el estado es 'porrealizarse'
        if actualizacion.status != 'porrealizarse':
            return redirect(self.success_url)

        ProductoFormSet = self.create_modelformset(
                                    actualizacion.orden.productos_ventas.filter(
                                        incluido=True
                                    ).aggregate(Sum('cantidad'))['cantidad__sum']
                        )

        initial = []
        for producto_venta in actualizacion.orden.productos_ventas.filter(incluido=True):
            for n in range(0, producto_venta.cantidad):
                initial.append({'estereotipo': producto_venta.estereotipo})
        productos_form = ProductoFormSet(initial=initial)

        return render(self.request, self.template_name, {'formset':productos_form, 'instance': actualizacion.orden})

    def post(self, *args, **kwargs):
        actualizacion = get_object_or_404(ActualizacionVenta, pk=kwargs.get('pk'))

        # El procesamiento de la actualizacion solo se llevará a cabo si el estado es 'porrealizarse'
        if actualizacion.status != 'porrealizarse':
            return redirect(self.success_url)

        ProductoFormSet = self.create_modelformset( actualizacion.orden.productos.all().aggregate(Sum('cantidad'))['cantidad__sum'] )
        productos_form = ProductoFormSet(self.request.POST)

        if productos_form.is_valid():
            actualizacion.completar()  # Se completa la actualizacion
            for producto_form in productos_form:
                producto_form.save()
                Movimiento.objects.create(**{
                    'actualizacion':actualizacion,
                    'producto': producto_form.instance,
                    'almacen_origen': actualizacion.almacen_origen,
                    'almacen_destino': actualizacion.almacen_destino
                })
                actualizacion.orden.productos_entregados()
                return redirect(self.success_url)

        return render(self.request, self.template_name, {'formset':productos_form, 'instance': actualizacion.orden})
