from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from ..models import ActualizacionInterna
from ..forms import Conciliacion

SUCCESS_URL = reverse_lazy('almacen.list')

class ListView(ListView):
    context_object_name = 'conciliaciones'
    model = ActualizacionInterna
    template_name = 'almacen/conciliacion/list.html'


class CreateView(View):
    template_name = 'almacen/conciliacion/create.html'
    success_url = SUCCESS_URL

    def get(self, *args, **kwargs):
        form = Conciliacion.ConciliacionForm()
        return render(self.request, self.template_name, {'form':form})

    def post(self, *args, **kwargs):
        form = Conciliacion.ConciliacionForm(self.request.POST)
        if form.is_valid():
            data = form.cleaned_data
            actualizacion = ActualizacionInterna.objects.create(tipo='interna',
                                                                motivo='conciliacion',
                                                                fecha_inicio=data['fecha'],
                                                                almacen_origen=data['almacen'],
                                                                almacen_destino=data['almacen'])
            return redirect(reverse_lazy('almacen.conciliacion.update', args=[actualizacion.id,]))

        return render(self.request, self.template_name, {'form':form})


class DetailView(DetailView):
    model = ActualizacionInterna
    context_object_name = 'conciliacion'
    success_url = SUCCESS_URL
    template_name = 'almacen/conciliacion/detail.html'


class UpdateView(View):
    success_url = SUCCESS_URL
    template_name = 'almacen/conciliacion/edit.html'

    def get(self, *args, **kwargs):
        conciliacion = get_object_or_404(ActualizacionInterna, pk=kwargs['pk'])
        almacen = conciliacion.almacen_destino
        return render(self.request, self.template_name, {'almacen':almacen, 'conciliacion':conciliacion})

    def post(self, *args, **kwargs):
        return render(self.request, self.template_name)


class DeleteView(DeleteView):
    success_url = SUCCESS_URL
    model = ActualizacionInterna
    template_name = 'almacen/conciliacion/delete.html'
