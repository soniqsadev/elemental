from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import ValidationError
from django.shortcuts import render, redirect, get_object_or_404

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from django.views.generic import View
from django.views.generic.list import ListView
from django.views.generic.edit import DeleteView
from django.views.generic.detail import DetailView

from django.forms.formsets import formset_factory
from django.forms import modelformset_factory

from ..models import Almacen, Ubicacion
from ..forms.Almacen import AlmacenForm
from ..forms.Ubicacion import UbicacionForm


SUCCESS_URL = reverse_lazy('almacen.list')

def permissions_show(user):
    return user.has_perm('almacen.can_add_almacen') or user.has_perm('almacen.can_change_almacen') or user.has_perm('almacen.can_delete_almacen')

class ListView(ListView):
    context_object_name = 'almacenes'
    model = Almacen
    queryset = Almacen.objects.filter(is_logical=False)
    page_kwarg = 'page'
    paginate_by = 10
    template_name = 'almacen/almacen/list.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(ListView, self).dispatch(*args, **kwargs)

class CreateView(View):

    template_name = 'almacen/almacen/create.html'

    def __init__(self, *args, **kwargs):
        super(CreateView, self).__init__(*args, **kwargs)
        self.UbicacionFormSet = modelformset_factory(Ubicacion,
                                                             can_delete=True,
                                                             max_num=20,
                                                             min_num=0,
                                                             validate_max=True,
                                                             fields= ('descripcion',)
                                                        )

    def get(self, request):
        form = AlmacenForm()
        form.ubicacion_form = self.UbicacionFormSet(queryset=Ubicacion.objects.none())
        return render(request, self.template_name, {'form':form})

    def post(self, request):
        form = AlmacenForm(request.POST)
        ubicaciones = self.UbicacionFormSet(request.POST)
        if form.is_valid() and ubicaciones.is_valid():
            form.save()
            for ubicacion in ubicaciones:
                ubicacion.instance.almacen = form.instance
            ubicaciones.save()
            return redirect(SUCCESS_URL)

        form.ubicacion_form = ubicaciones
        return render(request, self.template_name, {'form': form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('almacen.can_add_almacen'))
    def dispatch(self, *args, **kwargs):
        return super(CreateView, self).dispatch(*args, **kwargs)


class DetailView(DetailView):
    model = Almacen
    context_object_name = 'almacen'
    template_name = 'almacen/almacen/show.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(DetailView, self).dispatch(*args, **kwargs)

class UpdateView(View):
    template_name = 'almacen/almacen/create.html'

    def __init__(self, *args, **kwargs):
        super(UpdateView, self).__init__(*args, **kwargs)
        self.UbicacionFormSet = modelformset_factory(Ubicacion,
                                                             can_delete=True,
                                                             max_num=20,
                                                             validate_max=True,
                                                             fields= ('descripcion',)
                                                        )

    def get(self, request, pk):
        almacen = get_object_or_404(Almacen, pk=pk)
        form = AlmacenForm(instance=almacen)
        form.ubicacion_form = self.UbicacionFormSet(queryset=almacen.ubicaciones.all())
        return render(request, self.template_name, {'form':form})

    def post(self, request, pk):
        almacen = get_object_or_404(Almacen, pk=pk)
        form = AlmacenForm(request.POST, instance=almacen)
        ubicaciones = self.UbicacionFormSet(request.POST)
        for ubicacion in ubicaciones:
            ubicacion.instance.almacen = almacen
        if form.is_valid() and ubicaciones.is_valid():
            form.save()
            ubicaciones.save()
            return redirect(SUCCESS_URL)

        form.ubicacion_form = ubicaciones
        return render(request, self.template_name, {'form': form})

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('almacen.can_change_almacen'))
    def dispatch(self, *args, **kwargs):
        return super(UpdateView, self).dispatch(*args, **kwargs)


class DeleteView(DeleteView):
    model = Almacen
    context_object_name = 'almacen'
    success_url = SUCCESS_URL
    template_name = 'almacen/almacen/delete.html'

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('almacen.can_delete_almacen'))
    def dispatch(self, *args, **kwargs):
        return super(DeleteView, self).dispatch(*args, **kwargs)
