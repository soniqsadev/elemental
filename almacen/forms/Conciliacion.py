import datetime
from django import forms
from django.utils import timezone

from ..models import Almacen

class ConciliacionForm(forms.Form):
    almacen = forms.ModelChoiceField(queryset=Almacen.objects.filter(is_logical=False))
    fecha = forms.DateTimeField(initial=timezone.now())

    def __init__(self, *args, **kwargs):
        super(ConciliacionForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(ConciliacionForm, self).clean()
        return cleaned_data

    def save(self):
        cleaned_data = super(ConciliacionForm, self).clean()
