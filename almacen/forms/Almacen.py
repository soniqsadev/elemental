from django.forms import ModelForm
from division_territorial.forms import CiudadField, EstadoField

from ..models import Almacen

class AlmacenForm(ModelForm):
    ciudad = CiudadField()
    estado = EstadoField()

    class Meta:
        model = Almacen
        fields = '__all__'
