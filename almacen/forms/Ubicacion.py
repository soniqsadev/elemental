from django.forms import ModelForm
from ..models import Ubicacion

class UbicacionForm(ModelForm):
    # TODO: Define other fields here

    class Meta:
        model = Ubicacion
        exclude = ['almacen',]

    def __init__(self, *args, **kwargs):
        super(UbicacionForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(UbicacionForm, self).clean()
        return cleaned_data
