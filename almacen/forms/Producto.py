from django.forms import ModelForm
from core.models import Producto

class ProductoForm(ModelForm):
    # TODO: Define other fields here

    class Meta:
        model = Producto
        fields = ['codigo','estereotipo']

    def __init__(self, *args, **kwargs):
        super(ProductoForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(ProductoForm, self).clean()
        return cleaned_data

    def get_estereotipo_name(self):
        return self.initial['estereotipo']
