Menu = [
    {
        "id": "1",
        "title": "Almacén",
        "url": "/almacen",
        "icon": None,
        "help": "",
        "secondary":[
            {
                "title": "Actualizaciones",
                "url": "/almacen/actualizacion",
                "icon": None,
                "help": "",
                "secondary":[
                    {
                        "title": "Compra",
                        "url": "/almacen/actualizacion/compra",
                        "icon": None,
                        "help": "",
                    },
                    {
                        "title": "Venta",
                        "url": "/almacen/actualizacion/venta",
                        "icon": None,
                        "help": "",
                    },
                    {
                        "title": "interna",
                        "url": "/almacen/actualizacion/interna",
                        "icon": None,
                        "help": "",
                    }
                ]
            },
            {
                "title": "Almacenes",
                "url": "/almacen/almacen",
                "icon": None,
                "help": "",
            },
            {
                "title": "Conciliaciones",
                "url": "/almacen/conciliacion",
                "icon": None,
                "help": "",
            },
            {
                "title": "Productos",
                "url": "/almacen/producto",
                "icon": None,
                "help": "",
            }
        ]
    },
    {
        "id": "2",
        "title": "Compras",
        "url": "/compras",
        "icon": None,
        "help": "",
        "secondary":[
            {
                "title": "Pedidos de compra",
                "url": "/compras/pedido",
                "icon": None,
                "help": "",
            },
            {
                "title": "Productos por comprar",
                "url": "/compras/pedido/atender-productos",
                "icon": None,
                "help": "",
            },
            {
                "title": "Requisición",
                "url": "/compras/requisicion",
                "icon": None,
                "help": "",
            },
            {
                "title": "Proveedores",
                "url": "/compras/proveedor",
                "icon": None,
                "help": "",
            }
        ]
    },
    {
        "id": "3",
        "title": "Ventas",
        "url": "/ventas",
        "icon": None,
        "help": "",
        "secondary":[
            {
                "title": "Cotizaciones",
                "url": "/ventas/orden",
                "icon": None,
                "help": "",
            },
            {
                "title": "Clientes",
                "url": "/ventas/cliente",
                "icon": None,
                "help": "",
            },
            {
                "title": 'Hospitales',
                "url": "/ventas/hospital",
                "icon": None,
                "help": "",
            },
            {
                "title": 'Médicos',
                "url": "/ventas/medico",
                "icon": None,
                "help": "",
                "secondary":[
                    {
                        "title": 'Especialidades',
                        "url": "/ventas/medico/especialidad",
                        "icon": None,
                        "help": "",
                    },
                ]
            },
            {
                "title": 'Pacientes',
                "url": "/ventas/paciente",
                "icon": None,
                "help": "",
            },

        ]
    },
    {
        "id": "4",
        "title": "Configuración",
        "url": "/configuracion",
        "icon": None,
        "help": "",
        "secondary":[
            {
                "title": 'Productos',
                "url": "/configuracion/producto",
                "icon": None,
                "help": "",
                "secondary": [
                    {
                        "title": 'Categorias',
                        "url": "/configuracion/producto/categoria",
                        "icon": None,
                        "help": "",
                    },
                    {
                        "title": 'Marcas',
                        "url": "/configuracion/producto/marca",
                        "icon": None,
                        "help": "",
                    },
                    {
                        "title": 'Propiedades',
                        "url": "/configuracion/producto/propiedad",
                        "icon": None,
                        "help": "",
                    }
                ]
            },
            {
                "title": 'Configuración',
                "url": "/configuracion",
                "icon": None,
                "help": "",
            },
            {
                "title": 'Empresas',
                "url": "/configuracion/empresa",
                "icon": None,
                "help": "",
            },
        ]
    },
    {
        "id": "5",
        "title": "Usuarios",
        "url": "/auth",
        "icon": None,
        "help": "",
        "secondary":[
            {
                "title": 'Usuarios',
                "url": "/auth/users",
                "perssion": "authenticated",
                "icon": None,
                "help": "",
            },
            {
                "title": 'Crear usuario',
                "url": "/auth/users/create",
                "perssion": "authenticated",
                "icon": None,
                "help": "",
            },
            {
                "title": 'Grupos',
                "url": "/auth/groups",
                "perssion": "authenticated",
                "icon": None,
                "help": "",
            },
            {
                "title": 'Crear grupos',
                "url": "/auth/groups/create",
                "perssion": "authenticated",
                "icon": None,
                "help": "",
            }
        ]
    }
]
