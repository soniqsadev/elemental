"""erp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

# Para normalizar las url de las apis
apiurlpatterns = [
    url(r'^almacen/', include('almacen.api.urls')),
    url(r'^compras/', include('compras.api.urls')),
    url(r'^ventas/', include('ventas.api.urls')),
]

urlpatterns = [
    url(r'^admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
    url(r'^gestion/', admin.site.urls),
    url(r'^', include('core.urls')),
    url(r'^api/', include(apiurlpatterns)),
    url(r'^auth/', include('firewall.urls')),
    url(r'^division-territorial/', include('division_territorial.urls')),
    url(r'^ventas/', include('ventas.urls')),
    url(r'^compras/', include('compras.urls')),
    url(r'^almacen/', include('almacen.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [url(r'^__debug__/', include(debug_toolbar.urls)), ]
