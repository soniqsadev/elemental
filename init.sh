chown hernan:www-data . -R
chown www-data:www-data media -R
chmod 755 media
find media/ -type d -exec chmod 755 {} +
find media/ -type f -exec chmod 644 {} +
chmod g+w db.sqlite3
service apache2 restart
