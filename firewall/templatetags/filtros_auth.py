from django import template

register = template.Library()


@register.filter
def has_subitem_permissions(field, usuario):
    """
    Recibe la lista de elementos secundarios del menú
    y determina si tiene permisos para por lo menos una de las funcionalidades
    """
    for element in field:
        permission = element.get('perssion', None)
        if permission == None:
            return True
        else:
            permisos = permission.split('|')
            for permiso in permisos:
                if permiso == 'authenticated':
                    if usuario.is_authenticated():
                        return True
                else:
                    if usuario.has_permission(permiso):
                        return True
    return False

@register.filter
def first_subitem_permissions(field, usuario):
    """
    """
    for element in field:
        permission = element.get('perssion', None)
        if permission == None:
            return element
        else:
            permisos = permission.split('|')
            for permiso in permisos:
                if permiso == 'authenticated':
                    if usuario.is_authenticated():
                        return element
                else:
                    if usuario.has_permission(permiso):
                        return element
    return False

@register.filter
def all_subitem_permissions(field, usuario):
    """
    """
    all = []
    for element in field:
        permission = element.get('perssion', None)
        if permission == None:
            all.append(element)
        else:
            permisos = permission.split('|')
            for permiso in permisos:
                if permiso == 'authenticated':
                    if usuario.is_authenticated():
                        all.append(element)
                else:
                    if usuario.has_permission(permiso):
                        all.append(element)
    return all
