from django import forms
from django.contrib.auth.models import Group

class loginForm(forms.Form):
    username = forms.CharField(label='Usuario',max_length=64, required=True)
    password = forms.CharField(label='Contraseña',max_length=256, widget=forms.PasswordInput, required=True)


class registerForm(forms.Form):
    username = forms.CharField(label='Nombre de usuario', max_length=64, required=False)
    password = forms.CharField(label='Contraseña',max_length=256, widget=forms.PasswordInput, required=True)
    password_repeat = forms.CharField(label='Confirmación de contraseña',max_length=256, widget=forms.PasswordInput, required=True)
    first_name = forms.CharField(label='Nombres', max_length=128, required=False)
    last_name = forms.CharField(label='Apellidos', max_length=128, required=False)
    email = forms.EmailField(label='Correo electrónico', required=True)
    grupo = forms.ModelChoiceField(queryset=Group.objects.all())

    def clean_password_repeat(self):
        password = self.cleaned_data.get('password')
        password_repeat = self.cleaned_data.get('password_repeat')

        if password != password_repeat:
            raise forms.ValidationError("Las contraseñas no coinciden")
        return password_repeat


class UpdatePerfilForm(registerForm):

    def __init__(self, *args, **kwargs):
        super(UpdatePerfilForm, self).__init__(*args, **kwargs)
        self.fields['password'].required=False
        self.fields['password_repeat'].required=False


class GrupoForm(forms.ModelForm):
    # TODO: Define other fields here

    class Meta:
        model = Group
        fields = ('name', 'permissions')

    def __init__(self, *args, **kwargs):
        super(GrupoForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(GrupoForm, self).clean()
        return cleaned_data
