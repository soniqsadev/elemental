from django.db import models
from django.contrib.auth.models import User

from core.models import Natural

# Create your models here.
class Perfil(models.Model):
    usuario = models.OneToOneField(User, primary_key=True)
    persona_natural = models.OneToOneField(Natural, null=True)
    can_have_natural = models.BooleanField(default=False)

    def create_natural(self):
        pass

    def associate_natural(self, persona):
        self.persona_natural = persona
        self.save()

    class Meta:
        verbose_name = 'Perfil'
        verbose_name_plural = 'Perfiles'

    def __str__(self):
        if self.persona_natural:
            return self.persona_natural
        return self.usuario.username
