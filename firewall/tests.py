from django.test import TestCase

class TestLogin(TestCase):
    """Clase de prueba para login de usuarios"""

    def setUp(self):
        print('Llamada a setUp')

    def test_call_login_path(self):
        response = self.client.get('/auth/login/', follow=True)
        print (response.content)
        self.assertEqual(response.status_code, 200)
