from django.shortcuts import render, redirect
from django.contrib import messages
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic import View
from django.contrib.auth.models import User

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from ..forms import UpdatePerfilForm


class ShowPerfilView(View):
    template_name = 'firewall/perfil/show.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs) :
        return super(ShowPerfilView, self).dispatch(*args, **kwargs)

    def get(self, *args, **kwargs):
        user_id = kwargs.get('pk', None)

        if user_id != None:
            usuario = User.objects.get(pk=user_id)
        else:
            usuario = self.request.user
        return render(self.request, self.template_name, {'usuario':usuario})



class UpdatePerfilView(View):
    template_name = 'firewall/perfil/form.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs) :
        return super(UpdatePerfilView, self).dispatch(*args, **kwargs)

    def get(self, *args, **kwargs):
        user_id = kwargs.get('pk', None)
        if user_id != None:
            usuario = User.objects.get(pk=user_id)
        else:
            usuario = self.request.user
        form = UpdatePerfilForm(initial={
            "username": usuario.username,
            "email": usuario.email,
            "first_name": usuario.first_name,
            "last_name": usuario.last_name,
            "grupo": usuario.groups.first()
        })
        return render(self.request, self.template_name, {'form':form})

    def post(self, *args, **kwargs):
        user_id = kwargs.get('pk', None)
        if user_id != None:
            usuario = User.objects.get(pk=user_id)
        else:
            usuario = self.request.user

        form = UpdatePerfilForm(self.request.POST)

        if not form.is_valid():
            return render(self.request, self.template_name, {'form':form})

        data = form.cleaned_data

        usuario.username = data['username']
        usuario.email = data['email']
        usuario.first_name = data['first_name']
        usuario.last_name = data['last_name']

        if data['password']:
            usuario.set_password(data['password'])

        usuario.save()

        usuario.groups.clear()
        usuario.groups.add(data['grupo'])

        messages.success(self.request, 'Infomación almacenada correctamente' )
        return redirect('users.show', pk=usuario.id)

        return render(self.request, self.template_name, {'form':form})
