from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login as django_login, logout as django_logout
from django.contrib.auth.models import User
from django.views.generic import View

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from ..forms import loginForm, registerForm
from ..models import Perfil

class loginView(View):
    template_name = 'firewall/autenticacion/login.html'

    def get(self, request):
        form = loginForm()
        return render(request, self.template_name, {'form':form})

    def post(self, request):
        form = loginForm(request.POST)
        if not form.is_valid():
            return render(request, self.template_name, { 'form':form })

        data = form.cleaned_data
        usuario = authenticate(username=data['username'], password=data['password'])
        if usuario is not None:
            if usuario.is_active:
                django_login(request, usuario)
                prox = request.GET.get('next', 'index')
                messages.success(request, 'Bienvenido' )
                return redirect(prox)
            else:
                form = loginForm()
                messages.error( request, 'El usuario que ha ingresado se encuentra inactivo' )
                return render(request, self.template_name, { 'form':form })
        else:
            form = loginForm(request.POST)
            messages.error( request, 'Las credenciales que ha usado son incorrectos' )
            return render(request, self.template_name, { 'form':form })


def logout(request):
    ''' Funcionalidad para el cierre de sesión '''
    if not request.user.is_authenticated():
        messages.error( request, 'Usted aun no ha iniciado sesión')
        return redirect('login')
    django_logout(request)
    messages.success(request, 'Sesión cerrada satisfactoriamente')
    return redirect('index')


class registerView(View):
    template_name = 'firewall/register/form.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(registerView, self).dispatch(*args, **kwargs)

    def get(self, request):
        form = registerForm()
        return render(request, self.template_name, {'form':form})

    def post(self, request):
        form = registerForm(request.POST)
        if not form.is_valid():
            return render(request, self.template_name, {'form': form})

        data = form.cleaned_data
        if not data['username']:
            data['username'] = data['email']
        usuario = User.objects.create_user(
                    first_name = data['first_name'],
                    last_name = data['last_name'],
                    username = data['username'],
                    email = data['email'],
                    password = data['password']
                )
        usuario.save()
        usuario.groups.add(data['grupo'])
        Perfil(usuario=usuario).save()
        messages.success(request, 'Se ha creado el usuario satisfactoriamente' )

        return redirect('index')
