from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.views.decorators.http import require_http_methods, require_GET, require_POST

from django.views.generic import View
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView

from django.forms.formsets import formset_factory
from django.forms import modelformset_factory

from django.contrib.auth.models import Group, Permission

from ..forms import GrupoForm

SUCCESS_URL = reverse_lazy( 'groups.list' )

def permissions_show(user):
    return user.has_perm('auth.can_add_group') or user.has_perm('auth.can_change_group') or user.has_perm('auth.can_delete_group')


class GrupoListView(ListView):
    context_object_name = 'grupos'
    model = Group
    template_name = 'firewall/grupos/list.html'

    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(GrupoListView, self).dispatch(*args, **kwargs)


class GrupoCreateView(CreateView):
    model = Group
    form_class = GrupoForm
    success_url = SUCCESS_URL
    template_name = 'firewall/grupos/form.html'

    @method_decorator(login_required)
    @method_decorator(permission_required('auth.can_add_group'))
    def dispatch(self, *args, **kwargs):
        return super(GrupoCreateView, self).dispatch(*args, **kwargs)

class GrupoUpdateView(UpdateView):
    model = Group
    form_class = GrupoForm
    success_url = SUCCESS_URL
    template_name = 'firewall/grupos/form.html'

    @method_decorator(login_required)
    @method_decorator(permission_required('auth.can_change_group'))
    def dispatch(self, *args, **kwargs):
        return super(GrupoUpdateView, self).dispatch(*args, **kwargs)


class GrupoDetailView(DetailView):
    model = Group
    success_url = SUCCESS_URL
    template_name = 'firewall/grupos/detail.html'

    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(GrupoUpdateView, self).dispatch(*args, **kwargs)


class GrupoDeleteView(DeleteView):
    model = Group
    template_name = 'firewall/grupos/delete.html'

    @method_decorator(login_required)
    @method_decorator(permission_required('auth.can_delete_group'))
    def dispatch(self, *args, **kwargs):
        return super(GrupoUpdateView, self).dispatch(*args, **kwargs)
