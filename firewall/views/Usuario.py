from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.models import User
from django.views.generic.list import ListView
from django.views.generic.edit import DeleteView
from .Perfil import ShowPerfilView, UpdatePerfilView

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.views.decorators.http import require_http_methods, require_GET, require_POST


SUCCESS_URL = reverse_lazy( 'users.list' )

def permissions_show(user):
    return user.has_perm('auth.can_add_user') or user.has_perm('auth.can_change_user') or user.has_perm('auth.can_delete_user')


class UserListView(ListView):
    context_object_name = 'usuarios'
    model = User
    page_kwarg = 'page'
    paginate_by = 10
    template_name = 'firewall/usuarios/list.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(UserListView, self).dispatch(*args, **kwargs)


class UserShowView(ShowPerfilView):
    template_name = 'firewall/usuarios/show.html'

    # Permisos
    @method_decorator(require_GET)
    @method_decorator(login_required)
    #@method_decorator( user_passes_test( permissions_show ) )
    def dispatch(self, *args, **kwargs):
        return super(UserListView, self).dispatch(*args, **kwargs)

class UserUpdateView(UpdatePerfilView):
    template_name = 'firewall/usuarios/form.html'

    # Permisos
    @method_decorator(login_required)
    #@method_decorator(permission_required('auth.can_change_user'))
    def dispatch(self, *args, **kwargs):
        return super(UserListView, self).dispatch(*args, **kwargs)

class UserDeleteView(DeleteView):
    model = User
    context_object_name = 'usuario'
    success_url = SUCCESS_URL
    template_name = 'firewall/usuarios/delete_confirm.html'

    # Permisos
    @method_decorator(login_required)
    @method_decorator(permission_required('auth.can_delete_user'))
    def dispatch(self, *args, **kwargs):
        return super(UserDeleteView, self).dispatch(*args, **kwargs)
