from django.conf.urls import url

from .views import Autenticacion, Perfil, Usuario, Grupo


urlpatterns = [
    url(regex=r'^login/$', view=Autenticacion.loginView.as_view(), name='login'),
    url(regex=r'^logout/$', view=Autenticacion.logout, name='logout'),

    url(regex=r'^account/$', view=Perfil.ShowPerfilView.as_view(), name='my_account'),
    url(regex=r'^account/edit/$', view=Perfil.UpdatePerfilView.as_view(), name='my_account.edit'),

    url(regex=r'^users/$', view=Usuario.UserListView.as_view(), name='users.list'),
    url(regex=r'^users/create/$', view=Autenticacion.registerView.as_view(), name='users.register'),
    url(regex=r'^users/(?P<pk>[0-9]+)/$', view=Usuario.UserShowView.as_view(), name='users.show'),
    url(regex=r'^users/(?P<pk>[0-9]+)/edit/$', view=Usuario.UserUpdateView.as_view(), name='users.edit'),
    url(regex=r'^users/(?P<pk>[0-9]+)/delete/$', view=Usuario.UserDeleteView.as_view(), name='users.delete'),

    url(regex=r'^groups/$', view=Grupo.GrupoListView.as_view(), name='groups.list'),
    url(regex=r'^groups/create/$', view=Grupo.GrupoCreateView.as_view(), name='groups.create'),
    url(regex=r'^groups/(?P<pk>[0-9]+)/$', view=Grupo.GrupoDetailView.as_view(), name='groups.show'),
    url(regex=r'^groups/(?P<pk>[0-9]+)/edit/$', view=Grupo.GrupoUpdateView.as_view(), name='groups.edit'),
    url(regex=r'^groups/(?P<pk>[0-9]+)/delete/$', view=Grupo.GrupoDeleteView.as_view(), name='groups.delete'),
]
