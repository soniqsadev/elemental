from django.db import models

# Create your models here.

class Estado(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = u'Estado'
        verbose_name_plural = u'Estados'
        ordering = ['name',]


class Ciudad(models.Model):
    estado_id = models.ForeignKey('Estado', related_name='ciudades')
    name = models.CharField(max_length=100)
    capital = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = u'Ciudad'
        verbose_name_plural = u'Ciudades'
        ordering = ['name',]
