from django.conf.urls import url

from .views import ListEstadoView, lista_ciudades_view

urlpatterns = [

    url(regex=r'^estado/$', view=ListEstadoView.as_view(), name='estado.list'),
    url(regex=r'^estado/(?P<estado_id>[0-9]+)/ciudades/$', view=lista_ciudades_view, name='ciudad.list'),
]
