$(function () {

    

    $("#id_estado").on('change', function(event) {
        event.preventDefault();
        selected_id = this.options[this.selectedIndex].value;
        $.getJSON('/division-territorial/estado/' + selected_id + '/ciudades', {}, function(json, textStatus) {
                $.get('/static/js/ventas/templates/ciudades.ejs', function (template) {
                    options = ejs.render(template, {'ciudades':json})
                    $('#id_ciudad').html($(options));
                })
        });
    });
})
