from django.shortcuts import render

from .models import Estado, Ciudad
from .serializers import EstadoSerializer, CiudadSerializer

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view


class ListEstadoView(APIView):

    def get(self, request, format=None):
        estados = Estado.objects.all()
        serializer = EstadoSerializer(estados, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def lista_ciudades_view(request, estado_id):
    ciudades = Ciudad.objects.filter(estado_id=estado_id)
    serializer = CiudadSerializer(ciudades, many=True)
    return Response(serializer.data)
