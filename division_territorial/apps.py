from django.apps import AppConfig


class DivisionTerritorialConfig(AppConfig):
    name = 'division_territorial'
