from django.contrib import admin
from .models import Estado, Ciudad

@admin.register(Estado)
class EstadoAdmin(admin.ModelAdmin):
    pass

@admin.register(Ciudad)
class CiudadAdmin(admin.ModelAdmin):
    pass

# Register your models here.
