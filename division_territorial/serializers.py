from rest_framework import serializers

from .models import Estado, Ciudad

class EstadoSerializer(serializers.ModelSerializer):
    """docstring for HospitalSerializer"""
    class Meta:
        model = Estado
        fields = ('id','name')

class CiudadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ciudad
        fields = ('id', 'name', 'estado_id')
