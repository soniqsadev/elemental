from core.forms.custom import SelectDependent, Searchable
from ..models import Ciudad, Estado

class EstadoField(Searchable.ChoiceSearchableField):

    def __init__(self, *args, **kwargs):
        queryset = kwargs.get('queryset', Estado.objects.all())
        super(EstadoField, self).__init__(queryset=queryset, *args, **kwargs)

class CiudadField(SelectDependent.ChoiceSearchableDependentField):

    def __init__(self, *args, **kwargs):
        queryset = kwargs.get('queryset', Ciudad.objects.all())
        url = kwargs.get('url', '/division-territorial/estado/__id__/ciudades')
        target_id = kwargs.get('target_id', 'id_estado')
        kv = kwargs.get('kv', {'key':'id', 'value':'name'})
        super(CiudadField, self).__init__(
                                      queryset=queryset
                                    , url=url
                                    , target_id=target_id
                                    , kv=kv
                                    , *args
                                    , **kwargs
                                )
