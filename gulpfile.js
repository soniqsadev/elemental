var gulp = require('gulp');
var cssnano = require('gulp-cssnano');
var uglify = require('gulp-uglify');
var jsonminify = require('gulp-jsonminify');

var bower_dir = 'bower_components/';
var nodemod_dir = 'node_modules/'

/**
 *  Mover las librerias javascript de terceros
 */
gulp.task('contribjs', function(){

    paths = [
        // Librerias cargadas desde bower_dir
        bower_dir + 'angular/angular.min.js',
        bower_dir + 'angular-route/angular-route.min.js',
        bower_dir + 'angular-resource/angular-resource.min.js',
        bower_dir + 'angular-multi-step-form/dist/browser/angular-multi-step-form.min.js',
        bower_dir + 'jquery/dist/jquery.min.js',
        bower_dir + 'moment/min/moment.min.js',
        bower_dir + 'moment/locale/es.js',
        bower_dir + 'bootstrap/dist/js/bootstrap.min.js',
        bower_dir + 'eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
        bower_dir + 'bootstrap3-dialog/dist/js/bootstrap-dialog.min.js',
        bower_dir + 'chosen/chosen.jquery.js',
        bower_dir + 'datatables.net/js/jquery.dataTables.min.js',
        bower_dir + 'datatables.net-bs/js/dataTables.bootstrap.min.js',
        bower_dir + 'angular-chosen-localytics/dist/angular-chosen.min.js',
        // Librerias cargados desde node_modules
        nodemod_dir + 'ejs/ejs.min.js',

    ];

    dest = 'core/static/js/lib';

    move(paths, dest);
});

/**
 *  Mover las librerias css de terceros
 */
gulp.task('contribcss', function () {

    paths = [
        bower_dir + 'bootstrap/dist/css/bootstrap.min.css',
        bower_dir + 'bootstrap/dist/css/bootstrap-theme.min.css',
        bower_dir + 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
        bower_dir + 'bootstrap3-dialog/dist/css/bootstrap-dialog.min.css',
        bower_dir + 'chosen/chosen.css',
        bower_dir + 'chosen/chosen-sprite.png',
        bower_dir + 'datatables.net-bs/css/dataTables.bootstrap.min.css',
    ];

    dest = 'core/static/css/lib';

    move(paths, dest);

});

/**
 *  Mover las librerias css de terceros
 */
gulp.task('contribfonts', function () {

    paths = [
        bower_dir + 'bootstrap/dist/fonts/glyphicons-halflings-regular.eot',
        bower_dir + 'bootstrap/dist/fonts/glyphicons-halflings-regular.svg',
        bower_dir + 'bootstrap/dist/fonts/glyphicons-halflings-regular.ttf',
        bower_dir + 'bootstrap/dist/fonts/glyphicons-halflings-regular.woff',
        bower_dir + 'bootstrap/dist/fonts/glyphicons-halflings-regular.woff2',
    ];

    dest = 'core/static/css/fonts';

    move(paths, dest);

});

function move(paths, dest){
    gulp.src(paths)
        .pipe(gulp.dest(dest));
}


gulp.task('css', function () {
    gulp.src('core/src/css/*.css')
        .pipe(cssnano())
        .pipe(gulp.dest('core/static/css'));
});


gulp.task('js', function () {
    gulp.src('core/src/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('core/static/js'));
});

gulp.task('json', function () {
    gulp.src('core/src/js/*.json')
        .pipe(jsonminify())
        .pipe(gulp.dest('core/static/js'));
});


gulp.task('default', ['contribjs', 'contribcss', 'contribfonts', 'css', 'js', 'json']);
