# Elemental #

Software base del sistema ERP desarrollado para Soniqsa

### Requisitos ###

* Python 3
* Mysql o MariaDB
* Apache + mod_wsgi

### Paquetes adicionales para conectar con MySQL [Ubuntu 14.04] ###


```
#!bash

~$ sudo aptitude install python3-dev libmysqlclient-dev
```


```
#!bash

~$ pip install mysqlclient
```


### Configuración de Apache2 ###

...


[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)